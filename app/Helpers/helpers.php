<?php

use App\Models\User;

if (!function_exists('isVendorUser')) {
    /**
     * Check if the provided user is vendor or not
     * @param \App\Models\User $user
     * 
     * @return [type]
     */
    function isVendorUser(User $user)
    {
        return $user->identifier == 1;
    }
}

if (!function_exists('distanceCalcutator')) {

function distanceCalcutator($latFrom, $lonFrom, $latTo, $lonTo, $unit) {
    if (($latFrom == $latTo) && ($lonFrom == $lonTo)) {
      return 0;
    }
    else {
      $theta = $lonFrom - $lonTo;
      $dist = sin(deg2rad($latFrom)) * sin(deg2rad($latTo)) +  cos(deg2rad($latFrom)) * cos(deg2rad($latTo)) * cos(deg2rad($theta));
      $dist = acos($dist);
      $dist = rad2deg($dist);
      $miles = $dist * 60 * 1.1515;
      $unit = strtoupper($unit);
  
      if ($unit == "K") {
        return ($miles * 1.609344);
      } else if ($unit == "N") {
        return ($miles * 0.8684);
      } else {
        return $miles;
      }
    }
  }
}
