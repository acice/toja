<?php

namespace App\Helpers;

class DbConstants
{
    const TABLE_USERS = 'users';
    const TABLE_PAYER_DETAILS = 'payer_details';
    const TABLE_PAYMENT_DETAILS= 'payment_details';
}
