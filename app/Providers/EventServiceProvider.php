<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'Illuminate\Auth\Events\Login' => [
            'App\Listeners\Auth\Login\LogSuccessfulLogin',
        ],
        'Illuminate\Auth\Events\Failed' => [
            'App\Listeners\Auth\Login\LogFailedLogin',
        ],
        'App\Events\Deal\DealCreated' => [
            'App\Listeners\Deal\AddDealNotification',
        ],
        'App\Events\Deal\DealUpdated' => [
            'App\Listeners\Deal\UpdateDealNotification',
        ],
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
            \SocialiteProviders\Manager\SocialiteWasCalled::class => [
            // add your listeners (aka providers) here
            'SocialiteProviders\\PayPalSandbox\\PayPalSandboxExtendSocialite@handle',
            'SocialiteProviders\Apple\AppleExtendSocialite@handle',
            'SocialiteProviders\Google\GoogleExtendSocialite@handle',
            
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
