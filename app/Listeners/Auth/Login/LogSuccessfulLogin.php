<?php

namespace App\Listeners\Auth\Login;

use Carbon\Carbon;
use App\Location\Location;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class LogSuccessfulLogin
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $ip = request()->ip();
        $position = (new Location())->get($ip);

        session()->flash('status', __('auth.signedIn'));
        $event->user->update(['last_login' => new Carbon]);
        $event->user->logins()->create([
            'ip_address' => $ip,
            'successful' => true,
            'longitude' => $position->longitude ?? null,
            'latitude' => $position->latitude ?? null,
        ]);
    }
}
