<?php

namespace App\Listeners\Auth\Login;

use Carbon\Carbon;
use App\Models\UserLogin;
use App\Location\Location;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class LogFailedLogin
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $ip = request()->ip();

        $position = (new Location())->get($ip);

        $updateData = [
            'ip_address' => $ip,
            'successful' => false,
            'longitude' => $position->longitude ?? null,
            'latitude' => $position->latitude ?? null,
        ];

        if ($event->user) {
            $event->user->logins()->create($updateData);
        } else {
            UserLogin::create($updateData + ['email' => $event->credentials['email']]);
        }
    }
}
