<?php

namespace App\Listeners\Deal;

use App\Events\Deal\DealCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Notifications\DealActivated;
use App\Jobs\StatusChangeDeals;
use App\Models\User;

class AddDealNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  DealCreated  $event
     * @return void
     */
    public function handle(DealCreated $event)
    {   
        $notifiableVendor=User::where('id',$event->vendor_id)->first();
        $notifiableVendor->invoice_no = $event->invoice_no;
        $notifiableVendor->customer = auth()->user()->name;
        StatusChangeDeals::dispatch($event)->delay(now()->addMinutes(6));
        $notifiableVendor->notify(new DealActivated($notifiableVendor));
    }
}
