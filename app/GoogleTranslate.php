<?php

namespace App;
use Google\Cloud\Translate\TranslateClient;

class GoogleTranslate
{
  
    private $translate;
  
    public function __construct()
    {
       $this->translate =  new TranslateClient([
            'key' => 'AIzaSyAZFo-lYJTf4Z1T_hKfeTf-BYP2QjmzoR0'
         ]);
    }

    /**
     * Translate 
     *
     * @return Google\Cloud\Translate\TranslateClient
     */
    public function translate($string, $flag)
    {
        return $this->translate->translate($string, [
            'target' => $flag,
        ]);
    }
  
    public function detectLanguage($string, $options = []) {
      return $this->translate->detectLanguage($string, $options);
    }
}
