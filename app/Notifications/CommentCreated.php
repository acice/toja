<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use NotificationChannels\ExpoPushNotifications\ExpoChannel;
use NotificationChannels\ExpoPushNotifications\ExpoMessage;
use Illuminate\Support\Facades\App;

class CommentCreated extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
          return [ExpoChannel::class,'database'];
    }

    public function toExpoPush($notifiable)
    {
        $flag=$notifiable->flag;
        App::setLocale($flag);

        $data='{
          "type": "COMMENT",
          "user": {
            "name": "'.$notifiable->customer.'",
            "avatar_url": "'.$notifiable->avatar_url.'"
          },
          "post": { "title": "'.$notifiable->post_title.'", "id": '.$notifiable->post_id.' },
          "comment":{"id":'.$notifiable->comment_id.'}
        }';
        $data=json_decode($data);
      
        return ExpoMessage::create()
            ->badge(1)
            ->enableSound()
            ->title($notifiable->customer.trans('validation.comment_create'))
            ->body(trans('validation.comment_create_by').$notifiable->customer)
            ->setJsonData($data);
    }

    public function toDatabase($notifiable)
    {
        $flag=$notifiable->flag;
        App::setLocale($flag);
        
        $data='{
          "type": "COMMENT",
          "user": {
            "name": "'.$notifiable->customer.'",
            "avatar_url": "'.$notifiable->avatar_url.'"
          },
          "post": { "title": "'.$notifiable->post_title.'", "id": '.$notifiable->post_id.' },
          "comment":{"id":'.$notifiable->comment_id.'}
        }';

        $data=json_decode($data);
          
        return [
            $data,
        ];
    }


}
