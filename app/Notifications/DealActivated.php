<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use NotificationChannels\ExpoPushNotifications\ExpoChannel;
use NotificationChannels\ExpoPushNotifications\ExpoMessage;
use Illuminate\Support\Facades\App;
use App\Models\ActivatedDeals;

class DealActivated extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
          return [ExpoChannel::class,'database'];
    }

    public function toExpoPush($notifiable)
    {
        $flag=$notifiable->flag;
        App::setLocale($flag);

        $deal=ActivatedDeals::with('post','post.files','user:id,name,email,phone_no,avatar','payment:id,amount,vendor_amount,payment_status,name,activated_deals_id')->findOrFail($notifiable->deal_id);

        $deal->type="DEAL_REDEEMED";
        $data=json_decode($deal);
        
        return ExpoMessage::create()
            ->badge(1)
            ->enableSound()
            ->title(trans('validation.deal_redeem_title').$notifiable->customer)
            ->body(trans('validation.deal_redeem_by').$notifiable->customer." !!")
            ->setJsonData($data);
    }

    public function toDatabase($notifiable)
    {
        $flag=$notifiable->flag;
        App::setLocale($flag);

        $deal=ActivatedDeals::with('post','post.files','user:id,name,email,phone_no,avatar','payment:id,amount,vendor_amount,payment_status,name,activated_deals_id')->findOrFail($notifiable->deal_id);

        $deal->type="DEAL_REDEEMED";
        $data=json_decode($deal);

        return [
            $data
        ];
    }


}
