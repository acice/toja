<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use NotificationChannels\ExpoPushNotifications\ExpoChannel;
use NotificationChannels\ExpoPushNotifications\ExpoMessage;
use Illuminate\Support\Facades\App;

class Liked extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
          return [ExpoChannel::class,'database'];
    }

    public function toExpoPush($notifiable)
    {
        $flag=$notifiable->flag;
        App::setLocale($flag);
        
        $data='{
          "type": "POST_LIKED",
          "user": {
            "name": "'.$notifiable->user_name.'",
            "avatar_url": "'.$notifiable->avatar_url.'"
          },
          "post": { "title": "'.$notifiable->post_title.'", "id": '.$notifiable->post_id.' }
        }';
          $data=json_decode($data);
        return ExpoMessage::create()
            ->badge(1)
            ->enableSound()
            ->title($notifiable->user_name.trans('validation.liked_title'))
            ->body(trans('validation.liked_by').$notifiable->user_name)
            ->setJsonData($data);
    }

    public function toDatabase($notifiable)
    {
        $flag=$notifiable->flag;
        App::setLocale($flag);

        $data='{
          "type": "POST_LIKED",
          "user": {
            "name": "'.$notifiable->user_name.'",
            "avatar_url": "'.$notifiable->avatar_url.'"
          },
          "post": { "title": "'.$notifiable->post_title.'", "id": '.$notifiable->post_id.' }
        }';

        $data=json_decode($data);
          
        return [
            $data
        ];
    }


}
