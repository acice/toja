<?php

namespace App\Http\Requests\Auth;

use App\Helpers\DbConstants;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class SignUpRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    
    public function rules()
    {
        return [
            'name'       => 'required|string|min:3|max:50',
            'email'      => [
                'required',
                'string',
                'email',
                Rule::unique(DbConstants::TABLE_USERS, 'email')
                    ->whereNull('deleted_at')
            ],
            'phone_no'   => [
                'required',
                'min:9',
                'regex:/^([0-9\s\-\+\(\)]*)$/',
                Rule::unique(DbConstants::TABLE_USERS, 'phone_no')
            ],
            'flag'       => 'required',
            'identifier' => 'required|in:1,0'
        ];
    }
}
