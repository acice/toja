<?php

namespace App\Http\Requests\Api;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class CreatePostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user = auth()->user();
        return $user instanceof User
            && isVendorUser($user);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|min:10|max:120',
            'body' => 'required|min:10|max:300',
            'days' => 'max:100',
            'hours' => 'max:24',
            'discount_amount' => 'required|min:2||max:100',
            'discount_cost' => 'required|min:1||max:999999999',
            'discount_activations' => 'required|min:1||max:999',
            'availibility' => 'max:100',
            'sit_in_term' => 'max:100',
            'time_from' => 'max:100',
            'time_to' => 'max:100',
            'images.*' => 'required|mimes:jpeg,jpg,png|max:20048'
        ];
    }
}
