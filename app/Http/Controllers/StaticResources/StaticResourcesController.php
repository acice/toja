<?php
namespace App\Http\Controllers\StaticResources;

use App\Http\Controllers\Controller;
use Auth;
use StaticArray;



class StaticResourcesController extends Controller
{

    public function __construct()
    {

    }

    public function bankList(){
        if(Auth::user()->flag=='en'){
            $banks=StaticArray::$banks;
        }else{
            $banks=StaticArray::$iw_banks;
        }
        
        if ($banks) {
            return response()->json($banks,200);
        }
        else{
            return response()->json([
                'message' => trans('validation.unable_bank')
            ], 400);
        }
    }

    public function bankByID($id){
        if(Auth::user()->flag=='en'){
            $banks=StaticArray::$banks;
        }else{
            $banks=StaticArray::$iw_banks;
        }
        if (isset($banks[$id])) {
            return response()->json($banks[$id],200);
        }
        else{
            return response()->json([
                'message' => trans('validation.unable_bank')
            ], 400);
        }
    }

}
