<?php

namespace App\Http\Controllers\Posts;

use Carbon\Carbon;
use App\Models\File;
use App\Models\Post;
use App\Models\User;
use App\Models\Vendor;
use App\GoogleTranslate;
use App\Models\DiscountDay;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use App\Http\Requests\Api\CreatePostRequest;
use App\Http\Requests\Api\UpdatePostRequest;
use App\Repositories\PostEloquentRepository;
use Intervention\Image\ImageManagerStatic as Image;
use App\Models\Category;

/**
 * PostController
 */
class PostController extends Controller
{

    /**
     * PostEloquentRepository
     *
     * @var PostEloquentRepository 
     */
    protected $posts;

    /**
     * Constructor
     * 
     * @param PostEloquentRepository $posts
     */
    public function __construct(PostEloquentRepository $posts)
    {
        $this->posts = $posts;
    }

    public function index()
    {
        return view('vendor.discounts.show-discounts');
    }

       //all Posts
    public function getAllPosts()
    {
        $posts = Post::with(
            'files',
            'comments',
            'user',
            'userfollowing.following',
            'tags.tagged',
            'tags.user',
            'user.vendor'
        )->withCount('activatedDeals')
            ->orderBy("created_at", 'DESC')
            ->paginate(10);

        return response()->json(
            $posts,
            200
        );
    }

    //having deal discount time remaining
    public function getAllActivePosts()
    {
        $posts = Post::with(
            'files',
            'comments',
            'user',
            'userfollowing.following',
            'tags.tagged',
            'tags.user',
            'user.vendor'
        )
            ->withCount('activatedDeals')
            ->where('discount_end','>',now()->toDateTimeString())
            ->orderBy("created_at", 'DESC')
            ->paginate(10);

        return response()->json(
            $posts,
            200
        );
    }


 //Deal Reedemed by User
    public function getAllActivatedDealsForUser($userId)
    {
        $posts = Post::with('user', 'files', 'comments', 'discountdays', 'user.vendor', 'userfollowing.following')
            ->withCount('activatedDeals')
            ->where('discount_end','>',now()->toDateTimeString())
            ->whereHas('activatedDeals', function ($query) use ($userId) {
                return $query->where('user_id', $userId);
            })
            ->orderBy("created_at", 'DESC')
            ->paginate(10);

        return response()->json(
            $posts,
            200
        );
    }

 //Deal Reedemed by Vendor
    public function getAllActivatedDealsForVendor($vendorId)
    {
        $posts = Post::with(
            'files',
            'comments',
            'user',
            'userfollowing.following',
            'tags.tagged',
            'tags.user',
            'user.vendor'
        )
            ->withCount('activatedDeals')
            ->whereHas('activatedDeals', function ($query) use ($vendorId) {
                return $query->where('vendor_id', $vendorId);
            })
            ->paginate(10);


        return response()->json(
            $posts,
            200
        );
    }

 
    /**
     * Create post
     * @param CreatePostRequest $request
     *
     * @return Illuminate\Http\JsonResponse
     */
    public function createPost(CreatePostRequest $request)
    {
        $availibility_sit_in='sit in';
        $availibility_collect_in='collect in store';
        $avalible_terms=explode(',',$request->availibility);
        if($request->discount_cost<25){
            if($request->discount_amount<10){
                $message = trans('validation.minimum_discount_amount');
                return response()->json([
                    'message' => $message
                ], 400);
            }
        }
        
        if(in_array($availibility_sit_in,$avalible_terms)){
            $request->validate([
                'sit_in_term' => 'required'
            ]);
        }

        if(in_array($availibility_collect_in,$avalible_terms)){
            $request->validate([
                'time_from' => 'required',
                'time_to' => 'required'
            ]);
        }
        

        $days=!empty($request->days)?$request->days:0;
        $hours=!empty($request->hours)?$request->hours:0;

        $discountEnd = Carbon::now()
            ->addDays($days)
            ->addHours($hours)
            ->format('Y-m-d H:i:s');
        $title=$request->title;
        $body=$request->body;
        
        $post = new Post(
            [
                'title' => !empty($title)?(new GoogleTranslate)->translate($title,'en')['text']:'',
                'iw_title' => !empty($title)?(new GoogleTranslate)->translate( $title,'iw')['text']:'',
                'body' => !empty($body)?(new GoogleTranslate)->translate($body,'en')['text']:'',
                'iw_body' => !empty($body)?(new GoogleTranslate)->translate($body,'iw')['text']:'',
                'user_id' => Auth::user()->id,
                'discount_cost' => $request->discount_cost,
                'discount_amount' => $request->discount_amount,
                'discount_activations' => $request->discount_activations,
                'discount_end' => $discountEnd,
                'availibility' => $request->availibility,
                'sit_in_term' => $request->sit_in_term,
                'time_from' => $request->time_from,
                'time_to' => $request->time_to
            ]
        );

        $post->save();

        if ($request->hasFile('images')) {
            $this->addPostImage($request, $post->id);
        }

        if ($post) {
            $message = trans('validation.successful_create_post');
            return response()->json(
                [
                    'message' => $message,
                    'post' => $post->id,
                ],
                200
            );
        }
        $message = trans('validation.unable_create_post');
        return response()->json(
            [
                'message' => $message,
            ],
            400
        );
    }

    /**
     * Create image on post
     * $param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\JsonResponse
     */
    public function addPostImage(Request $request,int $id)
    {
        $request->validate([
            'image.*' => 'nullable|image|mimes:jpeg,jpg,png|size:6000'
        ]);

        $file = '';
        $maximumImageUpload = 5;
        $imageCount = DB::table('files')
            ->where('user_id', '=', Auth::user()->id)
            ->where('post_id', '=', $id)
            ->count();

        if ($imageCount > $maximumImageUpload) {
            return response()->json([
                'message' => trans('validation.max_image_upload'). $maximumImageUpload,
            ], 200);
        }

        if ($request->hasFile('images')) {
            $images = $request->file('images');
            foreach ($images as $image) {
                $fileName = mt_rand() . time() . '.jpg';

                $img = Image::make($image->getRealPath());
                $img->resize(450, 450, function ($constraint) {
                    $constraint->aspectRatio();
                });

                $img->stream('jpg',80); // <-- Key point
                Storage::disk('public')->put('post/images' . '/' . $fileName, $img, 'public');

                $file = new File;
                $file->file = $fileName;
                $file->user_id = Auth::user()->id;
                $file->post_id = $id;
                $file->save();
            }

            if ($file) {
                $message = trans('validation.successful_upload_image');
                return response()->json([
                    'message' => $message,
                ], 200);
            } else {
                $message = trans('validation.unable_upload_image');
                return response()->json([
                    'message' => $message,
                ], 400);
            }
        }
    }

    public function removePostImage(Request $request,int $id)
    {
        $file = DB::table('files')->where('id', '=', $id)->first();
        // Remove file from dir
        Storage::delete('post/images' . '/' . $file->file);
        // Remove file from table
        $delete = DB::table('files')->where('id', $id)->delete();

        if ($delete) {
            $message =trans('validation.successful_removed_image');
            return response()->json([
                'message' => $message,
            ], 200);
        } else {
            $message =trans('validation.unable_removed_image');
            return response()->json([
                'message' => $message,
            ], 400);
        }
    }

    /**
     *  Update post
     *  @param  Illuminate\Http\Request $request.
     *  @param [int] $id
     *
     * @return Illuminate\Http\JsonResponse
     */
    public function updatePost(UpdatePostRequest $request,int $id)
    {
        $availibility_sit_in='sit in';
        $availibility_collect_in='collect in store';
        $avalible_terms=explode(',',$request->availibility);
        if($request->discount_cost<25){
            if($request->discount_amount<10){
                $message = trans('validation.minimum_discount_amount');
                return response()->json([
                    'message' => $message
                ], 400);
            }
        }
        
        if(in_array($availibility_sit_in,$avalible_terms)){
            $request->validate([
                'sit_in_term' => 'required'
            ]);
        }

        if(in_array($availibility_collect_in,$avalible_terms)){
            $request->validate([
                'time_from' => 'required',
                'time_to' => 'required'
            ]);
        }
        
        
        $days=!empty($request->days)?$request->days:0;
        $hours=!empty($request->hours)?$request->hours:0;
        $minutes=!empty($request->minutes)?$request->minutes:0;
        $discountEnd = Carbon::now()->addDays($days)->addHours($hours)->addMinutes($minutes)->format('Y-m-d H:i:s');
        $title=$request->title;
        $body=$request->body;
        $update = Post::where('id', $id)
            ->update([
                'title' => !empty($title)?(new GoogleTranslate)->translate($title,'en')['text']:'',
                'iw_title' => !empty($title)?(new GoogleTranslate)->translate( $title,'iw')['text']:'',
                'body' => !empty($body)?(new GoogleTranslate)->translate($body,'en')['text']:'',
                'iw_body' => !empty($body)?(new GoogleTranslate)->translate($body,'iw')['text']:'',
                'user_id' => Auth::user()->id,
                'discount_cost' => $request->discount_cost,
                'discount_amount' => $request->discount_amount,
                'discount_end' => $discountEnd,
                'discount_activations' => $request->discount_activations,
                'availibility' => $request->availibility,
                'sit_in_term' => $request->sit_in_term,
                'time_from' => $request->time_from,
                'time_to' => $request->time_to
            ]);

        if ($request->has('images')) {
           $post= Post::find($id);
            if (!empty($post->files)) {
                foreach ($post->files as $file) {
                    $this->removePostImage($request, $file->id);
                }
            }
            $this->addPostImage($request, $id);
        }
        if ($update) {
            $message =  trans('validation.successful_update_post');
            return response()->json([
                'message' => $message ,
            ], 200);
        } else {
            $message = trans('validation.unable_update_post');
            return response()->json([
                'message' =>  $message ,
            ], 400);
        }
    }

    public function updateDiscountDay(Request $request,int $id)
    {
        $update = DiscountDay::where('id', $id)
            ->update([
                'day' => $request->day,
            ]);

        if ($update) {
            $message = Auth::user()->flag == 'en' ? 'Successfully updated discount day' : 'יום ההנחה עודכן בהצלחה';
            return response()->json([
                'message' => $message,
            ], 200);
        } else {
            $message = Auth::user()->flag == 'en' ? 'Unable to update discount day' : 'לא ניתן לעדכן את יום ההנחה';
            return response()->json([
                'message' => $message,
            ], 400);
        }
    }

    /**
     * Delete post
     *
     * @param [int] $id
     * @return Illuminate\Http\JsonResponse
     */
    public function deletePost(Request $request,int $id)
    {
        $post = Post::where('id', $id)->with('files')->first();

        if($post->activatedDeals()->count()>0){
            $message =trans('validation.post_has_deal');
            return response()->json([
                'message' => $message ,
            ], 400);
        }

        //     if (!empty($post->files)) {
        //         foreach ($post->files as $file) {
        //             $this->removePostImage($request, $file->id);
        //         }
        //     }

   
        $deletePost = Post::where('id', $id)->delete();
        if ($deletePost) {
            $message =trans('validation.successfully_remove_post');
            return response()->json([
                'message' => $message,
            ], 200);
        } else {
            $message =trans('validation.unable_remove_post');
            return response()->json([
                'message' => $message ,
            ], 400);
        }
    }

    /**
     * Show posts user follows
     *
     * @return Illuminate\Http\JsonResponse
     */
    public function listPosts(Request $request)
    {
        $followersId=Auth::user()->following()->pluck('followable_id');
        $following = $this->posts->activePosts()
            ->doesntHave('activatedDealsRejected')
            ->whereIN('user_id',$followersId)->with(
                [
                    'files',
                    'user.vendor',
                ]
            )
            ->where('discount_end', '>', now())
            ->orderBy("created_at", 'DESC')
            ->whereHas(
                'user',
                function ($q) {
                    return  $q->where('active', '=', 1);
                }
            );
          
        $following =$following->paginate(10);
    
        if ($following) {
            $data = [
                'posts' =>  $following
            ];

            return response()->json(
                $data,
                200
            );
        }

        $message = trans('validation.unable_get_post');
        return response()->json(
            [
                'message' => $message,
            ],
            400
        );
    }

    public function postByCategoriesId(Request $request,$id) {
        $category = Category::find($id);
        $vendors=$category->vendors()->with('user:id')->get();
        $userid=[];
        foreach ($vendors as $vendor) {
            if(!empty($vendor->user->id)){
                $userid[]=$vendor->user->id;
            }
        }
      
        $followersId=Auth::user()->following()->whereIN('followable_id',$userid)->pluck('followable_id');
        $following = $this->posts->activePosts()
                            ->doesntHave('activatedDealsRejected')
                            ->whereIN('user_id',$followersId)->with(
                                [
                                    'files',
                                    'user.vendor',
                                ]
                            )
                            ->where('discount_end', '>', now())
                            ->orderBy("created_at", 'DESC')
                            ->whereHas(
                                'user',
                                function ($q) {
                                    return  $q->where('active', '=', 1);
                                }
                            );
          
        $following =$following->paginate(10);
    
        if ($following) {
            $data = [
                'posts' =>  $following
            ];

            return response()->json(
                $data,
                200
            );
        }

        $message = trans('validation.unable_get_post');
        return response()->json(
            [
                'message' => $message,
            ],
            400
        );
    }

    public function getPostsByVendor(Request $request)
    {
        $user=Auth::user()->vendor()->first();
        if($user){
            $posts = Post::where('user_id', '=', Auth::user()->id)
                ->activePosts()
                ->with('user', 'files')
                ->where('discount_end','>=',now()->toDateTimeString())
                ->withCount('activatedDeals')
                ->orderBy("created_at", 'DESC')
                ->paginate(10);

            $data = [
                'posts' =>  $posts
            ];

            return response()->json(
                $data,
                200
            );
        }else{
            $data = [
                'posts' => []
            ];

            return response()->json(
                $data,
                200
            ); 
        }
    }

    public function getArchivesByVendor(Request $request)
    {
        $user=Auth::user()->vendor()->first();
        if($user){
            $posts = Post::where('user_id', '=',  Auth::user()->id)->with('user', 'files')
                ->where('discount_end','<=',now()->toDateTimeString())
                ->orderBy("created_at", 'DESC')
                ->paginate(10);

            $data = [
                'posts' =>  $posts
            ];

            return response()->json(
                $data,
                200
            );
        }else{
            $data = [
                'posts' =>  []
            ];

            return response()->json(
                $data,
                200
            ); 
        }
    }

    public function getPostsByVendorId(Request $request,int $id)
    {
        $user=User::find($id)->vendor()->first();
        if($user){
            $posts = Post::where('user_id', '=', $id)
                ->activePosts()
                ->doesntHave('activatedDealsRejected')
                ->with('user', 'files',  'user.vendor')
                ->withCount('activatedDeals')
                ->where('discount_end','>',now()->toDateTimeString())
                ->orderBy("created_at", 'DESC')
                ->paginate(10);

            $check = $this->isFollowing($request, $id);
            $vendor = Vendor::where('user_id', $id)->with('user', 'categories', 'files')->first();
            $vendor['following'] = $check;

            $user = $request->user();
            $count = $user->followers()->count();
            $vendor['followers'] = $count;

            $data = [
                'vendor' => $vendor,
                'posts' =>  $posts
            ];

            return response()->json(
                $data,
                200
            );
        }else{
            $data = [
                'posts' =>  []
            ];

            return response()->json(
                $data,
                200
            ); 
        }
    }

    public function isFollowing(Request $request,int $id)
    {
        $loggedInUser = $request->user();
        $userToFollowCheck = User::where('id', $id)->first();

        $isFollowing = $loggedInUser->isFollowing($userToFollowCheck);

        if ($isFollowing) {
            return true;
        } else {
            return false;
        }
    }

    public function getPostById(Request $request,int $id)
    {
        $post = Post::where('id', '=', $id)
                ->with('user', 'files','user.vendor')
                ->first();
        if ($post) {

            return response()->json([
                $post,
            ], 200);

        } else {
            $message = trans('valdiation.unable_get_postid');
            return response()->json([
                'message' => $message,
            ], 400);
        }
    }

    public function attachExtraFields($post)
    {
        if(Auth::user()->identifier=='1'){
            $postPrice=$post->discount_cost;
            $discountPercentage=$post->discount_amount;
            $discountAmount=($postPrice*$discountPercentage)/100;
            $noComissionAmount=$postPrice-$discountAmount;
            $discountAmount=$discountAmount;
            $discountedAmount=$postPrice-$discountAmount;
        }
        else{
            $postPrice=$post->discount_cost;
            $discountPercentage=$post->discount_amount;
            $discountAmount=($postPrice*$discountPercentage)/100;
            $noComissionAmount=$postPrice-$discountAmount;
            $discountAmount=$discountAmount-($discountAmount*0.2);
            $discountedAmount=$postPrice-$discountAmount;
        }   

        $remaining_time=Carbon::now()->diffInSeconds($post->discount_end, false);
        $human_time=$post->created_at->diffForHumans();
        
        $post->setAttribute(
            'like_count',
            $post->likers(User::class)->count()
        );

        $post->setAttribute(
            'no_comission_price',
            $noComissionAmount
        );

        $post->setAttribute(
            'discount_price',
            $discountAmount
       );

        $post->setAttribute(
            'discounted_price',
            $discountedAmount
        );

        $post->setAttribute(
            'remaining_time',
            $remaining_time
        );
        
        $post->setAttribute(
            'human_time',
            $human_time
         );
    }
}
