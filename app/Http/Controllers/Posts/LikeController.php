<?php
namespace App\Http\Controllers\Posts;

use App\Models\Comment;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\Post;
use App\Notifications\Liked;
use Carbon\Carbon;

class LikeController extends Controller
{
    public function __construct()
    {
    }

    /**
     * Like post
     *
     * @param [int] $id
     * @return Illuminate\Http\JsonResponse
     */
    public function likePost(Request $request, $id) {

        if(!$id) {
            return response()->json([
                'message' => trans('validation.need_id_to_like')
            ], 200);
        }
        // Get the logged in user.
        $user = $request->user();

        // Get the post to like
        $post = Post::where('id', $id)->first();

        if($post) {
            $notifiableVendor=$post->user()->first();
     
            // Like post.
            $likePost = $user->like($post);
            $notifiableVendor->user_name=auth()->user()->name;
            $notifiableVendor->avatar_url=auth()->user()->avatar_url;
            $liked=DB::table('likers')->where('liker_id',auth()->user()->id)->where('likeable_id',$id)->first();
            $human_time=Carbon::createFromTimeString($liked->created_at)->diffForHumans();
            $notifiableVendor->human_time=$human_time;
            $notifiableVendor->post_id=$post->id;
            $notifiableVendor->post_title=$post->title;
            $authFlag= auth()->user()->flag;
            Auth::user()->update([
                'flag' => $notifiableVendor->flag,
            ]);

            if($notifiableVendor->id !== auth()->user()->id){
                $notifiableVendor->notify(new Liked($notifiableVendor));
            }
            Auth::user()->update([
              'flag' => $authFlag,
            ]);
            $message = trans('validation.successful_like');
            if($likePost) {
                return response()->json([
                    'message' => $message
                ], 200);
            } else {
                return response()->json([
                    'message' => trans('validation.unable_like')
                ], 200);
            }

        } else {
            return response()->json([
                'message' => trans('validation.no_post'),
            ], 400);
        }
    }

    /**
     * Unlike post
     *
     * @param [int] $id
     * @return Illuminate\Http\JsonResponse
     */
    public function unLikePost(Request $request, $id) {
        if(!$id) {
            return response()->json([
                'message' => trans('validation.need_id_to_like')
            ], 200);
        }
        // Get the logged in user.
        $user = $request->user();

        // Get the post to like
        $post = Post::where('id', $id)->first();

        if($post) {
            // Unlike post.
            $likePost = $user->unlike($post);

            $message =trans('validation.successful_unlike');
            if($likePost) {
                return response()->json([
                    'message' => $message
                ], 200);
            } else {
                return response()->json([
                    'message' => trans('validation.unable_like')
                ], 200);
            }

        } else {
            return response()->json([
                'message' => trans('validation.no_post'),
            ], 400);
        }
    }


    /**
     * show posts user likes
     *
     * @return Illuminate\Http\JsonResponse
     */
    public function showUserLikedPosts(Request $request) {
        // Get the logged in user.
        $user = $request->user();

        if(!$user) {
            return response()->json([
                'message' => trans('validation.something_went_wrong'),
            ], 400);
        }

        // Posts the user likes.
        $postsUserLikes = Post::likedBy($user)->with('user', 'files', 'comments', 'user.vendor')
        // $postsUserLikes = Post::with('user', 'files', 'comments', 'user.vendor')
        ->withCount('activatedDeals')
        ->get();

        if($postsUserLikes->isEmpty()) {
            return response()->json([], 200);
        }

        return response()->json($postsUserLikes, 200);
    }

    /**
     * get post like count
     *
     * @return Illuminate\Http\JsonResponse
     */
    public function postLikeCount(Request $request, $id) {
     $count = DB::select('SELECT count(likeable_id) as total FROM `likers` WHERE likeable_type LIKE "%Post%"
        AND likeable_id = ?', [$id]);

        if($count) {
            return response()->json([
                $count[0]->total
            ], 200);
        } else {
            return response()->json([
                $count[0]->total
            ], 400);
        }
    }


    /**
     *  Like comment functions
     */

    /**
     * Like Comment
     *
     * @param [int] $id
     * @return Illuminate\Http\JsonResponse
     */
    public function likeComment(Request $request, $id) {

        if(!$id) {
            return response()->json([
                'message' => trans('validation.need_id_to_like')
            ], 200);
        }
        // Get the logged in user.
        $user = $request->user();

        // Get the post to like
        $comment = Comment::where('id', $id)->first();

        if($comment) {
            // Like post.
            $likeComment = $user->like($comment);

            $message =  trans('validation.successful_like');
            if($likeComment) {
                return response()->json([
                    'message' => $message
                ], 200);
            } else {
                return response()->json([
                    'message' => trans('validation.unable_like')
                ], 200);
            }

        } else {
            return response()->json([
                'message' =>trans('validation.no_post'),
            ], 400);
        }
    }

    /**
     * Unlike Comment
     *
     * @param [int] $id
     * @return Illuminate\Http\JsonResponse
     */
    public function unLikeComment(Request $request, $id) {
        if(!$id) {
            return response()->json([
                'message' => trans('validation.need_id_to_like')
            ], 200);
        }
        // Get the logged in user.
        $user = $request->user();

        // Get the post to like
        $comment = Comment::where('id', $id)->first();

        if($comment) {
            // Unlike comment.
            $unLikeComment = $user->unlike($comment);

            $message = trans('validation.unable_like');
            if($unLikeComment) {
                return response()->json([
                    'message' => $message
                ], 200);
            } else {
                return response()->json([
                    'message' =>  trans('validation.unable_like')
                ], 200);
            }

        } else {
            return response()->json([
                'message' => trans('validation.no_post'),
            ], 400);
        }
    }

    /**
     * get comment like count
     *
     * @return Illuminate\Http\JsonResponse
     */
    public function commentLikeCount(Request $request, $id) {
        $count = DB::select('SELECT count(likeable_id) as total FROM `likers` WHERE likeable_type LIKE "%Comment%"
        AND likeable_id = ?', [$id]);
        if($count) {
            return response()->json($count[0]->total, 200);
        } else {
            return response()->json([
                'count' => $count[0]->total
            ], 400);
        }
    }




}
