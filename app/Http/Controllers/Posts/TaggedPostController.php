<?php
namespace App\Http\Controllers\Posts;

use App\Models\Comment;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\TaggedPosts;
use Carbon\Carbon;
use App\Models\Post;

class TaggedPostController extends Controller
{
    public function __construct()
    {
    }

    /**
     * tag user to post post
     *
     * @return Illuminate\Http\JsonResponse
     */
    public function tag(Request $request) {
        $request->validate([
            'post_id' => 'required',
            'user_tagged_id' => 'required',
        ]);

        $post_id = $request->post_id;
        $users_to_tag_ids = explode(',', $request->user_tagged_id);

        foreach ($users_to_tag_ids as $user_tag_id) {
          $comment = new TaggedPosts([
              'post_id' => $post_id,
              'user_id' => Auth::user()->id,
              'user_tagged_id' => $user_tag_id,
          ]);
          $comment->save();
        }

        if($comment) {
            return response()->json([
                'message' =>trans('validation.successful_tagged')
            ], 200);
        } else {
            return response()->json([
                'message' => trans('validation.unable_tagged')
            ], 400);
        }
    }


    /**
     * Deleted tagged user
     *
     * @return Illuminate\Http\JsonResponse
     */
    public function deleteTag(Request $request) {

        $request->validate([
            'post_id' => 'required',
            'user_tagged_id' => 'required',
        ]);

        $post_id = $request->post_id;
        $users_to_tag_ids = explode(',', $request->user_tagged_id);

        foreach ($users_to_tag_ids as $user_tag_id) {
          $delete = TaggedPosts::where('post_id', '=', $post_id)
                      ->where('user_tagged_id', '=', $user_tag_id)
                      ->delete();
        }

        if($delete) {
            return response()->json([
                'message' => trans('validation.successful_removed_tagged')
            ], 200);
        } else {
            return response()->json([
                'message' => trans('validation.unable_removed_tagged')
            ], 400);
        }
    }


    /**
     * untag user to  post
     *
     * @param [int] $id
     * @return Illuminate\Http\JsonResponse
     */
    public function untag(Request $request, $id) {
        $removeTaggedUser = TaggedPosts::find($id);

        if(!$removeTaggedUser) {
            return response()->json([
                'message' => trans('validation.unable_tag_with_id')
            ], 404);
        }

        if(!$removeTaggedUser->delete()) {
             return response()->json([
                'message' => trans('validation.unable_tag')
            ], 400);
        }

        return response()->json([
                'message' => trans('validation.successfuly_tag')
            ], 200);
    }

     /**
     * show posts user likes
     *
     * @return Illuminate\Http\JsonResponse
     */
    public function showUserTaggedPosts(Request $request) {
        // Posts the user likes.
        $userTaggedPosts = Post::with('files', 'comments', 'user', 'tags', 'tags.tagged', 'tags.user', 'user.vendor')
        ->withCount('activatedDeals')
        // ->whereHas(
        //     'tags',
        //     function($q) use ($request) {
        //         return $q->where('user_tagged_id', '=', $request->user()->id);
        //     }
        // )
        ->whereHas('activatedDeals', function($query) use ($request) {
            return $query->where('user_id',  $request->user()->id);
        })
        ->get();

        if($userTaggedPosts) {
            return response()->json(
                $userTaggedPosts
            , 200);
        } else {
            return response()->json([
                'message' => trans('validation.no_tags')
            ], 200);
            }
    }
}
