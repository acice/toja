<?php

namespace App\Http\Controllers\Vendor;

use App\Models\File;
use App\Models\User;
use App\Models\Vendor;
use App\GoogleTranslate;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

/**
 * VendorController
 */
class VendorController extends Controller
{

    /**
     * Get selected categories of vendor
     *
     * @return \Illuminate\Support\Collection
     */
    public function getVendorSelectedCategories()
    {
        $nestedCategories = Category::nested()->get();
        $user             = User::GetVendor(
            Auth::user()->id
        )->with('vendor.categories')->first();

        if ($user->vendor->categories->count() === 0) {
            foreach ($nestedCategories as $parentKey => $nestedCategory) {
                $nestedCategories[$parentKey]['selected'] = false;

                if (!empty($nestedCategory['child'])) {
                    foreach ($nestedCategory['child'] as $childKey => $child) {
                        $nestedCategories[$parentKey]['child'][$childKey]['selected'] = false;
                    }
                }
            }

            return $nestedCategories;
        }

        $user->vendor->categories->map(
            function ($category) use (&$nestedCategories) {
                foreach ($nestedCategories as $parentKey => $nestedCategory) {
                    if ($nestedCategory['id'] == $category['id']) {
                        $nestedCategories[$parentKey]['selected'] = true;
                    } else if (!array_key_exists("selected", $nestedCategories[$parentKey])) {
                        $nestedCategories[$parentKey]['selected'] = false;
                    }

                    if (!empty($nestedCategory['child'])) {
                        foreach ($nestedCategory['child'] as $childKey => $child) {
                            if ($child['id'] == $category['id']) {
                                $nestedCategories[$parentKey]['child'][$childKey]['selected'] = true;
                            } else if (!array_key_exists("selected", $nestedCategories[$parentKey]['child'][$childKey])) {
                                $nestedCategories[$parentKey]['child'][$childKey]['selected'] = false;
                            }
                        }
                    }
                }
            }
        );

        return $nestedCategories;
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getLoggedInVendorDetails(Request $request)
    {
        $userId = Auth::user()->id;
        $vendor = User::GetVendor($userId)->with(
            [
                'vendor',
                'vendor.bank',
                'vendor.categories',
                'vendor.customTime',
            ]
        )->first();

        $followersCount = 0;
        $postCount =0;
        if($vendor->followers()){
            $followersCount = $vendor->followers()->count();
        }
        if($vendor->posts()){
            $postCount = $vendor->posts()->count();
        }

        $check               = $this->isFollowing($request, $userId);
        $vendor['following'] = $check;
        $vendor['followers'] = $followersCount;
        $vendor['post_count'] = $postCount;

        if ($vendor) {
            return response()->json(
                [
                    'vendor'              => $vendor,
                    'selected_categories' => $this->getVendorSelectedCategories(),
                ],
                200
            );
        }

        return response()->json(
            [
                'message' => trans('validation.unable_get_all_user'),
            ],
            400
        );
    }

    

    /* Get Vendor by ID */
    public function getVendor(Request $request,int $id)
    {   
        try {
            $userId = $id;
            $vendor = User::GetVendor($userId)->with(
                [
                    'vendor',
                    'vendor.customTime',
                ]
            )->first();
            if($vendor){
                $user           = Auth::user();
                $followersCount = 0;
                $postCount = 0;
                if($vendor->followers()){
                    $followersCount = $vendor->followers()->count();
                }
                if($vendor->posts()){
                    $postCount = $vendor->posts()->count();
                }
                $following=$user->isFollowing($vendor);
                $vendor['followers'] = $followersCount;
                $vendor['following'] = $following;
                $vendor['post_count'] = $postCount;
            }
        } catch (\Exception $e) {
                return response()->json([
                    'message' => $e->getMessage(),500
                ]);
        }    
        
        if ($vendor) {
            return response()->json(
                [
                    'vendor' => $vendor
                ],
                200
            );
        }

        return response()->json(
            [
                'message' => trans('validation.unable_get_vendor'),
            ],
            400
        );
    }

      //files for vendor profile
      public function getImages(Request $request)
      {
            $vendorID = Auth::user()->vendor()->first()->id;
            $vendorfiles = File::select(['id', 'file','user_id','post_id'])->where('vendor_id',$vendorID)->orderBy('created_at','DESC')->paginate(12);
    
            if ($vendorfiles) {
                return response()->json($vendorfiles, 200);
            }
            
            return response()->json(
                [
                    'message' => trans('validation.unable_get_vendor'),
                ],
                400
            );
      }

      public function getImagesByVendorID(Request $request,int $id)
      {
            $vendorID = $id;
            $vendorfiles = File::select(['id', 'file','user_id','post_id'])->where('vendor_id',$vendorID)->orderBy('created_at','DESC')->paginate(12);
  
            if ($vendorfiles) {
                return response()->json($vendorfiles, 200);
            }
            
            return response()->json(
                [
                    'message' => trans('validation.unable_get_vendor'),
                ],
                400
            );
      }

    /**
     * Check if logged-in user follows the user with provided id
     *
     * @param Request $request
     * @param int     $id
     *
     * @return bool
     */
    public function isFollowing(Request $request,int $id)
    {
        $loggedInUser      = $request->user();
        $userToFollowCheck = User::where('id', $id)->first();

        $isFollowing = $loggedInUser->isFollowing($userToFollowCheck);

        if ($isFollowing) {
            return true;
        }

        return false;
    }

    /**
     * Get list of vendors
     * 
     * @param Request $request
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function getVendors(Request $request)
    {
        $categories       = explode(',', $request->input('category_ids')); //[2, 1];
        $distance         = $request->input('distance');
        $long             = $request->input('longitude');
        $lat              = $request->input('latitude');
        $search     = '%' . $request->input('input_search') . '%';

        $vendors = Vendor::with(
            'categories',
            'files',
            'user'
        )
            ->whereHas(
                'user',
                function ($q) use ($request) {
                    return $q->where('active', '=', 1);
                }
            );

        if ($request->input('category_ids')) {
            $vendors = $vendors->whereHas(
                'categories',
                function ($query) use ($categories) {
                    $query->whereIn('category_id', $categories);
                }
            );
        }


        if ($request->has('input_search')) {
            $vendors->where('name', 'like', $search)
                ->orWhere('body', 'like', $search);
        }

        if ($lat && $long) {
            if ($distance) {
                $vendors = $vendors->distance($lat, $long)
                    ->havingRaw('distance < ?', [$distance])
                    ->orderBy('distance', 'ASC')
                    ->with('files', 'categories');
            } else {
                $vendors = $vendors->distance($lat, $long)
                    ->orderBy('distance', 'ASC')
                    ->with('files', 'categories');
            }
        }

        $vendors = $vendors->get();

        if ($vendors) {
            return response()->json($vendors, 200);
        }

        return response()->json(
            [
                'message' => trans('validation.something_went_wrong'),
            ],
            400
        );
    }

    /**
     * Search vendor by name
     * 
     * @param Request $request
     * 
     * @return [type]
     */
    public function getVendorByName(Request $request)
    {
        $name = '%' . $request->input('name') . '%';
        $body = '%' . $request->input('body') . '%';

        $vendors = Vendor::where('name', 'like', $name)->with('categories', 'files');
        if ($request->has('body')) {
            $vendors->Orwhere('body', 'like', $body);
        }

        $vendors = $vendors->get();

        if ($vendors) {
            return response()->json($vendors, 200);
        } else {
            return response()->json(
                [
                    'message' =>trans('validation.something_went_wrong'),
                ],
                400
            );
        }
    }

    public function addVendorImages(Request $request)
    {
        $request->validate([
            'images.*' => 'nullable|mimes:jpeg,jpg,png|max:6000'
        ]);
        
        $vendor = Vendor::where('user_id', Auth::user()->id)->first();

        if ($request->hasFile('images')) {
            $this->addVendorImage($request, $vendor->id);
        }

        $message = trans('validation.successful_upload_image');
        if ($vendor) {
            return response()->json(
                [
                    'message' => $message,
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => trans('validation.something_went_wrong'),
                ],
                400
            );
        }
    }

    public function updateVendorDetails(Request $request)
    {
        $update = Vendor::where('user_id', Auth::user()->id)->first();
        if ($request->has('name')) {
            $update->update(
                [
                    'name'           => $request->input('name'),
                    'body'           => $request->input('body'),
                    'postcode'       => $request->input('postcode'),
                    'company_number' => $request->input('company_number'),
                    'data'           => json_encode(
                        [
                            'name'           => (new GoogleTranslate)->translate($request->input('name'), 'iw')['text'],
                            'body'           => (new GoogleTranslate)->translate($request->input('body'), 'iw')['text'],
                            'postcode'       => (new GoogleTranslate)->translate($request->input('postcode'), 'iw')['text'],
                            'company_number' => (new GoogleTranslate)->translate($request->input('company_number'), 'iw')['text'],
                        ]
                    ),
                ]
            );
        }



        $message = trans('valdiation.successfuly_updated');
        if ($update) {
            return response()->json(
                [
                    'message' => $message,
                ],
                200
            );
        } else {
            return response()->json(
                [
                    'message' => trans('valdiation.unable_to_update'),
                ],
                400
            );
        }
    }

    /**
     * Create image for vendors
     * 
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\JsonResponse
     */
    public function addVendorImage(Request $request,int $id)
    {
        $file               = '';
        $maximumImageUpload = 1000;
        $imageCount         = DB::table('files')
            ->where('vendor_id', '=', $id)
            ->count();

        if ($imageCount > $maximumImageUpload) {
            return response()->json(
                [
                    'message' => trans('validation.max_image_upload'). $maximumImageUpload,
                ],
                200
            );
        }

        if ($request->hasFile('images')) {
            
            $images = $request->file('images');
            foreach ($images as $image) {
                $fileName = mt_rand() . time() . '.jpg';

                $img = Image::make($image->getRealPath());
                $img->resize(
                    450,
                    450,
                    function ($constraint) {
                        $constraint->aspectRatio();
                    }
                );

                $img->stream('jpg',80); // <-- Key point
                Storage::disk('public')
                    ->put(
                        'vendor/images/' . Auth::user()->id . '/' . $fileName,
                        $img,
                        'public'
                    );

                $file            = new File;
                $file->file      = $fileName;
                $file->user_id   = Auth::user()->id;
                $file->vendor_id = $id;
                $file->save();
            }

            $message = trans('validation.successful_upload_image');
            if ($file) {
                return response()->json(
                    [
                        'message' => $message,
                    ],
                    200
                );
            }

            return response()->json(
                [
                    'message' =>  trans('validation.unable_upload_image'),
                ],
                400
            );
        }
    }

    /**
     * Update the vendors profile image
     * 
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\JsonResponse
     */
    public function updateVendorAvatar(Request $request)
    {
        $update = '';

        if ($request->hasFile('avatar')) {
            $images = $request->file('avatar');
            foreach ($images as $image) {
                $fileName = mt_rand() . time() . '.jpg';

                $img = Image::make($image->getRealPath());
                $img->resize(
                    450,
                    450,
                    function ($constraint) {
                        $constraint->aspectRatio();
                    }
                );

                $img->stream('jpg',80); // <-- Key point
                Storage::disk('public')->put(
                    'avatars/' . Auth::user()->id . '/' . $fileName,
                    $img,
                    'public'
                );

                $update = User::where('id', Auth::user()->id)
                    ->update(
                        [
                            'avatar' => $fileName,
                        ]
                    );
            }

            $message =trans('validation.updated_profile');

            if ($update) {
                return response()->json(
                    [
                        'message' => $message,
                    ],
                    200
                );
            }

            return response()->json(
                [
                    'message' =>trans('validation.unable_updated_profile'),
                ],
                400
            );
        }
    }

    /**
     * Remove vendor image
     * 
     * @param Request $request
     * @param mixed $id
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function removeVendorImage(Request $request,int $id)
    {
        $file = File::where('id', '=', $id)->first();
        // Remove file from dir
        $remove = Storage::delete(
            'public/vendor/images/' . Auth::user()->id . '/' . $file->file
        );

        // Remove file from table
        $delete = File::where('id', $id)->delete();

        $message = trans('validation.successful_removed_image');
        if ($delete) {
            return response()->json(
                [
                    'message' => $message,
                ],
                200
            );
        }

        return response()->json(
            [
                'message' =>  trans('validation.unable_removed_image'),
            ],
            400
        );
    }
}
