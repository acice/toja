<?php
namespace App\Http\Controllers\Category;

use App\Http\Controllers\Controller;

use App\Models\Category;

class VendorCategoryController extends Controller
{

    public function __construct()
    {
    }


    /* Get all categories */
    public function categories() {
        $categories = Category::nested()->get();

        if($categories) {
            return response()->json($categories, 200);
        } else {
            return response()->json([
                'message' => trans('validation.unable_get_category')
            ], 400);
        }
    }
}
