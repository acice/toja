<?php
namespace App\Http\Controllers\Category;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\CategoryLink;
use Illuminate\Support\Facades\Auth;
use App\Models\Vendor;
use App\Models\User;

class CategoryController extends Controller
{

    public function __construct()
    {
        
    }

    /* Get all categories */
    public function categories() {
        $categories = Category::nested()->get();

        if($categories) {
            return response()->json($categories, 200);
        } else {
            return response()->json([
                'message' => trans('validation.unable_get_category')
            ], 400);
        }
    }

    // Add user categories
    public function addCategories(CategoryLink $category, Request $request) {

        if(Auth::user()->identifier == 0) {
                $authId = Auth::user()->id;
                $category->removeUserCategories($authId);

            } else {
                $vendorId =  Vendor::where('user_id', Auth::user()->id)->first()->id;
                $delete = $category->removeVendorCategories($vendorId);
        }
      
        $category_ids = explode(',', $request->input('category_ids'));

        if($category_ids[0] == "") {
            return response()->json([
                'message' => trans('validation.select_one_category')
            ], 400);
        }

        foreach ($category_ids as $category_id) {
            $categoryLink = new CategoryLink();
            if($categoryLink->where('category_id', '=', $category_id)
                                ->where('user_id', '=', Auth::user()->id)->first()) {
                continue;
            }

            if(Auth::user()->identifier == 0) {
                $categoryLink->user_id = $authId;
            } else {
                 $categoryLink->vendor_id = $vendorId;
            }
        
            $categoryLink->category_id = $category_id;
            $categoryLink->save();
        }
        
        $message = trans('validation.added_category');
        if($categoryLink) {
            return response()->json([
                'message' => $message
            ], 200);
        } else {
            return response()->json([
                'message' => trans('validation.unable_add_category')
            ], 400);
        }
    }

    // Update user categories
    public function updateCategories(Request $request, $id) {
        $update = CategoryLink::where('id', $id)
            ->update([
                'category_id' => $request->input('category_id')
            ]);
        
        $message = trans('validation.update_category_preferences');
        if($update) {
            return response()->json([
                'message' => $message
            ], 200);
        } else {
            return response()->json([
                'message' =>  trans('validation.unable_update_category')
            ], 400);
        }
    }

    
    /* Get With vendor categories */
    public function categoriesWithVendors(Request $request) {
        $categories = Category::where('parent_id',0)->with('latestVendors.vendor.user')->paginate(5);
        if($categories) {
            return response()->json($categories, 200);
        } else {
            return response()->json([
                'message' => trans('validation.unable_get_category')
            ], 400);
        }
    }

    /* Get  categories Vendors by ID*/
    public function categoriesById(Request $request,$id) {
        $input_search = '%' . $request->input('search') . '%';
        $category = Category::find($id);
        $vendors=$category->vendors()->with('user');
        if ($request->has('search')) {
            $vendors=  $vendors->where(function($q) use ( $input_search ){
                $q->where('name', 'like',$input_search);
                $q->orWhere('body', 'like', $input_search);
            });
        }
        $vendors= $vendors->paginate(10);
        if($category) {
            $data=[
                'category'=> $category,
                'vendors'=> $vendors,

            ];
            return response()->json($data, 200);
        } else {
            return response()->json([
                'message' => trans('validation.unable_get_category')
            ], 400);
        }
    }
}
