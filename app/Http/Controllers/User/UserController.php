<?php
namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\File;
use Illuminate\Support\Facades\Auth;
use App\Models\Vendor;
use Illuminate\Http\Request;
use App\Models\Category;
use Illuminate\Support\Arr;

use App\Models\User;
use App\Models\Notifications;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;
use App\Notifications\DealActivated;

class UserController extends Controller
{

    public function __construct()
    {
    }
  
    public function getUserSelectedCategories()
    {
        $nestedCategories = Category::nested()->get();
        $user = User::GetUser(Auth::user()->id)->with('categories')->first();
      
        if($user->categories->count() === 0) {
           foreach ($nestedCategories as $parentKey => $nestedCategory) {
                 $nestedCategories[$parentKey]['selected'] = false;

                if (!empty($nestedCategory['child'])) {
                    foreach ($nestedCategory['child'] as $childKey => $child) {
                        $nestedCategories[$parentKey]['child'][$childKey]['selected'] = false;
                    }
                }
            }

           return $nestedCategories;
        }
      
        $user->categories->map(function ($category) use (&$nestedCategories) {
            foreach ($nestedCategories as $parentKey => $nestedCategory) {

                if ($nestedCategory['id'] == $category['id']) {
                     $nestedCategories[$parentKey]['selected'] = true;
                } else if (!array_key_exists("selected",  $nestedCategories[$parentKey])) {
                     $nestedCategories[$parentKey]['selected'] = false;
                }

                if (!empty($nestedCategory['child'])) {
                    foreach ($nestedCategory['child'] as $childKey => $child) {
                        if ($child['id'] == $category['id']) {
                            $nestedCategories[$parentKey]['child'][$childKey]['selected'] = true;
                        } else if (!array_key_exists("selected", $nestedCategories[$parentKey]['child'][$childKey])) {
                             $nestedCategories[$parentKey]['child'][$childKey]['selected'] = false;
                        }
                    }
                }
            }
        });

        return $nestedCategories;
    }

    public function getUser(Request $request) {
        $user = User::GetUser(Auth::user()->id)->with('categories', 'files')->first();
        if($user) {
            $followingCount = 0;
            if($user->following()){
                $followingCount = $user->following()->count();
            }
            $user['following_count'] = $followingCount;
            return response()->json(
              [
                'user' => $user,
                'selected_categories' => $this->getUserSelectedCategories()
              ], 200);
        } else {
            return response()->json([
                'message' => trans('validation.only_for_users'),
            ], 400);
        }
    }
  
    public function getUsersByNameOrEmail(Request $request) {
        $users = User::getUsersByNameOrEmail($request)->with('categories' )->get();

        if($users) {
            return response()->json($users, 200);
        } else {
            return response()->json([
                'message' => trans('validation.unable_get_vendor'),
            ], 400);
        }
    }

    public function updateFlag(User $user, $flag) {
        return $user->updateFlag($flag);
    }

    public function getNotifications() {
        $user=Auth::user();
        $notifications=Notifications::where('notifiable_id',$user->id)->latest()->paginate(10);
        foreach ($notifications as $key => $notification) {
            $human_time=$notification->created_at->diffForHumans();
            $data=json_encode($notification->data[0]);
            $formated_data=json_decode($data);
            
            $notification->offsetUnset('notifiable_type');
            $notification->offsetUnset('type');
            $notification->offsetUnset('notifiable_id');
            if(!empty($formated_data)){
            $date_formated=$notification->created_at->format("F j, Y, g:i a");
            $notification->setAttribute(
                'created_at',
                $notification->created_at
            );
            $notification->setAttribute(
                'time',
                $human_time
            );
            $notification->setAttribute(
                'deal_id',
                !empty($formated_data->id)?$formated_data->id:''
            );
            $notification->setAttribute(
                'status',
                !empty($formated_data->status)?$formated_data->status:''
            );
            $notification->setAttribute(
                'preference',
                !empty($formated_data->preference)?$formated_data->preference:''
            );
            $notification->setAttribute(
                'invoice_no',
                !empty($formated_data->invoice_no)?$formated_data->invoice_no:''
            );
            $notification->setAttribute(
                'reject_reason',
                !empty($formated_data->reject_reason)?$formated_data->reject_reason:''
            );

            $notification->setAttribute(
                'type',
                !empty($formated_data->type)?$formated_data->type:''
            );

            $notification->setAttribute(
                'vendor_name',
                !empty($formated_data->vendor_name)?$formated_data->vendor_name:''
            );

            $notification->setAttribute(
                'vendor_avatar_url',
                !empty($formated_data->vendor_avatar_url)?$formated_data->vendor_avatar_url:''
            );

            if($formated_data->type!='FOLLOWED'){
                $notification->setAttribute(
                    'post',
                    !empty($formated_data->post)?$formated_data->post:''
                );
            }
            
            $notification->setAttribute(
                'user',
                !empty($formated_data->user)?$formated_data->user:''
            );
            $notification->setAttribute(
                'payment',
                !empty($formated_data->payment)?$formated_data->payment:''
            );
          
            $notification->offsetUnset('data');
            }
        }
        return response()->json($notifications, 200);
    }

    public function markAsReadNotification($id) {
        $user=Auth::user();
        $notification=$user->notifications()->where('id',$id)->first();
        $notification->markAsRead();
        return response()->json([
            'message' => trans('validation.notification_read')
        ], 200);
    }

    public function markAllAsReadNotification() {
        $user=Auth::user();
        $notification=$user->notifications->markAsRead();
        
        return response()->json([
            'message' => trans('validation.notification_all_read')
        ], 200);
    }

}
