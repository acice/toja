<?php

namespace App\Http\Controllers\Social;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UnFollowController extends Controller
{

    public function __construct()
    {

    }

    /**
     * UnFollow User
     *
     * @param  [Request] $request.
     * @param  [int] $id.
     *
     * @return [string] message
     */
    public function unFollowUser(Request $request, $id) {

        $loggedInUser = $request->user();
        $userToUnfollow =  User::where('id', $id)->first();

        $unFollow = $loggedInUser->unfollow($userToUnfollow);
        
        $message = trans('validation.unfollowed');
        if($unFollow) {
            return response()->json([
                'message' => $message . $userToUnfollow->name
            ], 200);
        } else {
            return response()->json([
                'message' => trans('validation.unfollowed'). $userToUnfollow->name
            ], 400);
        }
    }
}
