<?php

namespace App\Http\Controllers\Social;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Vendor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Notifications\Followed;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class FollowController extends Controller
{
    public function __construct()
    {
    }

    /**
     * Follow User
     *
     * @param [Request] $request.
     * @param [int]     $id.
     *
     * @return [string] message
     */
    public function followUser(Request $request, $id)
    {
        $loggedInUser = $request->user();
        $userToFollow =  User::where('id', $id)->first();
        $userToFollow->user_name=auth()->user()->name;
        $userToFollow->avatar_url=auth()->user()->avatar_url;
        $follow = $loggedInUser->follow($userToFollow);
        $following=DB::table('followers')->where('follower_id',auth()->user()->id)->where('followable_id',$id)->first();
        $human_time=Carbon::createFromTimeString($following->created_at)->diffForHumans();
        $userToFollow->human_time=$human_time;
        $authFlag= auth()->user()->flag;
        Auth::user()->update([
            'flag' => $userToFollow->flag,
        ]);
        $userToFollow->notify(new Followed($userToFollow));
        Auth::user()->update([
          'flag' => $authFlag,
        ]);
        
        $message = trans('validation.followed');
        if ($follow) {
            return response()->json(
                [
                    'message' => $message . $userToFollow->name
                ],
                200
            );
        } else {
            return response()->json(
                [
                    'message' => trans('validation.unable_followed'). $userToFollow->name
                ],
                400
            );
        }
    }

    /**
     * Show who the user is following.
     *
     * @param  [Request] $request.
     * @return [Array] $following
     */
    public function following(Request $request)
    {
        $user=Auth::user();
        $longitude = $request->input('longitude');
        $latitude = $request->input('latitude');
        $input_search = '%' . $request->input('input_search') . '%';

        $user_ids = $user->following()->pluck('followable_id')->toArray();
        $vendors=Vendor::whereIn('user_id',$user_ids)->whereHas('user')->with('files','user');
        if ($user->has('categories')) {
            $category_ids=$user->categories()->pluck('category_id')->toArray();
            $vendors = $vendors->whereHas(
                'categories',
                function ($query) use ($category_ids) {
                    $query->whereIn('category_id', $category_ids);
                }
            );
        }


        if ($request->has('input_search')) {
            $vendors->where('name', 'like', $input_search)->orWhere('body', 'like', $input_search);
        }


        if ($latitude && $longitude) {
            $vendors = $vendors->geofence($latitude, $longitude,0,Auth::user()->distance)->orderBy('distance', 'ASC');
        }

        $vendors = $vendors->simplePaginate(20);

        if ($vendors) {
            return response()->json($vendors, 200);
        } else {
            return response()->json(
                [
                    'message' => trans('validation.something_went_wrong'),
                ],
                400
            );
        }
    }


    /**
     * Show who the user isn't following
     *
     * @param  [Request] $request.
     * @return [Array] $notFollowing
     */
    public function notFollowing(Request $request)
    {  
        $user=Auth::user();
        $longitude =$request->input('longitude');
        $latitude = $request->input('latitude');
        $input_search = '%' . $request->input('input_search') . '%';
        $user_ids = $user->following()->pluck('followable_id')->toArray();
        $vendors=Vendor::whereNotIn('user_id',$user_ids)->whereHas('user')->with('files','user');
        
        if ($user->has('categories') ){
            
            $category_ids=$user->categories()->pluck('category_id')->toArray();
            
            $vendors = $vendors->whereHas(
                'categories',
                function ($query) use ($category_ids) {
                    $query->whereIn('category_id', $category_ids);
                }
            );
        }
  
        if ($request->has('input_search')) {
            $vendors->where('name', 'like', $input_search)->orWhere('body', 'like', $input_search);
        }


        if ($latitude && $longitude) {
            $vendors = $vendors->geofence($latitude, $longitude,0,Auth::user()->distance)->orderBy('distance', 'ASC');
        }
        
        $vendors = $vendors->simplePaginate(20);

        if ($vendors) {
            return response()->json($vendors, 200);
        } else {
            return response()->json(
                [
                    'message' =>  trans('validation.something_went_wrong'),
                ],
                400
            );
        }
    }

    /**
     * How many users the user is following
     *
     * @param [Request] $request.
     */
    public function followingCount(Request $request)
    {
        $user = $request->user();
        $count = $user->following()->count();

        if ($count) {
            return response()->json(
                [
                    'user' => $user->name,
                    'count' => $count
                ],
                200
            );
        } else {
            return response()->json(
                [
                    'user' => $user->name,
                    'count' => $count
                ],
                400
            );
        }
    }
    /**
     * How many users are following the user
     *
     * @param [Request] $request.
     */
    public function followersCount(Request $request)
    {
        $user = $request->user();
        $count = $user->followers()->count();

        if ($count) {
            return response()->json(
                [
                    'user' => $user->name,
                    'count' => $count
                ],
                200
            );
        } else {
            return response()->json(
                [
                    'user' => $user->name,
                    'count' => $count
                ],
                400
            );
        }
    }
}
