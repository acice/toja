<?php
namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\File;
use App\Models\Post;
use App\Models\User;
use App\Models\Vendor;
use App\Models\Invoice;
use App\GoogleTranslate;
use App\Models\DiscountDay;
use Illuminate\Http\Request;
use App\Models\ActivatedDeals;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class InvoiceController extends Controller
{
    public function index()
    {
        return view('vendor.invoices.show-invoices');
    }
    
    public function getAllInvoices() {
        $invoices =  Invoice::all();
        
        return response()->json(
            $invoices
            , 200);
    }
    
    public function updateInvoicePaid($id) {
        
        $invoice = Invoice::findOrFail($id);
 
        if ($invoice->piad === 1) {
           return  $invoice->update(['paid' => 0]);
        } 
        
        $invoice->update(['paid' => 1]);       
    }
}
