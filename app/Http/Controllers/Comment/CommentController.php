<?php
namespace App\Http\Controllers\Comment;

use App\GoogleTranslate;
use App\Http\Controllers\Controller;
use App\Models\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Notifications\CommentCreated;
use App\Notifications\CommentReply;

class CommentController extends Controller
{

    public function __construct()
    {
    }

    public function addCommentToPost(Request $request, $id)
    {
        $request->validate([
            'body' => 'required|string|max:190',
        ]);

        $parentID = !empty($request->parent_id) ? $request->parent_id : null;
        $comment = new Comment([
            'body' => (new GoogleTranslate)->translate($request->body, 'en')['text'],
            'iw_body' => (new GoogleTranslate)->translate($request->body, 'iw')['text'],
            'user_id' => Auth::user()->id,
            'post_id' => $id,
            'parent_id' => $parentID,
        ]);
        
        $comment->save();

        if($parentID){
            $parent_comment=Comment::find($parentID);
            $post = $comment->post()->first();
            $notifiableVendor = $parent_comment->user()->first();
            $notifiableVendor->customer = $comment->user()->first()->name;
            $notifiableVendor->avatar_url = $comment->user()->first()->avatar_url;
            $notifiableVendor->comment_id = $comment->id;
            $notifiableVendor->post_title =  $post->title;
            $notifiableVendor->human_time =  $post->human_time;
            $notifiableVendor->post_id =  $post->id;
            $notifiableVendor->comment_reply = $parentID;
            if($notifiableVendor->id!=$comment->user()->first()->id){
                $notifiableVendor->notify(new CommentReply($notifiableVendor));
            }
            
        }else{
            $post = $comment->post()->first();
            $notifiableVendor = $comment->post()->first()->user()->first();
            $notifiableVendor->customer = $comment->user()->first()->name;
            $notifiableVendor->avatar_url = $comment->user()->first()->avatar_url;
            $notifiableVendor->comment_id = $comment->id;
            $notifiableVendor->post_title =  $post->title;
            $notifiableVendor->human_time =  $post->human_time;
            $notifiableVendor->post_id =  $post->id;
            if($notifiableVendor->id!=$comment->user()->first()->id){
                $notifiableVendor->notify(new CommentCreated($notifiableVendor));
            }
            
        }

        

        $message = trans('validation.successful_create_comment');
        if ($comment) {
            return response()->json([
                'message' => $message,
                'comment' => $comment
            ], 200);
        } else {
            return response()->json([
                'message' => trans('validation.unable_create_comment'),
            ], 400);
        }
    }

    public function updateComment(Request $request, $id)
    {
        $update = DB::table('comments')->where('id', $id)
            ->update([
                'body' => (new GoogleTranslate)->translate($request->body, 'en')['text'],
                'iw_body' => (new GoogleTranslate)->translate($request->body, 'iw')['text'],
            ]);
  
        $message =trans('validation.successful_update_comment');
        if ($update) {
            return response()->json([
                'message' => $message
            ], 200);
        } else {
            return response()->json([
                'message' =>  trans('validation.unable_update_comment'),
            ], 400);
        }
    }

    public function deleteComment(Request $request, $id)
    {
        $delete = DB::table('comments')->where('id', $id)->delete();
        
        $message = trans('validation.succesful_remove_comment');
        if ($delete) {
            return response()->json([
                'message' => $message,
            ], 200);
        } else {
            return response()->json([
                'message' => trans('validation.unable_remove_comment'),
            ], 400);
        }
    }

    public function getCommentByPost(Request $request, $id)
    {
        $comments= Comment::where('parent_id',NULL)->where('post_id', $id)->with('user:id,name,avatar,identifier')->latest()->paginate(10);
        
        if ($comments) {
            return response()->json($comments, 200);
        } else {
            return response()->json([
                'message' => trans('validation.unable_retrive_comment'),
            ], 400);
        }
    }

    public function getCommentByParent(Request $request, $id)
    {
        $comments= Comment::where('parent_id', $id)->with('user:id,name,avatar,identifier')->latest()->paginate(10);
        if ($comments) {
            return response()->json($comments, 200);
        } else {
            return response()->json([
                'message' =>  trans('validation.unable_retrive_comment'),
            ], 400);
        }
    }

}
