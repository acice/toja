<?php

namespace App\Http\Controllers;

use App\Models\Vendor;
use App\Models\CustomTime;

class UtilityController extends Controller
{
    public function insertDateIntoOld(){
       $vendors=Vendor::whereDoesntHave('CustomTime')->get();
       foreach ($vendors as $key => $vendor) {
            set_time_limit(0);
                //generate custom Time
                $custom_defaults='[
                    {
                        "id": 2,
                        "day": "Sunday",
                        "status": "Open 24 Hrs",
                        "opening": "10:00",
                        "closing": "22:40"
                    },
                    {
                        "id": 3,
                        "day": "Monday",
                        "status": "Open 24 Hrs",
                        "opening": "10:00",
                        "closing": "22:40"
                    },
                    {
                        "id": 4,
                        "day": "Tuesday",
                        "status": "Open 24 Hrs",
                        "opening": "10:00",
                        "closing": "22:40"
                    },
                    {
                        "id": 5,
                        "day": "Wednesday",
                        "status": "Open 24 Hrs",
                        "opening": "10:00",
                        "closing": "22:40"
                    },
                    {
                        "id": 6,
                        "day": "Thursday",
                        "status": "Open 24 Hrs",
                        "opening": "10:00",
                        "closing": "22:40"
                    },
                    {
                        "id": 7,
                        "day": "Friday",
                        "status": "Open 24 Hrs",
                        "opening": "10:00",
                        "closing": "22:40"
                    },
                    {
                        "id": 8,
                        "day": "Saturday",
                        "status": "Closed",
                        "opening": "10:00",
                        "closing": "22:40"
                    }
                ]';
                
                $custom_days = json_decode($custom_defaults, true);
                if(!empty($custom_days)){
                    foreach ($custom_days as $key => $custom_day) {
                        $day=$custom_day['day'];
                        $opening=$custom_day['opening'];
                        $closing=$custom_day['closing'];
                        $status=$custom_day['status'];
        
                        $operatingTimePreference=CustomTime::updateOrCreate([
                            'vendor_id'   => $vendor->id,
                            'day'   => $day,
                        ],[
                            'opening' => $opening,
                            'closing' => $closing,
                            'status'   => $status
                        ]);
        
                    }
                }
       }
    }
    
}
