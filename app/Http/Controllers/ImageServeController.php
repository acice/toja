<?php

namespace App\Http\Controllers;

use App\Models\File as FileModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class ImageServeController extends Controller
{
    /**
     * Receive the request to load and file and download from s3 to local directory to serve file
     * @param  \Illuminate\Http\Request $request
     * @param  string  $image   the path of the file loading
     * @return \Illuminate\Http\Response
     */
    public function getServeImage(Request $request, $imagePath)
    {
//        $exp = explode('/', $imagePath);
//        $imageName = end($exp);
        //if (!$image = FileModel::where('file', '=', $imageName)->first()) {
        //    abort(404);
        //}

        $fileLocal = Storage::disk('public')->exists($imagePath);
        if (!$fileLocal) {
            abort(404);
        }

        $file = Storage::disk('public')->get($imagePath);

        $type = File::mimeType(Storage::disk('public')->getDriver()->getAdapter()->getPathPrefix().$imagePath);
        $response = response($file, 200, ["Content-Type" => $type]);
        return $response;
    }
}
