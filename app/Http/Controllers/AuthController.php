<?php

namespace App\Http\Controllers;

use Avatar;
use Storage;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Vendor;
use App\Models\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Notifications\SignupActivate;
use App\Http\Requests\Auth\SignUpRequest;
use Intervention\Image\ImageManagerStatic as Image;
use NotificationChannels\ExpoPushNotifications\Http\ExpoController;
use DB;
use Socialite;
use Google_Client;
use App\GoogleTranslate;
use App\Models\CustomTime;
use Illuminate\Support\Facades\Log;

class AuthController extends Controller
{

    /**
     * Create user
     * @param SignUpRequest $request
     * @return [string] message
     */
    public function signup(SignUpRequest $request)
    {
        if(!empty($request->provider_id)){
            $user = User::where('provider_id', $request->provider_id)->first();
            if($user){
                return response()->json([
                    'message' => trans('validation.social_already_exists')
                ], 201);
            }
        }
        $message = trans('validation.account_created');

        $user = new User([
            'name' => $request->name,
            'email' => $request->email,
            'password' =>empty($request->provider_id)? bcrypt($request->password):'',
            'identifier' => $request->identifier,
            'activation_token' => str_random(60),
            'active' => !empty($request->provider_id)?1:0,
            'flag' => $request->flag,
            'phone_no' => !empty($request->phone_no)?$request->phone_no:'',
            'provider_id'   => !empty($request->provider_id)?$request->provider_id:'',
            'provider'      => !empty($request->provider)?$request->provider:'',
            'access_token'  => !empty($request->access_token)?$request->access_token:'',
        ]);
        
        $user->save();
        // $avatar = Avatar::create($user->name)->getImageObject()->encode('png');
        // $avatarFileName='avatar.png';
        // Storage::put('public/avatars/' . $user->id . '/'.$avatarFileName, (string) $avatar);
        

        $lat = !empty($request->latitude)?$request->latitude:null;
        $long = !empty($request->longitude)?$request->longitude:null;

        if ($request->identifier == '1') {
            $name=!empty($request->input('name'))?(new GoogleTranslate)->translate($request->input('name'), 'en')['text']:'';
            $body=!empty($request->input('body'))?(new GoogleTranslate)->translate($request->input('body'), 'en')['text']:'';
            $nameIW=!empty($request->input('name'))?(new GoogleTranslate)->translate($request->input('name'), 'iw')['text']:'';
            $bodyIW=!empty($request->input('body'))?(new GoogleTranslate)->translate($request->input('body'), 'iw')['text']:'';
            $postcodeIW=!empty($request->input('postcode'))?(new GoogleTranslate)->translate($request->input('postcode'), 'iw')['text']:'';
            $vendor=Vendor::create([
                'name' => $request->input('name'),
                'iw_name' => $request->input('name'),
                'body' => $body,
                'iw_body' => $bodyIW,
                'company_number' => $request->company_number,
                 'address'        => $request->input('address'),
                'street' => !empty($request->street)?$request->street:null,
                'city' => !empty($request->city)?$request->city:null,
                'postcode' => !empty($request->postcode)?$request->postcode:null,
                'user_id' => $user->id,
                'longitude' => $lat,
                'latitude' => $long,
                'data' => json_encode([
                    'name' => $nameIW,
                    'body' =>$bodyIW,
                    'address' =>'',
                    'postcode' =>$postcodeIW
                ])
            ]);

        //generate custom Time
        $custom_defaults='[
            {
                "id": 2,
                "day": "Sunday",
                "status": "Open",
                "opening": "10:00",
                "closing": "22:40"
            },
            {
                "id": 3,
                "day": "Monday",
                "status": "Open",
                "opening": "10:00",
                "closing": "22:40"
            },
            {
                "id": 4,
                "day": "Tuesday",
                "status": "Open",
                "opening": "10:00",
                "closing": "22:40"
            },
            {
                "id": 5,
                "day": "Wednesday",
                "status": "Open",
                "opening": "10:00",
                "closing": "22:40"
            },
            {
                "id": 6,
                "day": "Thursday",
                "status": "Open",
                "opening": "10:00",
                "closing": "22:40"
            },
            {
                "id": 7,
                "day": "Friday",
                "status": "Open",
                "opening": "10:00",
                "closing": "22:40"
            },
            {
                "id": 8,
                "day": "Saturday",
                "status": "Closed",
                "opening": "10:00",
                "closing": "22:40"
            }
        ]';
        $custom_days = json_decode($custom_defaults, true);
        if(!empty($custom_days)){
            foreach ($custom_days as $key => $custom_day) {
                $day=$custom_day['day'];
                $opening=$custom_day['opening'];
                $closing=$custom_day['closing'];
                $status=$custom_day['status'];

                $operatingTimePreference=CustomTime::updateOrCreate([
                    'vendor_id'   => $vendor->id,
                    'day'   => $day,
                ],[
                    'opening' => $opening,
                    'closing' => $closing,
                    'status'   => $status
                ]);

            }
        }
        }

        if(empty($request->provider_id)){
            $user->notify(new SignupActivate($user));
        }

        $message =  trans('validation.account_created');
        return response()->json([
            'message' => $message
        ], 201);
    }

    /**
     * Login user and create token
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [boolean] remember_me
     * @return [string] access_token
     * @return [string] token_type
     * @return [string] expires_at
     */
    public function login(Request $request)
    {
        if(!empty($request->provider_id)){
            $user = User::where('provider_id', $request->provider_id)->first();
           
            if($request->provider=='google'){ 
                // $client = new Google_Client(['client_id' =>env('GOOGLE_CLIENT_ID')]);  
                // $userSocial = $client->verifyIdToken($request->access_token);
                $userSocial=true;
            }else{
                $userSocial = Socialite::driver($request->provider)->userFromToken($request->access_token);
            }
             
           
            if($user && $userSocial){
                $user->update([
                    'provider' => $request->provider,
                    'access_token' => $request->access_token
                ]);
            }
            else{
                return response()->json([
                    'message' =>trans('validation.user_unavailable')
                ]);
            }
        }
        else{
            $request->validate([
                'email' => 'required|string|email',
                'password' => 'required',
                'remember_me' => 'boolean'
            ]);
            $credentials = request(['email', 'password']);
            $credentials['deleted_at'] = null;
    
            if (!Auth::attempt($credentials))
                return response()->json([
                    'message' => trans('validation.invalid_credentials')
                ], 401);

            if(Auth::user()->active!=1){
                return response()->json([
                    'message' => trans('validation.activate_account')
                ], 401);
            }
            $user = $request->user();
        }
     
        $tokenResult = $user->createToken('Personal Access Token');
        //update language on login
        if($request->has('flag')){
            $user->update([
                'flag' => $request->flag
            ]);
        }
        $token = $tokenResult->token;

        if ($request->remember_me) {
            $token->expires_at = Carbon::now()->addWeeks(1);
        }

        $token->save();
        $message = trans('validation.login_success');

        //subscribe to notification
        Log::info($request);
        if(!empty($request->expo_token)){
            $checkUser= DB::table('exponent_push_notification_interests')->where('key','App.Models.User.'.Auth::id())->count();
            if($checkUser>0){
                DB::table('exponent_push_notification_interests')->where('key','App.Models.User.'.Auth::id())->delete();
            }
            $checkToken= DB::table('exponent_push_notification_interests')->where('value',$request->expo_token)->count();
            if($checkToken==0){
                try {
                    DB::table('exponent_push_notification_interests')->insert(
                        ['key' => 'App.Models.User.'.Auth::id(), 'value' => $request->expo_token]
                    );
                } catch (\Exception $e) {
                    return response()->json([
                        'message' => $e->getMessage(),500
                    ]);
                }
            }
        }
        
        return response()->json([
            'message' => $message,
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'user_id' => $user->id,
            'vendor_id' => optional($user->vendor)->id ?? null,
            'flag' => $user->flag,
            'identifier' => $user->identifier,
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString(),
            'onboarding_status' => $user->onboarding_status,
            'first_login' => $user->isFirstLogin(),
        ]);
    }

    /**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */
    public function logout(Request $request)
    {
        $message =trans('validation.logout_success');
        //unsubscribe to notification
        $checkToken= DB::table('exponent_push_notification_interests')->where('key', 'App.Models.User.'.Auth::id())->count();
        if($checkToken>0){
            try {
                DB::table('exponent_push_notification_interests')->where('key','App.Models.User.'.Auth::id())->delete();
            } catch (\Exception $e) {
                return response()->json([
                    'message' => $e->getMessage(),500
                ]);
            }
        }
        $request->user()->token()->revoke();
        return response()->json([
            'message' => $message
        ]);
    }

    /**
     * Get the authenticated User
     *
     * @return [json] user object
     */
    public function user(Request $request)
    {
        return response()->json($request->user());
    }

    public function signupActivate($token)
    {
        $user = User::where('activation_token', $token)->first();
        if (!$user) {
            return response()->json([
                'message' => 'This activation token is invalid - אסימון הפעלה זה אינו חוקי'
            ], 404);
        }
        $user->active = true;
        $user->activation_token = '';
        $user->email_verified_at = Carbon::now();
        $user->save();
        return "User account activated! - חשבון משתמש הופעל!";
    }

    public function checkIfEmailExists(Request $request)
    {
        $user = User::where('email', $request->email)->count();
        return response()->json($user, 200);
        
    }

    public function checkIfProviderIDExists(Request $request)
    {
        $user = User::where('provider_id', $request->provider_id)->count();
        return response()->json($user, 200);
        
    }

    public function redirect($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function Callback($provider){
        $userSocial =   Socialite::driver($provider)->user();
        $user  =   User::where(['email' => $userSocial->getEmail()])->orWhere(['provider_id' => $userSocial->getId()])->first();
        if($user){
            $tokenResult = $user->createToken('Personal Access Token');
            $token = $tokenResult->token;
    
            if ($user->remember_me) {
                $token->expires_at = Carbon::now()->addWeeks(1);
            }
    
            $token->save();
            $message = $user->flag == 'en' ? 'Successfully logged in' : 'התחבר בהצלחה';
    
            return response()->json([
                'message' => $message,
                'access_token' => $tokenResult->accessToken,
                'token_type' => 'Bearer',
                'user_id' => $user->id,
                'vendor_id' => optional($user->vendor)->id ?? null,
                'flag' => $user->flag,
                'identifier' => $user->identifier,
                'phone_no' => !empty($user->phone_no)?$user->phone_no:null,
                'street' => optional($user->vendor)->street ?? null,
                'city' => optional($user->vendor)->city ?? null,
                'postcode' => optional($user->vendor)->postcode ?? null,
                'expires_at' => Carbon::parse(
                    $tokenResult->token->expires_at
                )->toDateTimeString(),
                'onboarding_status' => $user->onboarding_status,
                'first_login' => $user->isFirstLogin(),
            ]);
        }else{
            $emailInfo=  $userSocial->getEmail();
            $user = User::create([
                'name'          => $userSocial->getName(),
                'email'         => !empty($emailInfo)?$emailInfo:'Email Required',
                'image'         => $userSocial->getAvatar(),
                'provider_id'   => $userSocial->getId(),
                'provider'      => $provider,
                'access_token'  => $userSocial->token,
                'active'        => 1,
            ]);
            $user->notify(new SignupActivate($user));
            $message = $message = trans('validation.account_created');;
            return response()->json([
                'message' => $message
            ], 201);
        }
    }
    
    public function insertDateIntoOld(){
        $vendors=Vendor::whereDoesntHave('CustomTime')->get();
        foreach ($vendors as $key => $vendor) {
             set_time_limit(0);
                 //generate custom Time
                 $custom_defaults='[
                     {
                         "id": 2,
                         "day": "Sunday",
                         "status": "Open",
                         "opening": "10:00",
                         "closing": "22:40"
                     },
                     {
                         "id": 3,
                         "day": "Monday",
                         "status": "Open",
                         "opening": "10:00",
                         "closing": "22:40"
                     },
                     {
                         "id": 4,
                         "day": "Tuesday",
                         "status": "Open",
                         "opening": "10:00",
                         "closing": "22:40"
                     },
                     {
                         "id": 5,
                         "day": "Wednesday",
                         "status": "Open",
                         "opening": "10:00",
                         "closing": "22:40"
                     },
                     {
                         "id": 6,
                         "day": "Thursday",
                         "status": "Open",
                         "opening": "10:00",
                         "closing": "22:40"
                     },
                     {
                         "id": 7,
                         "day": "Friday",
                         "status": "Open",
                         "opening": "10:00",
                         "closing": "22:40"
                     },
                     {
                         "id": 8,
                         "day": "Saturday",
                         "status": "Closed",
                         "opening": "10:00",
                         "closing": "22:40"
                     }
                 ]';
                 
                 $custom_days = json_decode($custom_defaults, true);
                 if(!empty($custom_days)){
                     foreach ($custom_days as $key => $custom_day) {
                         $day=$custom_day['day'];
                         $opening=$custom_day['opening'];
                         $closing=$custom_day['closing'];
                         $status=$custom_day['status'];
         
                         CustomTime::updateOrCreate([
                             'vendor_id'   => $vendor->id,
                             'day'   => $day,
                         ],[
                             'opening' => $opening,
                             'closing' => $closing,
                             'status'   => $status
                         ]);
         
                     }
                 }
        }
        return $vendors->count();
     }

     public function updateDateIntoOld(){
        $custom_days=CustomTime::where('status','Open 24 Hrs')->get();
        foreach ($custom_days as $key => $custom_day) {
           $custom_day->status='Open';
           $custom_day->save();
        }
        return $custom_days->count();
     }
}
