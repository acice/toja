<?php

namespace App\Http\Controllers\Favourite;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Favourite;
use Auth;

class FavouriteController extends Controller
{
    public function __construct() {
    
    }
  
    public function store(Request $request) {
     
        $favourite = Favourite::updateOrCreate([
            'user_id' => Auth::user()->id,
            'post_id' => $request->post_id
        ]);
        $favourite->user_id = Auth::user()->id;
        $favourite->post_id = $request->post_id;
        $favourite->save();
        return response()->json([
            'message' => trans('validation.successful_add_favourite')
        ], 200);
    }

    public function destroy($id) {
        $favourite = Favourite::where('user_id', Auth::user()->id)->where('post_id', $id)->first();
        if(empty($favourite)){
            return response()->json([
                'message' => trans('validation.unable_removed_favourite')
            ], 400);
        }
        $favourite->delete();
        return response()->json([
            'message' => trans('validation.successful_removed_favourite')
        ], 200);
    }
    
    public function index() {
        $favourites = Auth::user()->favourites()->whereHas(
            'post',
            function ($q) {
                return  $q->where('discount_end','>=',now()->toDateTimeString());
            }
        )->with(
            [
                'post.files',
                'post.user.vendor',
            ]
        )->orderBy("created_at", 'DESC')
            ->whereHas(
                'user',
                function ($q) {
                    return  $q->where('active', '=', 1);
                }
            )->paginate(10);
        return response()->json($favourites, 200);
    }
}
