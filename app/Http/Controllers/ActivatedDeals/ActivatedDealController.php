<?php
namespace App\Http\Controllers\ActivatedDeals;

use Carbon\Carbon;
use App\Models\Post;
use App\Models\User;
use App\Models\Comment;
use Illuminate\Http\Request;
use App\Models\ActivatedDeals;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\PaymentDetails;
use App\Models\Vendor;
use App\Notifications\DealActivated;
use App\Notifications\DealVerified;
use App\Notifications\DealCompleted;
use App\Jobs\StatusChangeDeals;
use App\Jobs\PaymentCheckDeals;
use App\Notifications\DealCancelled;
use App\Notifications\DealRejected;

class ActivatedDealController extends Controller
{

       /**
     * activate the deal
     * $param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\JsonResponse
     */

    //create deal for post
    public function activateDeal(Request $request)
    {
        $post_id=$request->post_id;
        $invoice_no = Str::random(10);
        $payment_invoice=Str::random(6);
        $post = Post::findOrFail($post_id);
        $activation_count=$post->activation_count;
        //checked if post already redeemed
        if ($post->user_activated_deal) {
            $message = trans('validation.one_deal_per_account');
            return response()->json([
                'message' => $message
            ], 422);
        }
        //checked for post if deals remained 
        if($activation_count >= $post->discount_activations){
            $message = trans('validation.no_of_deals_redeemed');
            return response()->json([
                'message' => $message
            ], 422);
        }
       
        $post->activation_count=$activation_count+1;
        $post->save();

        $preference=$request->preference;
        $postPrice=$post->discount_cost;
        $discountPercentage=$post->discount_amount;
        $discountAmount=($postPrice*$discountPercentage)/100;
        $discountedAmountNoCommission=$postPrice-$discountAmount;
        $discountAmount=$discountAmount-($discountAmount*0.2);
       
        $discountedAmount=$postPrice-$discountAmount;
        $vendor=Vendor::where('user_id',$post->user_id)->first();
        $notifiableVendor=User::where('id',$post->user_id)->first();
        $notifiableVendor->invoice_no = $invoice_no;
        $notifiableVendor->customer = auth()->user()->name;
      
        try {
         
            $deal=ActivatedDeals::create([
                'user_id' => auth()->user()->id,
                'vendor_id' => (!empty($vendor))?$vendor->id:0,
                'post_id' => $post_id,
                'confirmed' => now(),
                'invoice_no'=> '#'.$invoice_no,
                'preference'=>$preference,
                'status' => 'pending',
                'reject_reason'=>'',
            ]);

            $payment= PaymentDetails::create([
                'amount'=>$discountedAmount,
                'vendor_amount'=>$discountedAmountNoCommission,
                'payment_id'=>$payment_invoice,
                'name'=>'Payment for #'.$invoice_no,
                'payment_status'=>'unpaid',
                'user_id'=>auth()->user()->id,
                'post_id'=>$post_id,
                'activated_deals_id'=>$deal->id,
            ]);
            StatusChangeDeals::dispatch($deal)->delay(now()->addMinutes(5));
            $notifiableVendor->deal_id = $deal->id;
        

        } catch (\Exception $e) {

            return response()->json([
                'message' => $e->getMessage(),500
            ]);
            
        }
        
        $message = trans('validation.redeem_deal');

        $authFlag= auth()->user()->flag;

        Auth::user()->update([
            'flag' => $notifiableVendor->flag,
        ]);

        $notifiableVendor->notify(new DealActivated($notifiableVendor));

        Auth::user()->update([
            'flag' => $authFlag,
        ]);
        
        return response()->json([
            'message' => $message,
            'data' => $deal
        ], 200);
    }

    /**
     * cancel the deal.
     * @param int $id
     *
     * @return Illuminate\Http\JsonResponse
     */
    public function cancelDeal(Request $request,int $id)
    {
        $deal = ActivatedDeals::where('id', $id)->first();
        $post=$deal->post()->first();
        $activation_count=$post->activation_count;
        $post->activation_count=$activation_count-1;
        $post->save();
        $reject_reason=$request->reject_reason;
        if (!$deal) {
            return response()->json([
                'message' => trans('validation.select_current_deal')
            ], 422);
        }

        if (auth()->user()->id !== (int)$deal->user_id) {
            return response()->json([
                'message' =>  trans('validation.only_cancel_deal_own')
            ], 422);
        }
        $deal->update([
            'status'    => 'cancelled',
            'reject_reason'    => $reject_reason
        ]);
    
        $notifiableVendor=User::find($deal->vendor()->first()->user_id);
        $notifiableVendor->customer = auth()->user()->name;
        $notifiableVendor->avatar_url=auth()->user()->avatar_url;
        $notifiableVendor->deal_id= $deal->id;
        $notifiableVendor->post_id=  $post->id;
        $notifiableVendor->post_title=  $post->title;
        $notifiableVendor->reject_reason =$reject_reason;
        $notifiableVendor->human_time = $post->human_time;
        $notifiableVendor->notify(new DealCancelled($notifiableVendor));
        return response()->json([
            'message' => trans('validation.deal_cancelled')
        ], 200);
    }

    /**
     * Confirm activated deal.
     *
     * @param  int  $iid
     * @return \App\Models\ActivatedDeals
     */
    public function confirmActivatedDeal(Request $request,int $id) 
    {
        $activatedDeal = ActivatedDeals::find($id);
        $post=$activatedDeal->post()->first();
        $payment=$activatedDeal->payment()->first();
        $vendor=Vendor::where('user_id',auth()->user()->id)->first();
        if($vendor->id != $activatedDeal->vendor_id){
            return response()->json(['message' =>trans('validation.unable_find_deal')], 404);
        }
        if($activatedDeal){
            $notifiableUser=User::where('id',$activatedDeal->user_id)->first();
            $dealStatus= $request->status;
            $reject_reason=$request->reject_reason;
            $notifiableUser->vendor_name=auth()->user()->name;
            $notifiableUser->vendor_avatar_url=auth()->user()->avatar_url;
            $notifiableUser->deal_status= $dealStatus;
            $notifiableUser->deal_id= $activatedDeal->id;
            $notifiableUser->payment_id = $payment->id;
            $notifiableUser->post_id=  $post->id;
            $notifiableUser->post_title=  $post->title;
            $notifiableUser->post_discount_amount=  $post->discount_amount;
            $notifiableUser->post_title=  $post->title;
            $notifiableUser->reject_reason =$reject_reason;
            $notifiableUser->human_time = $post->human_time;

            $deal= tap($activatedDeal)
                ->update([
                    'confirmed' => now(),
                    'status'    => $dealStatus,
                    'reject_reason' => $reject_reason,
                ]);

            $authFlag= auth()->user()->flag;

            Auth::user()->update([
                'flag' => $notifiableUser->flag,
            ]);

            if($dealStatus=='inprogress'){
                PaymentCheckDeals::dispatch($activatedDeal)->delay(now()->addMinutes(5));
                $notifiableUser->notify(new DealVerified($notifiableUser));
            }
            elseif($dealStatus=='completed'){
                $notifiableUser->notify(new DealCompleted($notifiableUser));
            }
            elseif($dealStatus=='rejected'){
                $notifiableUser->notify(new DealRejected($notifiableUser));
            }
    
            Auth::user()->update([
                'flag' => $authFlag,
            ]);

            return $deal;
        }else{
            return response()->json(['message' =>trans('validation.unable_find_deal')], 404);
        }
    }

    /**
     * Search users that have activated a vendors deal.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \App\Models\ActivatedDeals
     */
    public function search(Request $request) {

        $attributes = $request->validate([
           'invoice_no' => 'required'
        ]);

        $invoice=$request->invoice_no;
        $vendor = auth()->user()->vendor;

        $activations = $vendor
                        ->activatedDeals()
                        ->where('invoice_no','like','%' .$invoice.'%' )
                        ->with('post','post.files','user:id,name,email,phone_no,avatar','payment:id,amount,vendor_amount,payment_status,name,activated_deals_id')
                        ->paginate(10);

        return response()->json($activations, 200);
    }

     /**
     * Return all activated deals of vendor.
     *
     *
     * @return \App\Models\ActivatedDeals
     */
    public function allActivatedDeal() {
        
        $status = !empty($_GET['status'])?$_GET['status']:'';

        if(auth()->user()->identifier==1){
            $user = auth()->user()->vendor;
        }
        else{
            $user = auth()->user();
        }

        $activations = $user->activatedDeals();
        
        if($status!=''){
           $activations = $activations->where('status',$status);
        }
            
        $activations =$activations
                    ->with('post','post.files','user:id,name,email,phone_no,avatar','payment:id,payment_status,amount,name,activated_deals_id')
                    ->orderBy("created_at", 'DESC')
                    ->paginate(10);

        return response()->json($activations, 200);
    }

    public function getActivatedDealsByID(int $id){
        $deal = ActivatedDeals::with('vendor:id,longitude,latitude','payment:id,amount,vendor_amount,payment_status,name,activated_deals_id')->where('id',$id)->first();
        if($deal){
            $created_date=Carbon::parse($deal->created_at);
            $created_date_added_time=$created_date->addMinutes(5.2);
            $remaining_time=Carbon::now()->diffInSeconds($created_date_added_time, false);
            if($remaining_time<0){
                $remaining_time=0; 
            }
            $deal->expire_time=$remaining_time;
            return response()->json($deal, 200);
        }else{
            return response()->json(['message' =>trans('validation.unable_find_deal')], 404);
        }

    }
}
