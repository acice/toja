<?php
namespace App\Http\Controllers\Payment;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\PaymentDetails;
use App\Models\PayerDetails;
use App\Models\Invoice;
use App\Models\User;
use App\GoogleTranslate;

class InvoiceController extends Controller
{

    public function store(Request $request)
    {
        $paymentDetails = PaymentDetails::where('user_id', User::where('id', Auth::user()->id)->with('vendor')->first()->vendor->id)->first();

        if (!$paymentDetails) {
            return response()->json([
                'message' => 'Sorry, You must link your PayPal account before creating an invoice',
            ], 400);
        }
        
        

        $originalPrice = $request->price;
        $discountAmount = number_format(($originalPrice * $request->discount), 2);
        $priceWithDiscount = $originalPrice - $discountAmount;
        $adminCharge = $priceWithDiscount * 0.06;
        $adminChargeWithVAT = number_format($adminCharge + ($adminCharge * 0.17), 2);

        $invoice = Invoice::create([
            'payment_id' => $paymentDetails->id,
            'name' => $request->name,
            'iw_name' => (new GoogleTranslate)->translate($request->name, 'iw')['text'],
            'price' => $priceWithDiscount,
            'admincharge' => $adminChargeWithVAT,
            'discount' => $request->discount,
            'payer_id' => $request->payer_id,
        ]);

        if (!$invoice) {
            return response()->json([
                'message' => 'Unable to create invoice',
            ], 400);
        }

        return response()->json([
            'message' => 'Successfully sent invoice',
        ], 200);
    }

    public function getUserInvoices() {
        $formattedInvoices = [];
        $invoices = Invoice::where('payer_id', Auth::user()->id)->with('vendorPaymentDetails', 'vendor')->get();
        foreach ($invoices as $invoice) {
           $formattedInvoice['business'] = $invoice->name; 
           $formattedInvoice['price'] = $invoice->price + $invoice->admincharge;
           $formattedInvoice['created_at'] = $invoice->created_at->toDateString();
           $formattedInvoice['paid'] = $invoice->paid;
           $formattedInvoice['invoice_id'] = $invoice->id;
           array_push($formattedInvoices, $formattedInvoice);
        }
        return response()->json($formattedInvoices, 200);
    }

     public function getVendorInvoices() {
        $formattedInvoices = [];
        $paymentDetails = PaymentDetails::where('user_id', User::where('id', Auth::user()->id)->with('vendor')->first()->vendor->id)->first();
        $invoices = Invoice::where('payment_id', $paymentDetails->id)->with('userPayerDetails')->get();
         foreach ($invoices as $invoice) {
           $formattedInvoice['user'] = $invoice->name; 
           $formattedInvoice['price'] = $invoice->price;
           $formattedInvoice['created_at'] = $invoice->created_at->toDateString();
           $formattedInvoice['paid'] = $invoice->paid;
           array_push($formattedInvoices, $formattedInvoice);
        }
        return response()->json($formattedInvoices, 200);
    }

    public function getInvoiceById($id) {
        $invoice = Invoice::findOrFail($id)->with('vendorPaymentDetails', 'userPayerDetails')->first();
        return response()->json($invoice, 200);
    }



}
