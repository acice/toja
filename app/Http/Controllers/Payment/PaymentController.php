<?php
namespace App\Http\Controllers\Payment;

use App\Http\Controllers\Controller;
use App\Models\ActivatedDeals;
use App\Models\Post;
use App\Models\User;
use App\Models\PaymentDetails;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Exception;
use Auth;
use App\Notifications\PaymentSuccess;
use App\Notifications\PaymentFailed;
use Mail;
use App\Mail\EmailInvoice;

class PaymentController extends Controller
{
  public function __construct() {
    
  }

    //get  payment intent &entry in Database
    public function create($postID){
      $post=Post::where('id',$postID)
                ->where('discount_activations','>',0)->first();
      if(empty($post)){
          return response()->json(
            [
                'message' => 'Post Not Found',
            ],
            404
        );
      }
      //Decrease discount count every time payment called
      $post->discount_activations--;
      $post->save();
      $discountAmount=($post->discount_amount*$post->discount_cost)/100;
      $priceAfterDiscount=$post->discount_amount-$discountAmount;
      $finalPrice=$priceAfterDiscount*1.2;

         //Payment Intent initialized
         $stripe = new \Stripe\StripeClient(env('STRIPE_SECRET'));
         $response=$stripe->paymentIntents->create([
           'amount' =>$finalPrice*100,
           'currency' => 'ils',
           // 'payment_method_types' => ['card'],
         ]);

      //Save Payment details
      $payment = new PaymentDetails();
      $payment->name='Payment for Instance - '.$response->id;
      $payment->user_id=!empty(Auth::id())?Auth::id():1;
      $payment->payment_status='Incomplete';
      $payment->payment_id=$response->id;
      $payment->post_id=$postID;
      $payment->amount=$finalPrice;
      $payment->save();

      return response()->json($response);
    }

    public function update(Request $request, $id){
      $payment=PaymentDetails::where('id',$id)->first();
      $deal=ActivatedDeals::where('id',$payment->activated_deals_id)->first();
      $post=$deal->post()->first();
      $notifiableVendor=User::find($deal->vendor()->first()->user_id);
      $notifiableVendor->customer = auth()->user()->name;
      $notifiableVendor->user_name=auth()->user()->name;
      $notifiableVendor->avatar_url=auth()->user()->avatar_url;
      $notifiableVendor->deal_id= $deal->id;
      $notifiableVendor->payment_id = $payment->id;
      $notifiableVendor->post_id=  $post->id;
      $notifiableVendor->post_title=  $post->title;
      $notifiableVendor->post_discount_amount=  $post->discount_amount;
      $notifiableVendor->post_title=  $post->title;
      $notifiableVendor->reject_reason =$deal->reject_reason;
      $notifiableVendor->human_time = $post->human_time;
      $status=!empty($request['status'])?$request['status']:'unpaid';
      $payment->payment_status=$status;
      $payment->save();
      // Add data for payment
      $payment->invoice_no = $deal->invoice_no;
      $payment->no_comission_price = $post->no_comission_price;
      $payment->comission_price = $post->comission_price;
      $payment->discounted_price = $post->discounted_price;
      if($status =='paid'){
        
        Mail::to(auth()->user()->email)
        ->send(new EmailInvoice($payment));

        $authFlag= auth()->user()->flag;
        Auth::user()->update([
            'flag' => $notifiableVendor->flag,
        ]);
        $notifiableVendor->notify(new PaymentSuccess($notifiableVendor));
    
        Auth::user()->update([
          'flag' => $authFlag,
        ]);
      }
      return response()->json($payment);
    }

    //Refund payment
  function paymentRefund($paymentID){
    $stripe = new \Stripe\StripeClient(env('STRIPE_SECRET'));
    $response=$stripe->refunds->create([
      'payment_intent' => $paymentID,
    ]);
    //Update in payment table
    $payment=PaymentDetails::where('payment_id',$paymentID)->first();
    $payment->payment_status='Refunded';
    $payment->save();
    return response()->json($response);
  }

    //payout to specific account
  function payout(){
    \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
    $transfer = \Stripe\Transfer::create([
      "amount" => 1110,
      "currency" => "gbp",
      "destination" => "acct_1KZ6l72EBeX1VX3j",
    ]);
  }

  public function webhook(){
    $input = file_get_contents('php://input');
    $body = json_decode($input);
    
  }

  public function success(){
    return "success";
  }

  public function failed(){
    return "failed";
  }

  public function cancel(){
    return "cancel";
  }
  
}