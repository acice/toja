<?php
namespace App\Http\Controllers\Payment;

use App\Http\Controllers\Controller;
use App\Models\Invoice;
use App\Models\PaymentDetails;
use App\Services\PayPalClient;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;
use PayPalCheckoutSdk\Orders\OrdersAuthorizeRequest;
use PayPalCheckoutSdk\Orders\OrdersCaptureRequest;
use PayPalCheckoutSdk\Orders\OrdersCreateRequest;
use PayPalCheckoutSdk\Payments\AuthorizationsCaptureRequest;
use PayPal\Api\Payment;
use PayPal\Api\Payout;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
use Sample\CaptureIntentExamples\CreateOrder;
use Srmklive\PayPal\Facades\PayPal;
use Srmklive\PayPal\Services\AdaptivePayments;

class PayPalController extends Controller
{

    public function testPayment()
    {
        $apiContext = new ApiContext(
            new OAuthTokenCredential(
                env('PAYPAL_SANDBOX_KEY'),
                env('PAYPAL_SANDBOX_SECRET')

            )
        );

        $apiContext->setConfig(
            array(
                'mode' => 'sandbox',
                'log.LogEnabled' => false,
                'log.LogLevel' => 'DEBUG', // PLEASE USE INFO LEVEL FOR LOGGING IN LIVE ENVIRONMENTS
                'cache.enabled' => false,
                //'cache.FileName' => '/PaypalCache' // for determining paypal cache directory
                // 'http.CURLOPT_CONNECTTIMEOUT' => 30
                // 'http.headers.PayPal-Partner-Attribution-Id' => '123123123'
                //'log.AdapterFactory' => '\PayPal\Log\DefaultLogFactory' // Factory class implementing \PayPal\Log\PayPalLogFactory
            )
        );

        $payouts = new Payout();

        $senderBatchHeader = new \PayPal\Api\PayoutSenderBatchHeader();
        // ### NOTE:
        // You can prevent duplicate batches from being processed. If you specify a `sender_batch_id` that was used in the last 30 days, the batch will not be processed. For items, you can specify a `sender_item_id`. If the value for the `sender_item_id` is a duplicate of a payout item that was processed in the last 30 days, the item will not be processed.
        // #### Batch Header Instance
        $senderBatchHeader->setSenderBatchId(uniqid())
            ->setEmailSubject("You have a payment");
        // #### Sender Item
        // Please note that if you are using single payout with sync mode, you can only pass one Item in the request
        $senderItem1 = new \PayPal\Api\PayoutItem();
        $senderItem1->setRecipientType('Email')
            ->setNote('Payment to  vendor')
            ->setReceiver('joshuajordancallis-new-business@gmail.com')
            ->setSenderItemId("item_1" . uniqid())
            ->setAmount(new \PayPal\Api\Currency('{
            "value":"0.99",
            "currency":"USD"
        }'));
        // #### Sender Item 2
        // There are many different ways of assigning values in PayPal SDK. Here is another way where you could directly inject json string.
        $senderItem2 = new \PayPal\Api\PayoutItem(
            '{
        "recipient_type": "EMAIL",
        "amount": {
        "value": 0.90,
        "currency": "USD"
        },
        "receiver": "joshuajordancallis-facilitator@gmail.com",
        "note": "FEE",
        "sender_item_id": "item_2"
        }'
        );
        $payouts->setSenderBatchHeader($senderBatchHeader)
            ->addItem($senderItem1)->addItem($senderItem2);
        // For Sample Purposes Only.
        $request = clone $payouts;
        // ### Create Payout
        try {
            $output = $payouts->create(null, $apiContext);
        } catch (Exception $ex) {
            // NOTE: PLEASE DO NOT USE RESULTPRINTER CLASS IN YOUR ORIGINAL CODE. FOR SAMPLE ONLY
            ResultPrinter::printError("Created Batch Payout", "Payout", null, $request, $ex);
            exit(1);
        }
        // NOTE: PLEASE DO NOT USE RESULTPRINTER CLASS IN YOUR ORIGINAL CODE. FOR SAMPLE ONLY
        ResultPrinter::printResult("Created Batch Payout", "Payout", $output->getBatchHeader()->getPayoutBatchId(), $request, $output);
        return $output;
    }

    public function getPayOutView(Request $request, $id)
    {
        $request->session('invoiceId')->flush();
        session(['invoiceId' => $id]);
        return view('payment.payout.payout');
    }

    /**
     * Setting up the JSON request body for creating the Order with complete request body. The Intent in the
     * request body should be set as "AUTHORIZE" for authorize intent flow.
     *
     */
    private static function buildRequestBody()
    {

        $invoiceNo = rand(10, 1000);
        $invoice = Invoice::where('id', session('invoiceId'))->with('userPayerDetails', 'vendorPaymentDetails', 'vendor')->first();
        $price = (string) round($invoice->price, 2);
        $adminCharge = (string) round($invoice->admincharge, 2);
        return array(
            'intent' => 'CAPTURE', //AUTHORIZE
            'application_context' => array(
                'return_url' => 'https://toja.app/api/payment/success',
                'cancel_url' => 'https://toja.app/api/payment/cancel',
                'brand_name' => 'TOJA',
                'locale' => 'en-US',
                'landing_page' => 'BILLING',
                'user_action' => 'PAY_NOW',
            ),
            'purchase_units' => array(
                0 => array(
                    'reference_id' => 'Invoice-' . $invoice->userPayerDetails->email . '-' . $invoiceNo,
                    'description' => 'Invoice from ' . $invoice->vendorPaymentDetails->name,
                    'custom_id' => 'Invoice-' . $invoice->userPayerDetails->email . '-' . $invoiceNo,
                    'soft_descriptor' => 'Invoice to be Paid',
                    'amount' => array(
                        'currency_code' => 'ILS',
                        'value' => $price,
                    ),
                    'payee' => array(
                        'email_address' => $invoice->vendorPaymentDetails->email,
                    ),
                    'payment_instruction' => array(
                        'disbursement_mode' => 'INSTANT',
                        'platform_fees' => array(
                            0 => array(
                                'amount' => array(
                                    'currency_code' => 'ILS',
                                    'value' => $adminCharge,
                                ),
                                'payee' => array(
                                    'email_address' => 'sb-gpbla56505@business.example.com',
                                ),
                                "payment_linked_group" => 1,
                                "custom" => "oren",
                                "invoice_number" => 'Invoice-' . $invoice->userPayerDetails->email . '-' . $invoiceNo,
                                "payment_descriptor" => "TOJA - Payments",
                            ),
                        ),
                    ),
                ),
            ),
        );
    }

    /** Create Order - get the order id */

    public function createOrder(Request $request, $debug = false)
    {
        $request = new OrdersCreateRequest();
        $request->headers["prefer"] = "return=representation";
        $request->body = self::buildRequestBody();
        $client = PayPalClient::client();
        $response = $client->execute($request);
        // if ($debug)
        // {
        //     print "Status Code: {$response->statusCode}\n";
        //     print "Status: {$response->result->status}\n";
        //     print "Order ID: {$response->result->id}\n";
        //     print "Intent: {$response->result->intent}\n";
        //     print "Links:\n";
        //     foreach($response->result->links as $link)
        //     {
        //         print "\t{$link->rel}: {$link->href}\tCall Type: {$link->method}\n";
        //     }
        //     print "Gross Amount: {$response->result->purchase_units[0]->amount->currency_code} {$response->result->purchase_units[0]->amount->value}\n";
        //     // To toggle printing the whole response body comment/uncomment below line
        //      echo json_encode($response->result, JSON_PRETTY_PRINT), "\n";
        // }

        return response()->json($response->result);
        // print_r($response);

    }

    public function capturePayment($orderId)
    {

        // 3. Call PayPal to get the transaction details
        // $client = PayPalClient::client();
        // $response = $client->execute(new OrdersGetRequest($orderId));
        /**
         *Enable the following line to print complete response as JSON.
         */
        // print json_encode($response->result);
        // print "Status Code: {$response->statusCode}\n";
        // print "Status: {$response->result->status}\n";
        // print "Order ID: {$response->result->id}\n";
        // print "Intent: {$response->result->intent}\n";
        // print "Links:\n";
        // foreach($response->result->links as $link)
        // {
        // // print "\t{$link->rel}: {$link->href}\tCall Type: {$link->method}\n";
        // }
        //4. Save the transaction in your database. Implement logic to save transaction to your database for future reference.
        // print "Gross Amount: {$response->result->purchase_units[0]->amount->currency_code} {$response->result->purchase_units[0]->amount->value}\n";

        //To print the whole response body, uncomment the following line

        return self::captureOrder($orderId);
        // return $this->AuthorizationsCapture($auth->id);
        // echo response()->json($response->result);

        // return response()->json([
        //     'success' => true,
        //     'success_redirect' => 'http://toja.com/api/payment/success',
        // ]);
    }

    public static function captureOrder($orderId)
    {
        $client = PayPalClient::client();
        // Here, OrdersCaptureRequest() creates a POST request to /v2/checkout/orders
        // $response->result->id gives the orderId of the order created above
        $request = new OrdersCaptureRequest($orderId);
        $request->prefer('return=representation');
        try {
            // Call API with your client and get a response for your call
            $response = $client->execute($request);

            // If call returns body in response, you can get the deserialized version from the result attribute of the response
            return response()->json($response->result);
        } catch (HttpException $ex) {
            echo $ex->statusCode;
            print_r($ex->getMessage());
        }
    }

    /**
     * Setting up request body for Authorize. This can be populated with fields as per need. Refer API docs for more details.
     *
     */
    public static function buildRequestBodyTwo()
    {
        return "{}";
    }

    /**
     * This function can be used to perform authorization on the approved order.
     * Valid Approved order id should be passed as an argument.
     */
    public static function authorizeOrder($orderId, $debug = false)
    {
        $request = new OrdersAuthorizeRequest($orderId);
        $request->body = self::buildRequestBodyTwo();
        $client = PayPalClient::client();
        $response = $client->execute($request);
        // if ($debug) {
        //     print "Status Code: {$response->statusCode}\n";
        //     print "Status: {$response->result->status}\n";
        //     print "Order ID: {$response->result->id}\n";
        //     print "Authorization ID: {$response->result->purchase_units[0]->payments->authorizations[0]->id}\n";
        //     print "Links:\n";
        //     foreach ($response->result->links as $link) {
        //         print "\t{$link->rel}: {$link->href}\tCall Type: {$link->method}\n";
        //     }
        //     print "Authorization Links:\n";
        //     foreach ($response->result->purchase_units[0]->payments->authorizations[0]->links as $link) {
        //         print "\t{$link->rel}: {$link->href}\tCall Type: {$link->method}\n";
        //     }
        //     // To toggle printing the whole response body comment/uncomment below line
        //     echo json_encode($response->result, JSON_PRETTY_PRINT), "\n";
        // }

        return self::AuthorizationsCapture($response->result->purchase_units[0]->payments->authorizations[0]->id);
        // return response()->json($response->result);
    }

    /**
     * Below method can be used to build the capture request body.
     * This request can be updated with required fields as per need.
     * Please refer API specs for more info.
     */
    public static function buildRequestBodyThree()
    {

        return "{}";
        // return array(
        //     'payment_instruction' => array(
        //         'disbursement_mode' => 'INSTANT',
        //         'platform_fees' => array(
        //             0 => array(
        //                 'amount' => array(
        //                     'currency_code' => 'USD',
        //                     'value' => '1.00',
        //                 ),
        //                 'payee' => array(
        //                     'email_address' => 'joshuajordancallis-facilitator@gmail.com',
        //                 ),
        //             ),
        //         ),
        //     ));
    }
    /**
     * Below function can be used to capture order.
     * Valid Authorization id should be passed as an argument.
     */
    public static function AuthorizationsCaptur($authorizationId, $debug = false)
    {
        $request = new AuthorizationsCaptureRequest($authorizationId);
        $request->body = self::buildRequestBodyThree();
        $client = PayPalClient::client();
        $response = $client->execute($request);
        // if ($debug) {
        //     print "Status Code: {$response->statusCode}\n";
        //     print "Status: {$response->result->status}\n";
        //     print "Capture ID: {$response->result->id}\n";
        //     print "Links:\n";
        //     foreach ($response->result->links as $link) {
        //         print "\t{$link->rel}: {$link->href}\tCall Type: {$link->method}\n";
        //     }
        //     // To toggle printing the whole response body comment/uncomment below line
        //     echo json_encode($response->result, JSON_PRETTY_PRINT), "\n";
        // }
        return response()->json($response->result);
    }

    /**
     * Capture the order and get the order id
     */

    public function getPayPalView($id)
    {
        // dd(env('PAYPAL_SANDBOX_API_SECRET'));
        $invoice = Invoice::where('id', $id)->with('userPayerDetails', 'vendorPaymentDetails', 'vendor')->first();
        $provider = new AdaptivePayments; // To use adaptive payments.
        $provider = PayPal::setProvider('adaptive_payments'); // To use adaptive payments.
        $provider->setCurrency('ILS');

        $data = [
            'receivers' => [
                [
                    'email' => $invoice->vendorPaymentDetails->email,
                    'amount' => $invoice->price,
                    'primary' => true,
                ],
                [
                    'email' => 'joshuajordancallis-new-business@gmail.com',
                    'amount' => $invoice->admincharge,
                    'primary' => false,
                ],
            ],
            'payer' => 'EACHRECEIVER', // (Optional) Describes who pays PayPal fees. Allowed values are: 'SENDER', 'PRIMARYRECEIVER', 'EACHRECEIVER' (Default), 'SECONDARYONLY'
            'return_url' => url('api/payment/success'),
            'cancel_url' => url('api/payment/cancel'),
        ];

        $response = $provider->createPayRequest($data);

        if (array_has($response, 'error')) {
            return $response['error'][0]['message'];
        }

        if ($response) {
            $redirect_url = $provider->getRedirectUrl('approved', $response['payKey']);
            // $invoice->InvoicePaid($invoice->id);
        }
        return redirect($redirect_url);
    }

    public function PaymentSuccess()
    {
        Invoice::where('id', '=', session('invoiceId'))->update([
            'paid' => 1,
        ]);

        return view('payment.success');
    }

    public function PaymentCancel(Request $request)
    {
        dd($request);
        // return view('payment.cancel');
    }

    public function PayPalConnect(Request $request)
    {
        $request->session()->forget('vendorId');
        $request->session()->put('vendorId', $request->vendorId);
        return Socialite::driver('paypal_sandbox')->stateless(true)->scopes(['openid', 'profile', 'email', 'https://uri.paypal.com/services/paypalattributes'])->redirect();
    }

    public function PayPalConnected(Request $request)
    {
        $user = Socialite::driver('paypal_sandbox')->stateless(true)->user();
        if (!$user) {
            return view('payment.unabletoconnect');
        }

        $paymentDetails = PaymentDetails::create([
            'merchant_id' => $user->user['payer_id'],
            'name' => $user->user['name'],
            'email' => $user->user['email'],
            'user_id' => $request->session()->get('vendorId'),
        ]);

        if ($paymentDetails) {
            return view('payment.paypalconnected');
        }

    }

    public function getAccessToken()
    {
        $client = new Client();

        $data = [
            // -h
            'headers' => [
                'Accept' => 'application/json',
                'Accept-Language' => 'en_US',
            ],
            // - u
            'auth' => [
                env('PAYPAL_SANDBOX_KEY'),
                env('PAYPAL_SANDBOX_SECRET'),
            ],
            // -d
            'form_params' => [
                'grant_type' => 'client_credentials',
            ],
        ];
        $response = $client->request('POST', 'https://api.sandbox.paypal.com/v1/oauth2/token', $data);
        return 'Bearer ' . json_decode($response->getBody()->getContents())->access_token;
    }
}
