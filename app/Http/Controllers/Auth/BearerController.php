<?php
namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class BearerController extends Controller
{
    public function __construct()
    {
    }

    /**
     * return Bearer
     *
     * @param [int] $id
     * @return Illuminate\Http\JsonResponse
     */
    public function getBearer(Request $request, $id) {
        // Get the bearer.
        $bearer = Bearer::where('user_id', $id)->first();
        if(!Auth::user()) {
            return;
        }
        if($bearer) {
            return response()->json([
                'bearer' => $bearer
            ], 200);
        } else {
            return response()->json([
                'message' => 'Unable to get bearer'
            ], 200);
        }
    }

    /**
     * Delete bearer
     *
     * @param [int] $id
     * @return Illuminate\Http\JsonResponse
     */
    public function deleteBearer() {
        //Remove the bearer.
        $delete = DB::table('bearer')->where('user_id', Auth::user()->id)->delete();

        if($delete) {
            return response()->json([
                'message' => 'Successfully removed bearer.'
            ], 200);
        } else {
            return response()->json([
                'message' => 'Unable to remove bearer.'
            ], 400);
        }
    }

}
