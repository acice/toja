<?php
namespace App\Http\Controllers\Events;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

use App\Models\EventTicket;

class EventTicketController extends Controller {

    public function __construct()
    {
    }

    /**
     * Create event tickets
     * $param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\JsonResponse
     */
    public function createEventTickets(Request $request) {
        $request->validate([
            'amount' => 'required',
            'price' => 'required',
        ]);

        $event = new EventTicket([
            'name' => $request->name,
            'amount' => $request->amount,
            'price' => $request->price,
            'event_id' => $request->event_id,
            'user_id' => Auth::user()->id,
        ]);
        $event->save();


        if($event) {
            return response()->json([
                'message' => 'Successfully created tickets for the event',
            ], 200);
        } else {
            return response()->json([
                'message' => 'Unable to create tickets for the event'
            ], 400);
        }
    }


    /**
     *  Update Amount of tickets available for an event
     *  @param  Illuminate\Http\Request $request.
     *  @param [int] $id
     *
     * @return Illuminate\Http\JsonResponse
     */
    public function updateEventTicket(Request $request, $id) {
        $update = EventTicket::where('event_id', $id)
            ->update([
                'name' => $request->name,
                'amount' => $request->amount,
                'price' => $request->price,
            ]);

        if($update) {
            return response()->json([
                'message' => 'Successfully updated event ticket information.'
            ], 200);
        } else {
            return response()->json([
                'message' => 'Unable to update event ticket information.'
            ], 400);
        }
    }


    /**
     *  List event tickets
     */
    public function getEventTickets() {
        $eventTickets = EventTicket::with('user', 'event')->get();

        return response()->json([
            $eventTickets,
        ], 200);
    }
}
