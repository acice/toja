<?php
namespace App\Http\Controllers\Events;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

use App\Models\Event;
use App\Models\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\DB;

use Carbon\Carbon;
use DateTimeZone;

class EventController extends Controller {

    public function __construct()
    {
    }


    public function listAllEvents(Request $request) {
        $distance = $request->input('distance');
        $long = $request->input('long');
        $lat = $request->input('lat');

        $today  = Carbon::now(new DateTimeZone('Europe/London'))->toDateTimeString();
        if(Auth::user()->id == 5) {
            $events = Event::where('user_id', '=', Auth::user()->id)->with('files', 'tickets')->get();
            return response()->json($events);
        }
        // Lat | Long
        if($lat && $long) {
            $query = Event::distance($lat, $long);
            $eventResult = $query->orderBy('distance', 'ASC')->with('files', 'tickets')->get();
            $events = $eventResult->where('event_date_time', '>=', $today)->where('distance', '<', $distance);
        } else {
            $events = Event::where('event_date_time', '>=', $today)->with('files', 'tickets')->get();
        }

        return response()->json($events);
    }

    public function getEventById(Request $request, $id) {
        $event = Event::where('id', '=', $id)->with('files', 'tickets')->first();

        $event['startdate'] = Carbon::parse(str_replace('"', "",$event->event_date_time))->format('m/d/y');;
        $event['starttime'] = Carbon::parse(str_replace('"', "",$event->event_date_time))->format('H:i:s');


        if($event) {
             return response()->json([$event], 200);
         } else {
             return response()->json(['message' => 'Unable to find an event with that id'], 400);
         }
    }

    public function getVendorEvents(Request $request) {
        $events = Event::where('user_id', '=', Auth::user()->id)->with('files', 'tickets')->get();
         if($events) {
             return response()->json($events, 200);
         } else {
             return response()->json(['message' => 'Unable to find an event with that id'], 400);
         }
    }


    /**
     * Create event
     * $param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\JsonResponse
     */
    public function createEvent(Request $request) {
        $geoResult = null;
        $request->validate([
            'title' => 'required|string',
            'body' => 'required|string',
            'location' => 'required|string',
        ]);

        if($request->has('location')) {
             // url encode the address
            $address = urlencode($request->input('location'));

            // google map geocode api url
            $url = "https://maps.googleapis.com/maps/api/geocode/json?address={$address}&key=AIzaSyAhpzcuE5Iw67yvzrIcIxwrKN0M18iDcxM";

            // get the json response
            $resp_json = file_get_contents($url);

            // decode the json
            $resp = json_decode($resp_json, true);
            if($resp['status']=='OK') {
                 $geoResult = $resp;
            }
        }

        $formatedDateTime = Carbon::parse(str_replace('"', "",$request->event_date_time))->format('Y-m-d H:i:s');
        $event = new Event([
            'title' => $request->title,
            'event_date_time' => $formatedDateTime,
            'location' => $request->location,
            'postcode' => $request->postcode,
            'body' => $request->body,
            'longitude' => !empty($geoResult) ? $geoResult['results'][0]['geometry']['location']['lng'] : NULL,
            'latitude' => !empty($geoResult) ? $geoResult['results'][0]['geometry']['location']['lat'] : NULL,
            'user_id' => Auth::user()->id,
        ]);
        $event->save();



        if($request->hasFile('images')) {
            $this->addEventImage($request, $event->id);
        }

        $message = Auth::user()->flag == 'en' ? 'Successfully created event.' : 'אירוע שנוצר בהצלחה';
        if($event) {
            return response()->json([
                'message' => $message,
                'event' => $event->id,
            ], 200);
        } else {
            return response()->json([
                'message' => 'Unable to create event'
            ], 400);
        }
    }

    /**
     * Create image on event
     * $param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\JsonResponse
     */
    public function addEventImage(Request $request, $id) {
        $file = '';
        $maximumImageUpload = 5;
        $imageCount = DB::table('files')
            ->where('user_id', '=', Auth::user()->id)
            ->where('event_id', '=', $id)
            ->count();

        if($imageCount > $maximumImageUpload) {
            return response()->json([
                'message' => 'You can only upload a maximum' . $maximumImageUpload . ' images.'
            ], 200);
        }

        if($request->hasFile('images')) {
            $images = $request->file('images');
            foreach($images as $image){
                $fileName = mt_rand() . time() . '.png';

                $img = Image::make($image->getRealPath());
                $img->resize(450, 450, function ($constraint) {
                    $constraint->aspectRatio();
                });

                $img->stream('png'); // <-- Key point
                Storage::disk('public')->put('event/images'.'/'.$fileName, $img, 'public');

                $file = new File;
                $file->file = $fileName;
                $file->user_id =  Auth::user()->id;
                $file->event_id = $id;
                $file->save();
            }

            $message = Auth::user()->flag == 'en' ? 'Successfully added image.' : 'תמונה שנוספה בהצלחה';
            if($file) {
                return response()->json([
                    'message' => $message
                ], 200);
            } else {
                return response()->json([
                    'message' => 'Unable to add image'
                ], 400);
            }
        }
    }

    public function removeEventImage(Request $request, $id) {
        $file = DB::table('files')->where('id', '=', $id)->first();
        // Remove file from dir
        Storage::delete('event/images'.'/'.$file->file);
        // Remove file from table
        $delete = DB::table('files')->where('id', $id)->delete();

        $message = Auth::user()->flag == 'en' ? 'Successfully removed image.' : 'התמונה הוסרה בהצלחה.';
        if($delete) {
            return response()->json([
                'message' => $message
            ], 200);
        } else {
            return response()->json([
                'message' => 'Unable to remove image.'
            ], 400);
        }
    }

    /**
     *  Update Event
     *  @param  Illuminate\Http\Request $request.
     *  @param [int] $id
     *
     * @return Illuminate\Http\JsonResponse
     */
    public function updateEvent(Request $request, $id) {
        $update = DB::table('events')->where('id', $id);

        $formatedDateTime = Carbon::parse(str_replace('"', "",$request->event_date_time))->format('Y-m-d H:i:s');

        if($request->has('title')) {
            $update->update([
                'title' => $request->title,
                'body' => $request->body,
                'event_date_time' =>  $formatedDateTime,
                'postcode' => $request->postcode,
            ]);
        }


        if($request->has('location')) {
             // url encode the address
            $address = urlencode($request->input('location'));

            // google map geocode api url
            $url = "https://maps.googleapis.com/maps/api/geocode/json?address={$address}&key=AIzaSyAhpzcuE5Iw67yvzrIcIxwrKN0M18iDcxM";

            // get the json response
            $resp_json = file_get_contents($url);

            // decode the json
            $resp = json_decode($resp_json, true);
            if($resp['status']=='OK') {
                 $geoResult = $resp;
            }

             $update->update([
                'longitude' => !empty($geoResult) ? $geoResult['results'][0]['geometry']['location']['lng'] : NULL,
                'latitude' => !empty($geoResult) ? $geoResult['results'][0]['geometry']['location']['lat'] : NULL,
                 'location' => $request->location,
             ]);
        }

        $message = Auth::user()->flag == 'en' ? 'Successfully updated event' : 'אירוע מעודכן בהצלחה';
        if($update) {
            return response()->json([
                'message' => $message
            ], 200);
        } else {
            return response()->json([
                'message' => 'Unable to update event.'
            ], 400);
        }
    }

    /**
     * Delete Event
     *
     * @param [int] $id
     * @return Illuminate\Http\JsonResponse
     */
    public function deleteEvent(Request $request, $id) {
        $events = Event::where('id', $id)->with('files')->get();
        foreach($events as $event) {
            if(!empty($event->files)) {
                foreach ($event->files as $file) {
                    $this->removeEventImage($request, $file->id);
                }
            }
        }

        $deleteEvent  = DB::table('events')->where('id', $id)->delete();
        $message = Auth::user()->flag == 'en' ? 'Successfully removed event.' : 'האירוע הוסר בהצלחה';
        if($deleteEvent) {
            return response()->json([
                'message' => $message
            ], 200);
        } else {
            return response()->json([
                'message' => 'Unable to remove event'
            ], 400);
        }
    }
}
