<?php
namespace App\Http\Controllers\Events;

use App\Http\Controllers\Controller;
use App\Models\EventTicket;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

use App\Models\EventPurchasedTickets;

class EventPurchasedTicketController extends Controller {

    public function __construct()
    {
    }

    /**
     * Log what tickets have been purchased
     * $param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\JsonResponse
     */
    public function createEventPurchasedTickets(Request $request) {
        $request->validate([
            'amount' => 'required',
        ]);

        // Get the origional amount of tickets available.
        $originalTicketAmount = EventTicket::where('id',  $request->event_ticket_id)->first();

        // If the amount of tickets requested is greater than the amount available.
        if($request->amount > $originalTicketAmount->amount) {
            return response()->json([
                'message' => 'Sorry, there are' . $originalTicketAmount->amount . ' tickets left to purchase',
            ], 200);
        }

        // Otherwise save the ticket purchases
        $event = new EventPurchasedTickets([
            'amount' => $request->amount,
            'price' => $request->price,
            'event_ticket_id' => $request->event_ticket_id,
            'merchant' => $request->merchant,
            'user_id' => Auth::user()->id,
        ]);
        $event->save();


        // Update the available tickets and take away the amount of tickets that were successfully brought.
        $ticketsAvailable = $originalTicketAmount->amount - $request->amount;
        $updateAvailableTickets = $originalTicketAmount->update([
                'amount' => $ticketsAvailable,
            ]);

        if($event && $updateAvailableTickets) {
            return response()->json([
                'message' => 'Successfully purchased tickets',
            ], 200);
        } else {
            return response()->json([
                'message' => 'Unable to purchase tickets'
            ], 400);
        }
    }


    /**
     *  Update Amount of tickets available for an event
     *  @param  Illuminate\Http\Request $request.
     *  @param [int] $id
     *
     * @return Illuminate\Http\JsonResponse
     */
    public function updateEventPurchasedTickets(Request $request, $id) {
        $update = EventPurchasedTickets::where('id', $id)
            ->update([
                'amount' => $request->amount,
                'price' => $request->price,
            ]);

        if($update) {
            return response()->json([
                'message' => 'Successfully updated event ticket information.'
            ], 200);
        } else {
            return response()->json([
                'message' => 'Unable to update event ticket information.'
            ], 400);
        }
    }


    /**
     *  List event tickets
     */
    public function getPurchasedTickets() {
        $purchasedTickets = EventPurchasedTickets::with( 'eventTicket', 'EventTicket.event')->get();

        return response()->json([
            $purchasedTickets,
        ], 200);
    }
}
