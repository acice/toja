<?php
namespace App\Http\Controllers\Preference;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\PricePreference;
use Illuminate\Support\Facades\Auth;
use App\Models\File;
use App\Models\Bank;
use App\Models\User;
use App\Models\Vendor;
use App\GoogleTranslate;
use App\Models\Category;
use App\Models\CustomTime;
use App\Models\CategoryLink;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;
use App\Rules\MatchOldPassword;
use Illuminate\Support\Facades\Hash;
use DB;
use Log;
use Illuminate\Validation\Rule;



class PreferenceController extends Controller
{

    public function __construct()
    {

    }

    public function updatePrice(PricePreference $pricePreference, Request $request) {
        $validated = $request->validate([
            'price_preference' => 'required',
        ]);
        if(is_string($request->price_preference)){
            $pricePreferences = explode(',', $request->price_preference);
        }else{
            $message =trans('validation.invalid_data');
            return response()->json([
                'message' => $message
            ], 400);
        }
        
          if(Auth::user()->identifier == 0) {
                $authId = Auth::user()->id;
                $pricePreference->removeUserPreferences($authId);
            } else {
                $vendorId = Vendor::where('user_id', Auth::user()->id)->first()->id;
                $pricePreference->removeVendorPreferences($vendorId);
            }

        
        foreach ($pricePreferences as $value) {  
            $pricePreference = new PricePreference();
            $pricePreference->price_preference = $value;
            if(Auth::user()->identifier == 0) {
                $pricePreference->user_id = $authId;
            } else {
                $pricePreference->vendor_id = $vendorId;
            }
            $pricePreference->save();
        }
        
        $message = trans('validation.price_preference_set');
        if($pricePreference) {
            return response()->json([
                'message' => $message
            ], 200);
        } else {
            return response()->json([
                'message' => trans('validation.unable_to_add_Price')
            ], 400);
        }
    }

    public function addImage(Request $request, $id)
    {
        $identify= (Auth::user()->identifier == 0)? 'user_id ' :'vendor_id';
        $file               = '';
        $maximumImageUpload = 5;
        $imageCount         = DB::table('files')
            ->where($identify, '=', $id)
            ->count();

        if ($imageCount > $maximumImageUpload) {
            return response()->json(
                [
                    'message' => trans('validation.max_image_upload') . $maximumImageUpload,
                ],
                200
            );
        }

        if ($request->hasFile('images')) {
            $images = $request->file('images');
            foreach ($images as $image) {
                $fileName = mt_rand() . time() . '.jpg';

                $img = Image::make($image->getRealPath());
                $img->resize(
                    450,
                    450,
                    function ($constraint) {
                        $constraint->aspectRatio();
                    }
                );

                $img->stream('jpg',80); // <-- Key point
                Storage::disk('public')
                    ->put(
                        'vendor/images/' . Auth::user()->id . '/' . $fileName,
                        $img,
                        'public'
                    );

                $file            = new File;
                $file->file      = $fileName;
                $file->user_id   = Auth::user()->id;
                $file->vendor_id = $id;
                $file->save();
            }

            $message = trans('validation.successful_upload_image');
            if ($file) {
                return response()->json(
                    [
                        'message' => $message,
                    ],
                    200
                );
            }

            return response()->json(
                [
                    'message' => trans('validation.unable_upload_image'),
                ],
                400
            );
        }
    }

    public function updatePersonalInfo(Request $request){
       
        $validated = $request->validate([
            // 'email' => 'required|email|max:100',
            'name' => 'required|min:3|max:50',
            'street' => 'max:100',
            'city' => 'max:100',
            'phone_no' => [
                'required',
                Rule::unique('users')->ignore(Auth::user()->id),
                'max:100','regex:/^([0-9\s\-\+\(\)]*)$/'
            ],
        ]);

        //update avatar user
        if ($request->hasFile('avatar')) {
            $request->validate([
                'avatar' => 'mimes:jpeg,jpg,png|max:7000'
            ]);
            $image= $request->file('avatar');
                $fileName = mt_rand() . time() . '.jpg';

                $img = Image::make($image->getRealPath());
                $img->resize(
                    450,
                    450,
                    function ($constraint) {
                        $constraint->aspectRatio();
                    }
                );

                $img->stream('jpg',80); // <-- Key point
                Storage::disk('public')->put(
                    'avatars/' . Auth::user()->id . '/' . $fileName,
                    $img,
                    'public'
                );
                $old_image = User::where('id', Auth::user()->id)->pluck('avatar')->first();
                $old_image_path='public/avatars/' . Auth::user()->id . '/' . $old_image ;
                if(File::exists($old_image_path)) {
                    Storage::delete($old_image_path);
                }
                $avatar = User::where('id', Auth::user()->id)
                    ->update(
                        [
                            'avatar' => $fileName,
                        ]
                    );
        }
        $update = Auth::user();
        $update->update(
            [
                'name'           => $request->input('name'),
                // 'email'          => $request->input('email'),
                'phone_no'       => $request->input('phone_no'),
            ]
        );
        
        if(Auth::user()->identifier==1){
            $update = Vendor::where('user_id', Auth::user()->id)->first();
            $name=!empty($request->input('name'))?(new GoogleTranslate)->translate($request->input('name'), 'en')['text']:'';
            $nameIW=!empty($request->input('name'))?(new GoogleTranslate)->translate($request->input('name'), 'iw')['text']:'';
            $body=!empty($request->input('body'))?(new GoogleTranslate)->translate($request->input('body'), 'en')['text']:'';
            $bodyIW=!empty($request->input('body'))?(new GoogleTranslate)->translate($request->input('body'), 'iw')['text']:'';
            $postcodeIW=!empty($request->input('postcode'))?(new GoogleTranslate)->translate($request->input('postcode'), 'iw')['text']:'';
            $company_numberIW=!empty($request->input('company_number'))?(new GoogleTranslate)->translate($request->input('company_number'), 'iw')['text']:'';
            $cityIW=!empty($request->input('city'))?(new GoogleTranslate)->translate($request->input('city'), 'iw')['text']:'';
            $streetIW=!empty($request->input('street'))?(new GoogleTranslate)->translate($request->input('street'), 'iw')['text']:'';
            try{
                $update->update(
                    [
                        'name' => $request->input('name'),
                        'iw_name' => $request->input('name'),
                        'body' => $body,
                        'iw_body' => $bodyIW,
                        'postcode'       => $request->input('postcode'),
                        'company_number' => $request->input('company_number'),
                        'address'        => $request->input('address'),
                        'street'        => $request->input('street'),
                        'city'        => $request->input('city'),
                        'data'           => json_encode(
                            [
                                'name'           => $nameIW,
                                'body'           => $bodyIW,
                                'postcode'       => $postcodeIW,
                                'company_number' => $company_numberIW,
                                'city' => $cityIW,
                                'street' => $streetIW
                            ]
                        ),
                    ]
                );
            }  catch (\Exception $e) {
                return response()->json([
                    'message' => $e->getMessage(),500
                ]);
            } 
        }


        $message = trans('validation.successfully_updated');
        if ($update) {
            return response()->json(
                [
                    'message' => $message,
                ],
                200
            );
        } else {
            return response()->json(
                [
                    'message' => trans('validation.unable_to_update'),
                ],
                400
            );
        }
    }

    public function updateBankInfo(Request $request){
        $validated = $request->validate([
            'account_no' => 'required|max:8',
            'name' => 'required|max:100',
            'branch' => 'required|max:3',
        ]);

        $vendor = Vendor::where('user_id', Auth::user()->id)->first();
    
        if(!empty($vendor)){
            try{
                $bank=Bank::where('vendor_id',$vendor->id)->first();
                if(!empty($bank)){
                    $bank->update([
                        'account_no'        => $request->input('account_no'),
                        'routing_no'        => $request->input('routing_no'),
                        'name'              => $request->input('name'),
                        'branch'            => $request->input('branch'),
                        'address'           => $request->input('address')
                    ]);
                }else{
                    Bank::create([
                        'vendor_id'         => $vendor->id,
                        'account_no'        => $request->input('account_no'),
                        'routing_no'        => $request->input('routing_no'),
                        'name'              => $request->input('name'),
                        'branch'            => $request->input('branch'),
                        'address'           => $request->input('address')
                    ]);
                }
               
                $message = trans('validation.successfuly_updated');
            }  catch (\Exception $e) {
                return response()->json([
                    'message' => $e->getMessage(),500
                ]);
            } 
        }else{
            $message = trans('validation.vendor_unavailable');
        }
      
    
        $message =trans('validation.successfuly_updated');
        if ($vendor) {
            return response()->json(
                [
                    'message' => $message,
                ],
                200
            );
        } else {
            return response()->json(
                [
                    'message' => trans('validation.unable_to_update'),
                ],
                400
            );
        }
    }
  

    public function updateLocation(Request $request){
       
        $validated = $request->validate([
            'latitude' => 'required|max:100',
            'longitude' => 'required|max:100',
        ]);
      
        $update = Vendor::where('user_id', Auth::user()->id)->firstOrFail();
        
            $lat = !empty($request->latitude)?$request->latitude:null;
            $long = !empty($request->longitude)?$request->longitude:null;
        
            $update->update(
                [
                    'longitude' => $long,
                    'latitude'  => $lat,
                ]
            );
        $message = trans('validation.successfuly_updated_location');
        if ($update) {
            return response()->json(
                [
                    'message' => $message,
                ],
                200
            );
        } else {
            return response()->json(
                [
                    'message' =>trans('validation.unable_to_update_location'),
                ],
                400
            );
        }
    }

    // update or add user categories
    public function updateOrAddCategories(CategoryLink $category, Request $request) {

        if(Auth::user()->identifier == 0) {
                $authId = Auth::user()->id;
                $category->removeUserCategories($authId);
                
            } else {
                $vendorId =  Vendor::where('user_id', Auth::user()->id)->first()->id;
                $delete = $category->removeVendorCategories($vendorId);
        }


        if(is_string($request->input('category_ids'))){
            $category_ids = explode(',', $request->input('category_ids'));
        }else{
            return response()->json([
                'message' => trans('validation.invalid_data')
            ], 400);
        }

        if(Auth::user()->identifier == 1 && count($category_ids)>1) {
            return response()->json([
                'message' => trans('validation.select_one_category')
            ], 400);
        }

        if($category_ids[0] == "") {
            return response()->json([
                'message' => trans('validation.select_atleast_one_category')
            ], 400);
        }

        foreach ($category_ids as $category_id) {
            $categoryLink = new CategoryLink();
            if($categoryLink->where('category_id', '=', $category_id)
                            ->where('user_id', '=', Auth::user()->id)->first()) {
                continue;
            }

            if(Auth::user()->identifier == 0) {
                $categoryLink->user_id = $authId;
            } else {
                 $categoryLink->vendor_id = $vendorId;
            }
        
            $categoryLink->category_id = $category_id;
            $categoryLink->save();
        }
        
        $message = trans('validation.update_category_preferences');
        if($categoryLink) {
            return response()->json([
                'message' => $message
            ], 200);
        } else {
            return response()->json([
                'message' => trans('validation.unable_update_category')
            ], 400);
        }
    }


    public function updateDistance(Request $request) {
        $validated = $request->validate([
            'distance' => 'required|max:10',
        ]);

         $distancePreference = User::where('id', Auth::user()->id)
                 ->update([
            'distance' => $request->input('distance'),
        ]);
      
        $message = trans('validation.update_distance_preferences');
        if($distancePreference) {
            return response()->json([
                'message' => $message
            ], 200);
        } else {
            return response()->json([
                'message' => trans('validation.unable_update_distance')
            ], 400);
        }
    }

    public function updateOperatingTime(Request $request) {
        $validated = $request->validate([
            'opening_days' => 'required|max:100',
            'opening_time' => 'required|max:20',
            'closing_time' => 'required|max:20',
        ]);
        if(Auth::user()->identifier == 1) {
            $operatingTimePreference = Vendor::where('user_id', Auth::user()->id)
                    ->update([
                'opening_days' => $request->input('opening_days'),
                'opening_time' => $request->input('opening_time'),
                'closing_time' => $request->input('closing_time'),
            ]);
        }else{
            return response()->json([
                'message' =>  trans('validation.only_vendor_can_update_operation')
            ], 400); 
        }
      
        $message =trans('validation.update_operation_time');
        if($operatingTimePreference) {
            return response()->json([
                'message' => $message
            ], 200);
        } else {
            return response()->json([
                'message' => trans('validation.unable_update_operation_time')
            ], 400);
        }
    }

    public function updateCustomTime(Request $request) {
        
        $custom_days = json_decode($request->getContent(), true);

        if(Auth::user()->identifier == 1) {
            if(!empty($custom_days)){
                $vendor=Vendor::where('user_id', Auth::user()->id)->first();
                foreach ($custom_days as $key => $custom_day) {
                    $day=$custom_day['day'];
                    $opening=$custom_day['opening'];
                    $closing=$custom_day['closing'];
                    $status=$custom_day['status'];

                    $operatingTimePreference=CustomTime::updateOrCreate([
                        'vendor_id'   => $vendor->id,
                        'day'   => $day,
                    ],[
                        'opening' => $opening,
                        'closing' => $closing,
                        'status'   => $status
                    ]);

                }
            }
        }else{
            return response()->json([
                'message' =>  trans('validation.only_vendor_can_update_operation')
            ], 400); 
        }
      
        $message =trans('validation.update_operation_time');
        if(!empty($operatingTimePreference)){
            return response()->json([
                'message' => $message
            ], 200);
        } else {
            return response()->json([
                'message' => trans('validation.unable_update_operation_time')
            ], 400);
        }
    }

    public function updateOnboardingStatus(Request $request){
        $validated = $request->validate([
            'onboarding_status' => 'required|max:20',
        ]);
        $statusOnboarding = User::where('id', Auth::user()->id)
        ->update([
            'onboarding_status' => $request->input('onboarding_status'),
        ]);
        $message = trans('validation.onboarding_complete');
        if($statusOnboarding) {
            return response()->json([
                'message' => $message
            ], 200);
        } else {
            return response()->json([
                'message' => trans('validation.unable_onboarding_complete')
            ], 400);
        }
    }

    public function changePassword(Request $request)
    {
        if(Auth::user()->password==''){
            $request->validate([
                'new_password' => ['required'],
                'new_confirm_password' => ['same:new_password'],
            ]);
        }else{
            $request->validate([
                'current_password' => ['required', new MatchOldPassword],
                'new_password' => ['required'],
                'new_confirm_password' => ['same:new_password'],
            ]);
        }
        

        $update=User::find(Auth::user()->id)->update(['password'=> Hash::make($request->new_password)]);
        $message =trans('validation.password_change');
        return response()->json([
            'message' => $message
        ], 200);
    }

    public function updateAvatar(Request $request)
    {
        $request->validate([
            'avatar' => 'mimes:jpeg,jpg,png|max:7000',
            'body' => 'max:300'
        ]);
        if(!$request->hasFile('avatar')){
            $existing_image = User::where('id', Auth::user()->id)->pluck('avatar')->first();
           
            if(empty($existing_image)){
                return response()->json(
                    [
                        'message' => trans('validation.image_required'),
                    ],
                    422
                );
            }
        }
        
        $update = '';

        //update description vendor
        if ($request->has('body')) {
            $body=!empty($request->input('body'))?(new GoogleTranslate)->translate($request->input('body'), 'en')['text']:'';
            $bodyIW=!empty($request->input('body'))?(new GoogleTranslate)->translate($request->input('body'), 'iw')['text']:'';
            $update=Vendor::where('user_id', Auth::user()->id)
            ->update(
                [
                    'body' => $body,
                    'iw_body' => $bodyIW
                ]
            );
        }

        
        //update avatar user
        if ($request->hasFile('avatar')) {
            $image= $request->file('avatar');
                $fileName = mt_rand() . time() . '.jpg';

                $img = Image::make($image->getRealPath());
                $img->resize(
                    450,
                    450,
                    function ($constraint) {
                        $constraint->aspectRatio();
                    }
                );

                $img->stream('jpg',80);// <-- Key point
                Storage::disk('public')->put(
                    'avatars/' . Auth::user()->id . '/' . $fileName,
                    $img,
                    'public'
                );
                $old_image = User::where('id', Auth::user()->id)->pluck('avatar')->first();
                $old_image_path='public/avatars/' . Auth::user()->id . '/' . $old_image ;
                if(File::exists($old_image_path)) {
                    Storage::delete($old_image_path);
                }
                $update = User::where('id', Auth::user()->id)
                    ->update(
                        [
                            'avatar' => $fileName,
                        ]
                    );
        }

        $message = trans('validation.updated_profile');
        if ($update) {
            return response()->json(
                [
                    'message' => $message,
                ],
                200
            );
        }

        return response()->json(
            [
                'message' => trans('validation.unable_updated_profile'),
            ],
            400
        );
    }

    public function setting(){
        $setting=DB::table('setting')->first();
        if ($setting) {
            return response()->json($setting,200);
        }
        else{
            return response()->json([
                'message' =>  trans('validation.unable_setting')
            ], 400);
        }
    }

    public function getCustomTime(){
        $vendorId = Vendor::where('user_id', Auth::user()->id)->first()->id;
        $custom_times=CustomTime::select('id','day','status','opening','closing')->where('vendor_id',$vendorId )->get();
        if ($custom_times) {
            return response()->json($custom_times,200);
        }
        else{
            return response()->json([
                'message' =>  trans('validation.unable_customtime')
            ], 400);
        }
    }

}
