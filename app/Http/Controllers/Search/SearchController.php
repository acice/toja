<?php

namespace App\Http\Controllers\Search;

use App\Models\File;
use App\Models\User;
use App\Models\Post;
use App\Models\Vendor;
use App\GoogleTranslate;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

/**
 * SearchController
 */
class SearchController extends Controller
{

        /**
     * Search By Vendor
     *
     * @param  [Request] $request.
     * @return [Array] $following
     */
    public function searchVendor(Request $request)
    {
        $user=Auth::user();
        $longitude = $request->input('longitude');
        $latitude = $request->input('latitude');
        $input_search = '%' . $request->input('search') . '%';
        $vendors=Vendor::whereHas('user')->with('user');

        if ($request->has('search')) {
            $vendors->where('name', 'like', $input_search)->orWhere('body', 'like', $input_search);
        }

        
        if ($latitude && $longitude) {
            $vendors = $vendors->geofence($latitude, $longitude,0,$user->distance)->orderBy('distance', 'ASC');
        }

        $vendors = $vendors->simplePaginate(10);

        if ($vendors) {
            return response()->json($vendors, 200);
        } else {
            return response()->json(
                [
                    'message' => trans('validation.something_went_wrong'),
                ],
                400
            );
        }
    }

    public function searchPost(Request $request)
    {
        $user=Auth::user();
        $longitude = $request->input('longitude');
        $latitude = $request->input('latitude');
        $input_search = '%' . $request->input('search') . '%';
        $vendors=Vendor::whereHas('user')->with('user');
        $posts=Post::whereHas('user')->with('files','user');

        if ($request->has('search')) {
            $posts->where(function($query) use ($input_search) {
                $query->where('title', 'LIKE', '%'.$input_search.'%')
                    ->orWhere('body', 'LIKE', '%'.$input_search.'%');
            });
        }
        
        if ($latitude && $longitude) {
            $vendors = $vendors->geofence($latitude, $longitude,0,$user->distance)->orderBy('distance', 'ASC');
        }
    
        $posts = $posts
            ->where('discount_end','>',now()->toDateTimeString())
            ->simplePaginate(10);
  

        if ($posts) {
            return response()->json($posts, 200);
        } else {
            return response()->json(
                [
                    'message' => trans('validation.something_went_wrong'),
                ],
                400
            );
        }
    }

}
