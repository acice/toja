<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BankResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'account_no' => $this->account_no,
            'name' => $this->name,
            'branch' => $this->branch,
            'routing_no' => $this->routing_no,
            'address' => $this->address
        ];
    }
}
