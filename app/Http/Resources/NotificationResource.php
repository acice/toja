<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class NotificationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $human_time=$this->created_at->diffForHumans();
        $data=json_encode($this->data[0]);
        $formated_data=json_decode($data);
        return [
            'id' => $this->id,
            'type' =>$formated_data->type,
            'created_at' => (String)$this->created_at,
            'updated_at' => (String)$this->updated_at,
            'deal_id' =>$formated_data->id,
            'post' => $formated_data->post,
            'user' => $formated_data->user,
            'payment' => $formated_data->payment,
            'data' =>json_decode($data),
            'time' => $human_time,
            'read_at' => (String)!empty($this->read_at)?(String)$this->read_at:$this->read_at,
            
        ];
    }

    /**
     * Get additional data that should be returned with the resource array.
     *
     * @param \Illuminate\Http\Request  $request
     * @return array
     */
    public function with($request)
    {
        return [
            'meta' => [
                'key' => 'value',
            ],
        ];
    }
}
