<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Auth;
use Carbon\Carbon;

class Localization
{
    /**
    * Handle an incoming request.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \Closure  $next
    * @return mixed
    */
    public function handle(Request $request, Closure $next)
    {
      
        if(isset($request->flag)){
            $flag=$request->flag;
            App::setLocale($flag);
            if($flag=='iw'){
                Carbon::setLocale('he');
            }
        }elseif (Auth::user()) {
            $flag=Auth::user()->flag;
            App::setLocale($flag);
            if($flag=='iw'){
                Carbon::setLocale('he');
            }
        }
        return $next($request);
    }
    
}