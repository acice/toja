<?php

namespace App\Location;

use App\Location\Drivers\Driver;
use Illuminate\Support\Optional;
use App\Location\Exceptions\DriverDoesNotExistException;

class Location
{
    /**
     * The current driver.
     *
     * @var Driver
     */
    protected $driver;

    /**
     * Constructor.
     *
     * @throws DriverDoesNotExistException
     */
    public function __construct()
    {
        $this->setDefaultDriver();
    }

    /**
     * Creates the selected driver instance and sets the driver property.
     *
     * @param Driver $driver
     */
    public function setDriver(Driver $driver)
    {
        $this->driver = $driver;
    }

    /**
     * Sets the default driver from the configuration.
     *
     * @throws DriverDoesNotExistException
     */
    public function setDefaultDriver()
    {
        // Retrieve the default driver.
        $driver = $this->getDriver($this->getDefaultDriver());

        foreach($this->getDriverFallbacks() as $fallback) {
            // We'll add each fallback to our responsibility chain.
            $driver->fallback($this->getDriver($fallback));
        }

        // Finally, set the driver.
        $this->setDriver($driver);
    }

    /**
     * Retrieve the users location.
     *
     * @param array|string $ip
     *
     * @return \App\Location\Position|bool
     */
    public function get($ip = '')
    {
        $ip = $ip === '127.0.0.1' ? '62.30.222.145' : $ip;
        if ($location = $this->driver->get($ip ?: request()->ip())) {
            return new Optional($location);
        }

        return new Optional(false);
    }

    /**
     * Retrieves the config option for select driver fallbacks.
     *
     * @return array
     */
    protected function getDriverFallbacks()
    {
        return [
            \App\Location\Drivers\FreeGeoIp::class,
            \App\Location\Drivers\IpInfo::class
        ];
    }

    /**
     * Returns the selected driver
     *
     * @return \Illuminate\Support\Facades\Config
     */
    protected function getDefaultDriver()
    {
        return \App\Location\Drivers\GeoPlugin::class;
    }

    /**
     * Returns the specified driver.
     *
     * @param string $driver
     *
     * @return Driver
     *
     * @throws DriverDoesNotExistException
     */
    protected function getDriver($driver)
    {
        if (class_exists($driver)) {
            return new $driver();
        }

        throw new DriverDoesNotExistException("The driver [{$driver}] does not exist.");
    }
}
