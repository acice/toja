<?php

namespace App\Location\Exceptions;

class DriverDoesNotExistException extends LocationException
{
    //
}
