<?php

namespace App\Console;

use App\Jobs\ProcessEmailInvoices;
use App\Jobs\ProcessVendorInvoice;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {    
        // Test to dispatch the invoices and emails to the vendor immediately.
        
        // $schedule->call(function () {
        //     ProcessVendorInvoice::dispatchNow();
        // });
        
        // $schedule->call(function () {
        //     ProcessEmailInvoices::dispatchNow();
        // });
        
        $schedule->call(function () {
            ProcessVendorInvoice::dispatch();
            ProcessEmailInvoices::dispatch();
        })->monthly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
