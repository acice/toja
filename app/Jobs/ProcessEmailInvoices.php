<?php

namespace App\Jobs;

use App\Models\Post;
use App\Models\Invoice;
use App\Mail\EmailInvoice;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ProcessEmailInvoices implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $invoices = Invoice::where('paid', 0)
            ->whereNull('email_sent')
            ->get();
            
        // If no invoices found then dont do anything.
        if (!$invoices) {
            return;
        }
        collect($invoices)->map(function($invoice) {

            Mail::to($invoice->email)
                ->send(new EmailInvoice($invoice));
                
            $invoice->update([
                'email_sent' => now()
            ]);
        });
    }
}
