<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Log;
use App\Models\ActivatedDeals;
use App\Models\User;
use App\Notifications\DealRejected;

class StatusChangeDeals implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $deal;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(ActivatedDeals $deal)
    {
        $this->deal = $deal;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if($this->deal->status=='pending'){
            $this->deal->update([
                'status' => 'rejected',
                'reject_reason' => 'Vendor No Response'
            ]);
            $notifiableUser=User::where('id',$this->deal->user_id)->first();
            $notifiableUser->deal_id=$this->deal->id;
            $notifiableUser->notify(new DealRejected($notifiableUser));
        }
    }
}
