<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Models\Post;
use App\Models\Invoice;
use Illuminate\Support\Carbon;

class ProcessVendorInvoice implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $startOfMonth = Carbon::now()
            ->startOfMonth()
            ->toDateString();
        
        $endOfMonth = Carbon::now()
            ->endOfMonth()
            ->toDateString();
        
        $deals = Post::whereHas('activatedDeals', function ($query) use (
            $startOfMonth,
            $endOfMonth
        ) {
            // Only get deals that have activated deals.
            // Only get the deals that have activations done this month.
            return $query
                ->whereNotNull('confirmed')
                ->whereBetween('created_at', [$startOfMonth, $endOfMonth]);
        })
            ->with('user.vendor')
            ->withCount([
                'activatedDeals',
                'activatedDeals as activated_deals_count' => function ($query) use (
                    $startOfMonth,
                    $endOfMonth
                ) {
                    // count all the activated deals on a deal that have been confirmed.
                    // Only count the deals that have have been activated this month
                    $query
                        ->whereNotNull('confirmed')
                        ->whereBetween('created_at', [$startOfMonth, $endOfMonth]);
                }
            ])
            ->get();
                
        //If a deal hasn't had any activations then ignore it.
        if (!$deals) {
            return;
        }
        
        foreach ($deals as $deal) {            
            $amountSaved = number_format(($deal->discount_cost / $deal->discount_amount), 2); 
            $amountMade  =  $amountSaved * $deal->activated_deals_count;
            $adminFeeCharge = number_format($amountMade * 0.14, 2);

            Invoice::create([
                'name' => $deal->user->name,
                'description' => 'The fee to be paid is the total amount that each user saved on all the discounts.',
                'price' => $amountMade,
                'admincharge' => $adminFeeCharge,
                'amountsaved' => $amountSaved,
                'dealsactivated' => $deal->activated_deals_count,
                'discount' => $deal->discount_amount,
                'vendor_id' => $deal->user->vendor->id,
                'email' => $deal->user->email
            ]);
        }
    }
}
