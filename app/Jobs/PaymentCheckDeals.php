<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use App\Models\User;
use App\Notifications\DealCancelled;

class PaymentCheckDeals implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $deal;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($deal)
    {
        $this->deal=$deal;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if($this->deal->status=='inprogress'){
            $payment=$this->deal->payment()->first();
            if($payment->payment_status=='unpaid'){
                $this->deal->update([
                    'status' => 'cancelled',
                    'reject_reason' => 'Failed payment'
                ]);

                $notifiableVendor=$this->deal->vendor()->first()->user;
                $notifiableVendor->deal_id=$this->deal->id;
                $notifiableVendor->customer='payment timeout';
                $notifiableVendor->notify(new DealCancelled($notifiableVendor));
            }
        }
    }
}
