<?php

namespace App\Services;

use PayPalCheckoutSdk\Core\PayPalHttpClient;
use PayPalCheckoutSdk\Core\SandboxEnvironment;

class PayPalClient
{
/**
 * Returns PayPal HTTP client instance with environment that has access
 * credentials context. Use this instance to invoke PayPal APIs, provided the
 * credentials have access.
 */
    public static function client()
    {
        return new PayPalHttpClient(self::environment());
    }

    /**
     * Set up and return PayPal PHP SDK environment with PayPal access credentials.
     * This sample uses SandboxEnvironment. In production, use LiveEnvironment.
     */
    public static function environment()
    {
        $clientId = env("PAYPAL_SANDBOX_KEY") ?: "AZVab7_8l9xiyMr9dgRMWgx6ICof5rMqas64CGwSq5zho09LwQGojZ-bnmVvSwSV9rg8Sewsrch_NC7c";
        $clientSecret = env("PAYPAL_SANDBOX_SECRET") ?: "EH_oaQWu6cGI-roPdmsKCaR8s8XMXn-ypwVyraHlwCbtOElhE6d5_eElWCoEt9Bgrw2OPNLoW0WDjZZ3";
        return new SandboxEnvironment($clientId, $clientSecret);
    }
}
