<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Stichoza\GoogleTranslate\GoogleTranslate;

class Followers extends Model
{


	private $googleTranslate;

    public function __construct() {
        $this->googleTranslate = new GoogleTranslate();
    }

    public function getNameAttribute() {
        return $this->googleTranslate->setSource('en')->setTarget(Auth::user()->flag)->translate($this->attributes['name']);
    }

	public function following() {
    	return $this->belongsTo(User::class, 'followable_id');
    }

}
