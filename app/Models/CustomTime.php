<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;


class CustomTime extends Model 
{


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'status', 'day','opening','closing','vendor_id'
    ];

  


    public function vendor()
    {
        return $this->belongsTo(Vendor::class);
    }

    // public function getDayAttribute()
    // {
    //     if (!Auth::user()->flag && Auth::user()->flag != 'iw') {
    //         return $this->attributes['day'];
    //     }

    //     return $this->attributes['day'] = trans('days.'.$this->attributes['day']);
    // }

    // public function getStatusAttribute()
    // {
    //     if (!Auth::user()->flag && Auth::user()->flag != 'iw') {
    //         return $this->attributes['status'];
    //     }

    //     return $this->attributes['status'] = trans('days.'.$this->attributes['status']);
    // }


}
