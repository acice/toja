<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    protected $fillable = [
        'account_no', 'routing_no', 'vendor_id','name', 'branch', 'address'
    ];

    public function vendor()
    {
        return $this->belongsTo(Vendor::class);
    }
}
