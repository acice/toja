<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class PricePreference extends Model
{
    protected $table = 'price_preference';

    protected $fillable = [
        'user_id', 'vendor_id', 'price_preference'
    ];

    public function pricePreference() {
        return $this->belongsTo(User::class);
    }


    public function add($data) {
    	return $this->create($data);
    }

    public function removeUserPreferences($id) {
    	return $this->where('user_id', '=', $id)->delete();
    }

     public function removeVendorPreferences($id) {
        return $this->where('vendor_id', '=', $id)->delete();
    }
}
