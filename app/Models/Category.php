<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Nestable\NestableTrait;
use Illuminate\Support\Facades\Auth;
use App\Models\BaseModel;

class Category extends BaseModel
{
    use NestableTrait;
    protected $table = 'categories';
    protected $parent = 'parent_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'slug', 'parent_id'
    ];
 
    
    public function getNameAttribute()
    {
        if (Auth::user()->flag != 'iw') {
            return $this->attributes['name'];
        }

        return $this->attributes['name'] = $this->attributes['iw_name'];
    }

    public function vendors() {
        return $this->belongsToMany(Vendor::class,'category_link');
        
    }

    public function latestVendors() {
        return $this->hasMany(CategoryLink::class, 'category_id','id')->where('vendor_id','<>',0)->latest()->nPerGroup('category_id', 5);
    }
  
}
