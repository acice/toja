<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class EventTicket extends Model
{
    protected $table = 'events_ticket';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'amount', 'price', 'event_id', 'user_id'
    ];


    public function user() {
        return $this->belongsTo(User::class);
    }

    public function event() {
        return $this->belongsTo(Event::class);
    }

}
