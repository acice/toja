<?php

namespace App\Models;

use App\Models\Post;
use App\Models\User;
use App\Models\Vendor;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;


class ActivatedDeals extends Model
{
    protected $table = 'activated_deals';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'vendor_id', 'post_id','confirmed','status','preference','invoice_no','reject_reason'
    ];
       /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'created_at_formated'
    ];
    
    public function count($postId) {
        return $this->where('post_id', $postId)->count();
    }
    
    public function post() {
        return $this->belongsTo(Post::class)->withTrashed();
    }
    
    public function vendor() {
        return $this->belongsTo(Vendor::class);
    }
    
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function payment()
    {
        return $this->hasOne(PaymentDetails::class);
    }

    public function getCreatedAtFormatedAttribute()
    {
        $date=date_create($this->attributes['created_at']);
        return date_format($date,"F j, Y, g:i a");
    }

    public function getCreatedAtAttribute()
    {
        $date=date_create($this->attributes['created_at']);
        return date_format($date,"F j, Y, g:i:s a");
    }



}
