<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Rennokki\Befriended\Contracts\Liking;
use Rennokki\Befriended\Scopes\LikeFilterable;
use Rennokki\Befriended\Traits\Like;
use Carbon\Carbon;

class Comment extends Model implements Liking
{
    use Like, LikeFilterable;

    protected $table = 'comments';
    protected $dates = ['deleted_at'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'user_liked','child_count'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'post_id', 'user_id','parent_id', 'body', 'iw_body',
    ];

    public function getBodyAttribute()
    {
        if (Auth::user()->flag != 'iw') {
            return $this->attributes['body'];
        }

        return $this->attributes['body'] = $this->attributes['iw_body'];
    }

    public function getCreatedAtAttribute()
    {
        $human_time=Carbon::parse($this->attributes['created_at'])->diffForHumans();
        return $this->attributes['created_at'] =$human_time;
    }

    public function getChildCountAttribute()
    {
        $child_count=Comment::where('parent_id',$this->attributes['id'])->count();
        return $child_count;
    }

    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    public function tags()
    {
        return $this->hasManyThrough(Post::class, TaggedPosts::class, 'post_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    protected function getUserLikedAttribute(): bool
    {
       return \Auth::user()->isLiking($this);
    }

}
