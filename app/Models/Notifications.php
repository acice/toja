<?php

namespace App\Models;

use Illuminate\Notifications\DatabaseNotification;
use DateTimeInterface;
use Carbon\Carbon;

class Notifications extends DatabaseNotification
{
    protected $fillable = [
        'id', 'type', 'notifiable_type', 'notifiable_id ', 'data', 'read_at','created_at', 'updated_at'
    ];


    public function users()
    {
        return $this->belongsTo(User::class);
    }


     /**
     * Prepare a date for array / JSON serialization.
     *
     * @param  \DateTimeInterface  $date
     * @return string
     */
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('F j, Y, g:i a');
    } 
}