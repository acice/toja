<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
class Invoice extends Model
{
    protected $fillable = [
      'name', 'iw_name', 'price', 'discount', 'paid', 'vendor_id', 'admincharge', 'amountsaved', 'dealsactivated', 'description', 'iw_description', 'email', 'email_sent'
    ];
  

    public function vendorPaymentDetails() {
        return $this->hasOne(PaymentDetails::class, 'id', 'payment_id');
    }

    public function vendor() {
        return $this->belongsTo(Vendor::class, 'payment_id', 'id');
    }

    public function userPayerDetails() {
        return $this->hasOne(User::class, 'id', 'payer_id');
    }


    /**
     *  Invoice paid.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  mixed  $invoice_id
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeInvoicePaid($query, $invoice_id) {
        $invoice = $query->where('id', $invoice_id)->update([
            'paid' => 1,
        ]);

        if(!$invoice) {
           return response()->json([
                'message' => 'Unable to mark invoice as paid'
                ], 400
            );
        }
        
        return response()->json([
            'message' => 'Invoice paid'
            ], 200
        );
    }
}
