<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\Auth;
use Stichoza\GoogleTranslate\GoogleTranslate;

class TaggedPosts extends Model
{
    protected $table = 'tagged_posts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'post_id', 'user_id', 'user_tagged_id',
    ];


//     private $googleTranslate;

//     public function __construct() {
//         $this->googleTranslate = new GoogleTranslate();
//     }

//     public function getBodyAttribute() {
//         return $this->googleTranslate->setSource('en')->setTarget(Auth::user()->flag)->translate($this->attributes['body']);
//     }

    public function posts()
    {
        return $this->belongsToMany(Post::class, 'tagged_posts', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function tagged()
    {
        return $this->belongsTo(User::class, 'user_tagged_id');
    }

}
