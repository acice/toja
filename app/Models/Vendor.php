<?php

namespace App\Models;

use App\Models\ActivatedDeals;
use Illuminate\Support\Facades\Auth;
use Rennokki\Befriended\Traits\Like;
use Malhal\Geographical\Geographical;
use Illuminate\Database\Eloquent\Model;
use Rennokki\Befriended\Traits\CanBlock;
use Illuminate\Database\Eloquent\Builder;
use Rennokki\Befriended\Contracts\Liking;
use Rennokki\Befriended\Traits\CanFollow;
use Rennokki\Befriended\Contracts\Blocking;
use Rennokki\Befriended\Contracts\Following;
use Rennokki\Befriended\Traits\CanBeBlocked;
use Rennokki\Befriended\Traits\CanBeFollowed;
use Rennokki\Befriended\Scopes\LikeFilterable;
use Rennokki\Befriended\Scopes\BlockFilterable;
use Rennokki\Befriended\Scopes\FollowFilterable;
use Carbon\Carbon;

class Vendor extends Model
{
    use Geographical, CanFollow, CanBeFollowed, CanBlock, CanBeBlocked,
        LikeFilterable, Like, BlockFilterable, FollowFilterable;


    protected static $kilometers = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'vendor_id', 'name', 'description', 'iw_description', 'address', 'postcode', 'company_number', 'address_id', 'longitude', 'latitude', 'code', 'data', 'iw_name','iw_body', 'email', 'body', 'user_id','street','city','opening_days','opening_time','closing_time'
    ];

    // public function getNameAttribute()
    // {
    //     if (Auth::check()) {
    //         if (!Auth::user()->flag || Auth::user()->flag != 'iw') {
    //             return $this->attributes['name'];
    //         }
    //         return $this->attributes['name'] = !empty($this->attributes['iw_name']) ? $this->attributes['iw_name'] : $this->attributes['name'];
    //     }
    // }

    public function getBodyAttribute()
    {
        if (Auth::check()) {
            if (!Auth::user()->flag || Auth::user()->flag != 'iw') {
                return $this->attributes['body'];
            }

            return $this->attributes['body'] = !empty($this->attributes['iw_body']) ? $this->attributes['iw_body'] : $this->attributes['body'];
        }
    }

    public function getPostcodeAttribute()
    {
        if (!Auth::user()->flag || Auth::user()->flag != 'iw') {
            return $this->attributes['postcode'];
        }
        return $this->attributes['postcode'] = $this->attributes['postcode'] ;
    }


    public function activatedDeals() {
        return $this->hasMany(ActivatedDeals::class);
    }

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function bank() {
        return $this->hasOne(Bank::class);
    }

    public function files() {
        return $this->hasMany(File::class);
    }
    
    public function customTime() {
        return $this->hasMany(CustomTime::class);
    }

    public function posts() {
        return $this->hasManyThrough(Post::class,User::class,'id','user_id','user_id','id');
    }

    public function categories() {
        return $this->belongsToMany(Category::class,"category_link","vendor_id","category_id");
    }


       /**
     * Search Opening filter for vendor
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */

    public function scopeIsOpen(Builder $query) {
        $now=Carbon::now()->format('H');
        $currentDay=Carbon::now()->shortEnglishDayOfWeek;
        $query->where(function($q) use ($now) {
            return $q->where('opening_time', '<=',$now);
         })->where(function($q) use ($now) {
            return $q->where('closing_time', '>=', $now);
         })->where(function($q) use ($currentDay) {
            return $q->where('opening_days', 'like','%'.$currentDay.'%');
         });
    }

}
