<?php
namespace App\Models;

use App\Models\BaseModel;
use DB;
use Illuminate\Database\Eloquent\Model;

class CategoryLink extends BaseModel
{

    protected $table = 'category_link';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'category_id', 'user_id', 'vendor_id',
    ];
    

    public function removeUserCategories($id) {
        return $this->where('user_id', '=', $id)->delete();
    }

     public function removeVendorCategories($id) {
        return $this->where('vendor_id', '=', $id)->delete();
    }

    public function vendor() {
        return $this->hasMany(Vendor::class, 'id','vendor_id');
    }
    
    
}
