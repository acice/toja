<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DiscountDay extends Model
{

	protected $table = 'discount_day';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'post_id', 'day'
    ];

}
