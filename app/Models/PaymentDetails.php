<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentDetails extends Model
{
    protected $fillable = [
    	'user_id','post_id', 'payment_id', 'name', 'email','payment_status','amount','vendor_amount','activated_deals_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    public function activated_deals()
    {
        return $this->belongsTo(ActivatedDeals::class);
    }
}
