<?php

namespace App\Models;



use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Auth;

class File extends Model
{
    protected $fillable = [
        'user_id', 'post_id', 'vendor_id', 'file'
    ];

        /**
     * The accessors to append to the model's array form.s
     *
     * @var array
     */
    protected $appends = ['file_url'];

    public function post() {
        return $this->belongsTo(Post::class);
    }

    public function getFileUrlAttribute()
    {
        if($this->attributes['post_id']!=0){
            return Storage::url('post/images/' . $this->file);
        }
        else{
            return Storage::url('vendor/images/' .$this->attributes['user_id']. '/' . $this->file);
        }
       
    }
}
