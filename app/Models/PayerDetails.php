<?php

namespace App\Models;

use App\Helpers\DbConstants;
use Illuminate\Database\Eloquent\Model;

class PayerDetails extends Model
{
    protected $table = DbConstants::TABLE_PAYER_DETAILS;

    protected $fillable = [
        'user_id', 'invoice_id', 'data', 'details'
    ];
}
