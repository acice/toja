<?php

namespace App\Models;

use Carbon\Carbon;
use App\Models\DiscountDay;
use App\Models\ActivatedDeals;
use Illuminate\Support\Facades\Auth;
use Rennokki\Befriended\Traits\Like;
use Illuminate\Database\Eloquent\Model;
use Rennokki\Befriended\Traits\CanBlock;
use Rennokki\Befriended\Contracts\Liking;
use Rennokki\Befriended\Traits\CanFollow;
use Rennokki\Befriended\Contracts\Following;
use Rennokki\Befriended\Traits\CanBeBlocked;
use Illuminate\Database\Eloquent\SoftDeletes;
use Rennokki\Befriended\Traits\CanBeFollowed;
use Rennokki\Befriended\Scopes\LikeFilterable;
use Rennokki\Befriended\Scopes\BlockFilterable;
use Rennokki\Befriended\Scopes\FollowFilterable;

class Post extends Model implements Liking, Following
{

    use SoftDeletes, CanFollow, CanBeFollowed, CanBlock, CanBeBlocked,
    LikeFilterable, Like, BlockFilterable, FollowFilterable;

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'body', 'iw_body','title','iw_title', 'user_id',
        'discount_cost', 'discount_amount', 'discount_end', 'discount_activations','activation_count','availibility','sit_in_term','time_from','time_to' 
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'user_liked',
        'user_activated_deal',
        'user_favourite',
        'like_count',
        'comments_count',
        'no_comission_price',
        'comission_price',
        'discount_price',
        'discounted_price',
        'remaining_time',
        'human_time'
    ];

    protected $hidden = ['address_id','company_number'];

    public function getBodyAttribute()
    {
        if(Auth::user()){
            if (Auth::user()->flag != 'iw') {
                return $this->attributes['body'];
            }
        }
        return $this->attributes['body'] = $this->attributes['iw_body'];
    }

    protected function getLikeCountAttribute()
    {
        return  $this->likers(User::class)->count();
    }

    protected function getCommentsCountAttribute()
    {
        return  $this->comments()->count();
    }

    protected function getNoComissionPriceAttribute()
    {
        $postPrice=$this->attributes['discount_cost'];
        $discountPercentage=$this->attributes['discount_amount'];
        $discountAmount=($postPrice*$discountPercentage)/100;
        $noComissionAmount=$postPrice-$discountAmount;
        return round($noComissionAmount,2);
    }

    protected function getComissionPriceAttribute()
    {
        $postPrice=$this->attributes['discount_cost'];
        $discountPercentage=$this->attributes['discount_amount'];
        $discountAmount=($postPrice*$discountPercentage)/100;
        $commission=($discountAmount*0.2);
        return round($commission,2);
    }

    protected function getDiscountPriceAttribute()
    {
        $postPrice=$this->attributes['discount_cost'];
        $discountPercentage=$this->attributes['discount_amount'];
        $discountAmount=($postPrice*$discountPercentage)/100;
        return round($discountAmount,2);
    }

    protected function getDiscountedPriceAttribute()
    {
        $postPrice=$this->attributes['discount_cost'];
        $discountPercentage=$this->attributes['discount_amount'];
        $discountAmount=($postPrice*$discountPercentage)/100;
        $discountAmount=$discountAmount-($discountAmount*0.2);
        $discountedAmount=$postPrice-$discountAmount;
        return round($discountedAmount,2);
    }

    protected function getRemainingTimeAttribute()
    {
        $remaining_time=Carbon::now()->diffInSeconds($this->attributes['discount_end'], false);
        return $remaining_time;
    }

    protected function getHumanTimeAttribute()
    {
        $human_time=Carbon::createFromTimeString($this->attributes['created_at'])->diffForHumans();
        return $human_time;
    }

    protected function getUserActivatedDealAttribute()
    {
        if(Auth::user()){
            return $this->activatedDeals()->where('status','<>','cancelled')->whereIn('user_id', [Auth::user()->id])->latest()->first() ? true : false;
        }else{
            return false;
        }
        
    }

    protected function getUserLikedAttribute()
    {
        return !empty(Auth::user())?Auth::user()->isLiking($this):false;
    }

    

    public function getUserFavouriteAttribute()
    {
        return $this->favoriteByUser($this);
    }

    public function getTitleAttribute()
    {
        if(Auth::user()){
            if (Auth::user()->flag != 'iw') {
                return $this->attributes['title'];
            }
        }
        return $this->attributes['title'] = $this->attributes['iw_title'];
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function payments()
    {
        return $this->hasMany(PaymentDetails::class,'id','post_id');
    }

    public function files()
    {
        return $this->hasMany(File::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function favourites()
    {
        return $this->hasMany(Favourite::class);
    }

    public function discountdays()
    {
        return $this->hasMany(DiscountDay::class);
    }

    public function tags()
    {
        return $this->hasMany(TaggedPosts::class);
    }

    public function activatedDeals()
    {
        return $this->hasMany(ActivatedDeals::class);
    }

    public function activatedDealsRejected()
    {
        return $this->hasMany(ActivatedDeals::class)->where('status','rejected');
    }

    public function userfollowing()
    {
        return $this->belongsTo(Followers::class, 'user_id', 'followable_id');
    }

    public function scopeFavoriteByUser($query)
    {
        if(Auth::user()){
        return $this->favourites()->whereIn('user_id', [Auth::user()->id])->first() ? true : false;
        }else{
            return false;
        }
    }



    /**
     * Posts that don't belong to a user.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  mixed  $type
     * @return \Illuminate\Database\Eloquent\Builder
     */

     
    public function scopePostsNotByUser($query)
    {
        if(Auth::user()){
            return $query->where('user_id', '!=', Auth::user()->id);
        }
    }


     /**
     * Posts that are available for activation.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  mixed  $type
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActivePosts($query)
    {
        return $query->whereRaw('discount_activations > activation_count');
    }

}
