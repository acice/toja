<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class EventPurchasedTickets extends Model
{

    protected $table = 'event_purchased_tickets';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'event_ticket_id', 'amount', 'price', 'merchant'
    ];

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function eventTicket() {
        return $this->belongsTo(EventTicket::class);
    }

}
