<?php

namespace App\Models;

use Carbon\Carbon;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\Auth;
use Rennokki\Befriended\Traits\Like;
use Illuminate\Support\Facades\Storage;
use Illuminate\Notifications\Notifiable;
use Rennokki\Befriended\Traits\CanBlock;
use Illuminate\Database\Eloquent\Builder;
use Rennokki\Befriended\Contracts\Liking;
use Rennokki\Befriended\Traits\CanFollow;
use Rennokki\Befriended\Contracts\Blocking;
use Rennokki\Befriended\Contracts\Following;
use Rennokki\Befriended\Traits\CanBeBlocked;
use Illuminate\Database\Eloquent\SoftDeletes;
use Rennokki\Befriended\Traits\CanBeFollowed;
use Rennokki\Befriended\Scopes\LikeFilterable;
use Rennokki\Befriended\Scopes\BlockFilterable;
use Rennokki\Befriended\Scopes\FollowFilterable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * User model
 */
class User extends Authenticatable implements
    Following,
    Blocking,
    Liking
{
    use Notifiable,
        HasApiTokens,
        Notifiable,
        SoftDeletes,
        CanFollow,
        CanBeFollowed,
        CanBlock,
        CanBeBlocked,
        LikeFilterable,
        Like,
        BlockFilterable,
        FollowFilterable;

    protected $dates = ['deleted_at'];

    protected static $kilometers = true;

    /**
     * The accessors to append to the model's array form.s
     *
     * @var array
     */
    protected $appends = ['avatar_url','following','no_password','unread_notification'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'active', 'activation_token', 'phone_no','avatar', 'identifier', 'flag','provider','provider_id','access_token','onboarding_status'
    ];

    protected $casts = [
        'isadmin' => 'boolean'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'activation_token',
    ];

    public function getAvatarUrlAttribute()
    {
        if($this->avatar=='avatar.png'){
            return '/avatar.jpg';
        }else{
            return Storage::url('avatars/' . $this->id . '/' . $this->avatar);
        }
        
    }


    /**
     * Scope a query to only include users of a given type.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  mixed  $type
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOfType($query, $type)
    {
        if(Auth::user()){
            return $query->where('users.identifier', $type)->where('users.id', '!=', Auth::user()->id);
        }
        
    }

    /**
     *  Get vendor.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  mixed  $id
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeGetVendor($query, $id)
    {
        return $query->where('id', '=', $id)->where('users.identifier', 1);
    }

    /**
     *  Get User.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  mixed  $id
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeGetUser($query, $id)
    {
        return $query->where('id', '=', $id)->where('identifier', 0);
    }

    public function scopeGetUsersByNameOrEmail($query, $request)
    {
        $name = '%' . $request->name . '%';
        return $query->where('name', 'LIKE', $name)
            ->Orwhere('email', 'LIKE', $name)
            ->where('identifier', 0);
    }

    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public function payments()
    {
        return $this->hasMany(PaymentDetails::class,'id','user_id');
    }

    public function activatedDeals() {
        return $this->hasMany(ActivatedDeals::class);
    }


    public function files()
    {
        return $this->hasMany(File::class);
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'category_link');
    }

    public function vendor()
    {
        return $this->hasOne(Vendor::class);
    }

    public function usersFollowing()
    {
        return $this->belongsTo(Followers::class, 'id', 'followable_id');
    }



    /**
     * Retrieves the login history for the user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\Relation
     */
    public function logins()
    {
        return $this->hasMany(UserLogin::class);
    }

    public function favourites()
    {
        return $this->hasMany(Favourite::class);
    }

    public function isFirstLogin()
    {
        return $this->logins->count() <= 1;
    }

    protected function getFollowingAttribute()
    {
        if (Auth::user()) {
            return \Auth::user()->isfollowing($this) ? \Auth::user()->isfollowing($this) : false;
        }
    }

    public function getnoPasswordAttribute()
    {
        if(Auth::user()){
            if(Auth::user()->password==''){
                return true;
            }else{
                return false; 
            }
        }else{
            return false;
        }
    }

    
    protected function getUnreadNotificationAttribute()
    {
        $count=0;
        if (Auth::user()) {
           $count=Auth::user()->unreadNotifications()->count();
        }
        return $this->attributes['unread_notification'] = $count;
    }
    
    public function updateFlag($flag)
    {
        if (!Auth::user()) {
            return response()->json([
                'message' => 'You need to be the authenticated user to update the translation',
            ], 400);
        }

        $update = Auth::user()->update([
            'flag' => $flag,
        ]);

        if (!$update) {
            return response()->json([
                'message' => 'Something went wrong',
            ], 400);
        }

        $message = $flag == 'iw' ? 'שפה מוגדרת לעברית' : 'Language set to English';

        return response()->json([
            'message' => $message,
            'flag' => $flag,
        ], 200);
    }
}
