<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Malhal\Geographical\Geographical;

class Event extends Model
{
    use Geographical;

    protected static $kilometers = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'body', 'user_id', 'title', 'event_date_time', 'location', 'postcode', 'longitude', 'latitude',
    ];


    public function user() {
        return $this->belongsTo(User::class);
    }

    public function files() {
        return $this->hasMany(File::class);
    }

    public function comments() {
        return $this->hasMany(Comment::class);
    }

    public function tickets() {
        return $this->hasMany(EventTicket::class);
    }

      /**
     * Search between set 1 and set 2 for long and lat
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @param  array $longs lower and upper longitudes
     * @param  array $lats lower and upper latitudes
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeBetweenLngLat(Builder $query, $longs, $lats)
    {
         $query->whereBetween('longitude', $longs)->whereBetween('latitude', $lats);
    }


}
