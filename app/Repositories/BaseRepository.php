<?php

namespace App\Repositories;

/**
 * Class BaseRepository
 * 
 * @package App\Repositories
 */
class BaseRepository extends  \Prettus\Repository\Eloquent\BaseRepository
{
    /**
     * Repository model
     * 
     * @var string
     */
    protected $repositoryModel;

    /**
     * Get the repository model class
     *
     * @return string
     */
    public function model(): string
    {
        return $this->repositoryModel;
    }

    /**
     * @param string $column
     * @param string $value
     * @param array  $columns
     *
     * @return Model|array
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     * @throws ModelNotFoundException
     */
    public function findByColumn(string $column, string $value, array $columns = ['*'])
    {
        $this->applyCriteria();
        $this->applyScope();

        /** @var Builder $model */
        $model = $this->model;
        $data  = $model->where($column, '=', $value)->get($columns);
        if ($data->count() === 0) {
            throw (new ModelNotFoundException)->setModel($this->model(), $column . "." . $value);
        }

        $this->model = $model;

        $this->resetModel();

        return $this->parserResult($data->first());
    }
}
