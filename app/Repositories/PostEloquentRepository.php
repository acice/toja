<?php

namespace App\Repositories;

use App\Models\Post;
use Prettus\Repository\Traits\CacheableRepository;
use Prettus\Repository\Contracts\CacheableInterface;

/**
 * PostRepository
 */
class PostEloquentRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;

    protected $repositoryModel = Post::class;
}
