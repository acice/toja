<?php

namespace App\Repositories;

use App\Models\Vendor;

class VendorsRepository extends BaseRepository
{
  protected $repositoryModel = Vendor::class;
}
