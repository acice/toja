<?php

namespace App\Repositories;

use App\Models\Category;

class CategoriesRepository extends BaseRepository
{
  protected $repositoryModel = Category::class;
}
