<?php

namespace App\Repositories;

use App\Models\User;

/**
 * UsersRepository
 * 
 * @package \App\Repositories
 */
class UsersRepository extends BaseRepository
{
  protected $repositoryModel = User::class;
}
