<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */


Route::group([
    'prefix' => 'auth',
], function () {
    Route::post('login', 'AuthController@login');
    Route::get('login/{provider}', 'AuthController@redirect');
    Route::get('login/{provider}/callback','AuthController@Callback');
    Route::post('signup', 'AuthController@signup');
    Route::get('signup/activate/{token}', 'AuthController@signupActivate');
    Route::post('checkemail', 'AuthController@checkIfEmailExists');
    Route::post('checkproviderid', 'AuthController@checkIfProviderIDExists');
    

    Route::group([
        'middleware' => 'auth:api',
    ], function () {
        Route::get('logout', 'AuthController@logout');
        Route::get('user', '\App\Http\Controllers\User\UserController@getUser');
        Route::post('user/flag/{flag}', '\App\Http\Controllers\User\UserController@updateFlag');
        Route::get('notifications', '\App\Http\Controllers\User\UserController@getNotifications');
        Route::get('notification/read/{id}', '\App\Http\Controllers\User\UserController@markAsReadNotification');
        Route::get('notification/readall', '\App\Http\Controllers\User\UserController@markAllAsReadNotification');
        Route::get('test', '\App\Http\Controllers\User\UserController@getUserSelectedCategoriesTest');
    });
});

Route::group([
    'namespace' => 'Auth',
    'middleware' => 'api',
    'prefix' => 'password',
], function () {
    Route::post('create', 'PasswordResetController@create');
});

Route::group([
    'prefix' => 'social',
], function () {
    Route::group([
        'middleware' => 'auth:api',
    ], function () {
        // Follow
        Route::post('follow/{id}', '\App\Http\Controllers\Social\FollowController@followUser');
        Route::post('unfollow/{id}', '\App\Http\Controllers\Social\UnFollowController@unFollowUser');
        Route::post('following', '\App\Http\Controllers\Social\FollowController@following');
        Route::post('not/following', '\App\Http\Controllers\Social\FollowController@notFollowing');
        Route::get('following/count', '\App\Http\Controllers\Social\FollowController@followingCount');
        //Followers
        Route::get('followers/count', '\App\Http\Controllers\Social\FollowController@followersCount');
    });
});



Route::group([
    'prefix' => 'post',
], function () {
    Route::group([
        'middleware' => 'auth:api',
    ], function () {
        // Post
        Route::post('create', '\App\Http\Controllers\Posts\PostController@createPost');
        Route::post('update/{id}', '\App\Http\Controllers\Posts\PostController@updatePost');

        Route::post('update/discountday/{id}', '\App\Http\Controllers\Posts\PostController@updatePost');

        Route::post('delete/{id}', '\App\Http\Controllers\Posts\PostController@deletePost');
        Route::post('image/delete/{id}', '\App\Http\Controllers\Posts\PostController@removePostImage');
        Route::post('image/add/{id}', '\App\Http\Controllers\Posts\PostController@addPostImage');
        Route::get('posts', '\App\Http\Controllers\Posts\PostController@listPosts');
        Route::get('category/{id}', '\App\Http\Controllers\Posts\PostController@postByCategoriesId');
        Route::get('activated/posts', '\App\Http\Controllers\Posts\PostController@getAllActivePosts');
        
        Route::get('activated/deals/user/{userId}', '\App\Http\Controllers\Posts\PostController@getAllActivatedDealsForUser');
        Route::get('activated/deals/vendor/{vendorId}', '\App\Http\Controllers\Posts\PostController@getAllActivatedDealsForVendor');

        Route::post('confirm/activated/deal/{id}', '\App\Http\Controllers\ActivatedDeals\ActivatedDealController@confirmActivatedDeal');
        Route::post('activated/deals/search', '\App\Http\Controllers\ActivatedDeals\ActivatedDealController@search');
        Route::get('activated/deals', '\App\Http\Controllers\ActivatedDeals\ActivatedDealController@allActivatedDeal');
        Route::get('activated/deal/{id}', '\App\Http\Controllers\ActivatedDeals\ActivatedDealController@getActivatedDealsByID');
        Route::post('activate/deal', '\App\Http\Controllers\ActivatedDeals\ActivatedDealController@activateDeal');
        Route::post('cancel/deal/{id}', '\App\Http\Controllers\ActivatedDeals\ActivatedDealController@cancelDeal');

        Route::get('by/vendor', '\App\Http\Controllers\Posts\PostController@getPostsByVendor');
        Route::get('archive/vendor', '\App\Http\Controllers\Posts\PostController@getArchivesByVendor');
        Route::get('by/vendor/{id}', '\App\Http\Controllers\Posts\PostController@getPostsByVendorId');
        Route::get('/{id}', '\App\Http\Controllers\Posts\PostController@getPostById');

        // Like
        Route::post('like/{id}', '\App\Http\Controllers\Posts\LikeController@likePost');
        // UnLike
        Route::post('unlike/{id}', '\App\Http\Controllers\Posts\LikeController@unLikePost');
        // Liked Posts
        Route::get('liked/posts', '\App\Http\Controllers\Posts\LikeController@showUserLikedPosts');
        // Like Count for Post
        Route::get('like/count/{id}', '\App\Http\Controllers\Posts\LikeController@postLikeCount');

        // Comment
        Route::post('comment/{id}', '\App\Http\Controllers\Comment\CommentController@addCommentToPost');
        // Update Comment
        Route::post('comment/update/{id}', '\App\Http\Controllers\Comment\CommentController@updateComment');
        // Delete Comment
        Route::post('comment/delete/{id}', '\App\Http\Controllers\Comment\CommentController@deleteComment');

        // Comment by post ID
        Route::get('comment/{id}', '\App\Http\Controllers\Comment\CommentController@getCommentByPost');

        // Comment by parent ID
        Route::get('comment/parent/{id}', '\App\Http\Controllers\Comment\CommentController@getCommentByParent');

        // Like Comment
        Route::post('like/comment/{id}', '\App\Http\Controllers\Posts\LikeController@likeComment');
        // Unlike Comment
        Route::post('unlike/comment/{id}', '\App\Http\Controllers\Posts\LikeController@unLikeComment');
        // Like Count for Comment
        Route::get('like/comment/count/{id}', '\App\Http\Controllers\Posts\LikeController@commentLikeCount');
    });
});

Route::group([
    'prefix' => 'tag',
], function () {
    Route::group([
        'middleware' => 'auth:api',
    ], function () {
        // Tag user.
        Route::post('user', '\App\Http\Controllers\Posts\TaggedPostController@tag');

        // Tagged posts
        Route::get('posts', '\App\Http\Controllers\Posts\TaggedPostController@showUserTaggedPosts');
        Route::post('delete', '\App\Http\Controllers\Posts\TaggedPostController@deleteTag');
    });
});

Route::group([
    'prefix' => 'untag',
], function () {
    Route::group([
        'middleware' => 'auth:api',
    ], function () {
        // Untag user.
        Route::get('user/{id}', '\App\Http\Controllers\Posts\TaggedPostController@untag');

    });
});

Route::group([
    'prefix' => 'event',
], function () {
    Route::group([
        'middleware' => 'auth:api',
    ], function () {
        // Post
        Route::post('create', '\App\Http\Controllers\Events\EventController@createEvent');
        Route::post('update/{id}', '\App\Http\Controllers\Events\EventController@updateEvent');
        Route::post('delete/{id}', '\App\Http\Controllers\Events\EventController@deleteEvent');
        Route::post('image/delete/{id}', '\App\Http\Controllers\Events\EventController@removeEventImage');
        Route::post('image/add/{id}', '\App\Http\Controllers\Events\EventController@addEventImage');
        Route::post('events', '\App\Http\Controllers\Events\EventController@listAllEvents');
        // Route::post('created/by/vendor', '\App\Http\Controllers\Events\EventController@getVendorEvents');

        Route::get('/{id}', '\App\Http\Controllers\Events\EventController@getEventById');

        Route::group([
            'prefix' => 'ticket',
        ], function () {
            Route::post('create', '\App\Http\Controllers\Events\EventTicketController@createEventTickets');
            Route::post('update/{id}', '\App\Http\Controllers\Events\EventTicketController@updateEventTicket');
            Route::get('available', '\App\Http\Controllers\Events\EventTicketController@getEventTickets');

            Route::group([
                'prefix' => 'purchase',
            ], function () {
                Route::post('/', '\App\Http\Controllers\Events\EventPurchasedTicketController@createEventPurchasedTickets');
                Route::post('update/{id}', '\App\Http\Controllers\Events\EventPurchasedTicketController@updateEventPurchasedTickets');
                Route::get('/', '\App\Http\Controllers\Events\EventPurchasedTicketController@getPurchasedTickets');
            });
        });
    });
});

Route::group([
    'prefix' => 'vendor',
], function () {
    Route::group([
        'middleware' => 'auth:api',
    ], function () {
        Route::get('/vendors', '\App\Http\Controllers\Vendor\VendorController@getVendors');
        Route::get('/', '\App\Http\Controllers\Vendor\VendorController@getLoggedInVendorDetails');
        Route::get('/images', '\App\Http\Controllers\Vendor\VendorController@getImages');
        Route::get('/images/{id}', '\App\Http\Controllers\Vendor\VendorController@getImagesByVendorID');
        Route::get('/{id}', '\App\Http\Controllers\Vendor\VendorController@getVendor');
        Route::post('/search', '\App\Http\Controllers\Vendor\VendorController@getVendorByName');
        Route::post('/add/image', '\App\Http\Controllers\Vendor\VendorController@addVendorImages');
        Route::post('/update/details', '\App\Http\Controllers\Vendor\VendorController@updateVendorDetails');
        Route::post('/update/avatar', '\App\Http\Controllers\Vendor\VendorController@updateVendorAvatar');
        Route::post('image/delete/{id}', '\App\Http\Controllers\Vendor\VendorController@removeVendorImage');
    });
});

Route::group([
    'prefix' => 'categories',
], function () {
    Route::group([
        'middleware' => 'auth:api',
    ], function () {
        Route::get('/', '\App\Http\Controllers\Category\CategoryController@categories');
        Route::get('/vendors', '\App\Http\Controllers\Category\CategoryController@categoriesWithVendors');
        Route::get('/{id}', '\App\Http\Controllers\Category\CategoryController@categoriesById');
        Route::post('/add', '\App\Http\Controllers\Category\CategoryController@addCategories');
        Route::post('/update/{id}', '\App\Http\Controllers\Category\CategoryController@updateCategories');
    });
});

Route::group([
    'prefix' => 'preference',
], function () {
    Route::group([
        'middleware' => 'auth:api',
    ], function () {
        Route::post('update/price', '\App\Http\Controllers\Preference\PreferenceController@updatePrice');
        Route::post('update/distance', '\App\Http\Controllers\Preference\PreferenceController@updateDistance');
        Route::post('update/personal','\App\Http\Controllers\Preference\PreferenceController@updatePersonalInfo');
        Route::post('update/bank','\App\Http\Controllers\Preference\PreferenceController@updateBankInfo');
        Route::post('update/categories','\App\Http\Controllers\Preference\PreferenceController@updateOrAddCategories');
        Route::post('update/location','\App\Http\Controllers\Preference\PreferenceController@updateLocation');
        Route::post('update/operatingtime', '\App\Http\Controllers\Preference\PreferenceController@updateOperatingTime');
        Route::post('update/customtime', '\App\Http\Controllers\Preference\PreferenceController@updateCustomTime');
        Route::post('change-password', '\App\Http\Controllers\Preference\PreferenceController@changePassword');
        Route::post('update/avatar', '\App\Http\Controllers\Preference\PreferenceController@updateAvatar');
        Route::post('status/onboarding', '\App\Http\Controllers\Preference\PreferenceController@updateOnboardingStatus');
        Route::get('customtime', '\App\Http\Controllers\Preference\PreferenceController@getCustomTime');
        
    });
});

Route::group([
    'prefix' => 'setting',
], function () {
    Route::group([
        'middleware' => 'auth:api',
    ], function () {
        Route::get('/', '\App\Http\Controllers\Preference\PreferenceController@setting');
        Route::get('/banks', '\App\Http\Controllers\Preference\PreferenceController@bankList');
    });
});

Route::group([
    'prefix' => 'search',
], function () {
    Route::group([
        'middleware' => 'auth:api',
    ], function () {
        Route::post('/vendor', '\App\Http\Controllers\Search\SearchController@searchVendor');
        Route::post('/post', '\App\Http\Controllers\Search\SearchController@searchPost');
    });
});

Route::group([
    'prefix' => 'resource',
], function () {
    Route::group([
        'middleware' => 'auth:api',
    ], function () {
        Route::get('/banks', '\App\Http\Controllers\StaticResources\StaticResourcesController@bankList');
        Route::get('/bank/{id}', '\App\Http\Controllers\StaticResources\StaticResourcesController@bankByID');
    });
});

Route::group([
    'prefix' => '',
], function () {
    Route::group([
        'middleware' => 'auth:api',
    ], function () {
        Route::resource('favourites', '\App\Http\Controllers\Favourite\FavouriteController');
    });
});

Route::group([
    'prefix' => 'search',
], function () {
    Route::group([
        'middleware' => 'auth:api',
    ], function () {
        Route::post('user', '\App\Http\Controllers\User\UserController@getUsersByNameOrEmail');
    });
});

Route::group([
    'prefix' => 'payment',
], function () {
    Route::group(['middleware' => ['web']], function () {
        Route::get('/paypal/connect', '\App\Http\Controllers\Payment\PayPalController@PayPalConnect');
        Route::get('/paypal/connected', '\App\Http\Controllers\Payment\PayPalController@PayPalConnected');
        Route::get('/paypal/{id}', '\App\Http\Controllers\Payment\PayPalController@getPayPalView');
        Route::get('/success', '\App\Http\Controllers\Payment\PayPalController@PaymentSuccess');
        Route::get('/cancel', '\App\Http\Controllers\Payment\PayPalController@PaymentCancel');
        Route::post('/ipn/notify', '\App\Http\Controllers\Payment\PayPalController@postNotify');
        //PayOut
        Route::get('/payout/{id}', '\App\Http\Controllers\Payment\PayPalController@getPayOutView');
        Route::post('/create/order', '\App\Http\Controllers\Payment\PayPalController@createOrder');
        Route::post('/capture/{orderId}', '\App\Http\Controllers\Payment\PayPalController@capturePayment');

    });

    Route::group([
        'middleware' => 'auth:api',
    ], function () {
        Route::group([
            'prefix' => 'paypal',
        ], function () {

        });
    });
});

Route::group([
    'prefix' => 'payment',
], function () {
    Route::group([
   'middleware' => 'auth:api',
    ], function () {
        Route::get('/create/{postID}', '\App\Http\Controllers\Payment\PaymentController@create')->name('getpayment');
        Route::post('/create/{postID}', '\App\Http\Controllers\Payment\PaymentController@getPayment')->name('createpayment');
        Route::post('/update/{paymentID}', '\App\Http\Controllers\Payment\PaymentController@update')->name('updatepayment');
        Route::get('/refund/{paymentID}', '\App\Http\Controllers\Payment\PaymentController@paymentRefund')->name('paymentrefund');
        Route::get('/payments', '\App\Http\Controllers\Payment\PaymentController@all')->name('allpayments');
        Route::get('/payments/{vendorID}', '\App\Http\Controllers\Payment\PaymentController@getPaymentByVendor')->name('vendorpayments');
        Route::get('/payout', '\App\Http\Controllers\Payment\PaymentController@payout')->name('payout');
    }); 
});


Route::group([
    'prefix' => 'invoice',
], function () {
    Route::group([
        'middleware' => 'auth:api',
    ], function () {
        Route::get('/{id}', '\App\Http\Controllers\Payment\InvoiceController@getInvoiceById');
        Route::post('/create', '\App\Http\Controllers\Payment\InvoiceController@store');
        Route::get('/user/invoices', '\App\Http\Controllers\Payment\InvoiceController@getUserInvoices');
        Route::get('/vendor/invoices', '\App\Http\Controllers\Payment\InvoiceController@getVendorInvoices');
    });
});

Route::group([
    'prefix' => 'helper',
], function () {
    Route::group([
        'middleware' => 'auth:api',
    ], function () {
        Route::get('/default/customtime', '\App\Http\Controllers\AuthController@insertDateIntoOld');
        Route::get('/update/customtime', '\App\Http\Controllers\AuthController@updateDateIntoOld');
    });
});

