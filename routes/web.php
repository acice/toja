<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return view('welcome');
});

Route::get('/storage/{image}', [
    'uses' => 'ImageServeController@getServeImage',
])->where('image', '.*');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/discounts', '\App\Http\Controllers\Posts\PostController@index')->name('discounts')->middleware('auth');
Route::get('all/posts', '\App\Http\Controllers\Posts\PostController@getAllPosts')->name('posts')->middleware('auth');
Route::get('all/active/posts', '\App\Http\Controllers\Posts\PostController@getAllActivePosts')->name('posts')->middleware('auth');

Route::get('/invoices', '\App\Http\Controllers\InvoiceController@index')->name('invoices')->middleware('auth');
Route::get('all/invoices', '\App\Http\Controllers\InvoiceController@getAllInvoices')->name('invoices')->middleware('auth');
Route::post('/invoice/paid/{id}', '\App\Http\Controllers\InvoiceController@updateInvoicePaid');

Route::group([
    'namespace' => 'Auth',
    'middleware' => 'web',
    'prefix' => 'password',
], function () {
    Route::post('update', 'PasswordResetController@reset');
    Route::get('find/{token}', 'PasswordResetController@find');
});

Route::get('change-password', 'ChangePasswordController@index')->name('changepassword');
Route::post('change-password', 'ChangePasswordController@store')->name('change.password');

Route::get('change-password-user/{id}', 'ChangePasswordControllerUser@index')->name('changepassword.user');
Route::post('change-password-user/{id}', 'ChangePasswordControllerUser@store')->name('change.password.user');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/notifyTest', '\App\Http\Controllers\User\UserController@notificationTest')->name('testNot');



Route::get('/terms-conditions-hebrew', function () {
    return view('termHebrew');
});

Route::get('/terms-conditions-english', function () {
    return view('termEnglish');
});

Route::get('/privacy-policy-hebrew', function () {
    return view('policyHebrew');
});

Route::get('/privacy-policy-english', function () {
    return view('policyEnglish');
});
Route::get('/payment-test', function () {
    return view('paymentTest');
});
Route::group([
    'middleware' => 'web',
    'prefix' => 'payment',
], function () {
    Route::match(array('GET', 'POST'), '\App\Http\Controllers\Payment\PaymentController@webhook')->name('webhook');
    Route::match(array('GET', 'POST'),'bp/success', '\App\Http\Controllers\Payment\PaymentController@success');
    Route::match(array('GET', 'POST'),'bp/cancel', '\App\Http\Controllers\Payment\PaymentController@cancel');
    Route::match(array('GET', 'POST'),'bp/failed', '\App\Http\Controllers\Payment\PaymentController@failed');
});
