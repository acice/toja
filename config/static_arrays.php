<?php

class StaticArray {

    public static $iw_banks = [
        "b01"=>'בנק לאומי לישראל בע“מ',
        "b02"=>'בנק הפועלים בע“מ',
        "b03"=>'בנק דיסקונט לישראל בע“מ',
        "b04"=>'בנק יהב לעובדי המדינה בע“מ',
        "b05"=>'בנק מזרחי טפחות בע“מ',
        "b06"=>'בנק אגוד לישראל בע“מ',
        "b07"=>'בנק אוצר החייל בע“מ',
        "b08"=>'בנק מרכנתיל דיסקונט בע“מ',
        "b09"=>'וואן זירו הבנק הדיגיטלי בע“מ',
        "b10"=>'בנק ירושלים בע“מ',
        "b11"=>'HSBC בנק',
        "b12"=>'יובנק בע“מ',
        "b13"=>'בנק הבינלאומי הראשון לישראל בע“מ',
        "b14"=>'בנק מסד בע“מ',
        "b15"=>'בנק פועלי אגודת ישראל בע“מ',
        "b16"=>'בנק ירושלים בע“מ',
    ];

    public static $banks = [
        "b01" => 'Bank Leumi LeIsrael Ltd.',
        "b02" => 'Bank Hapoalim Ltd.',
        "b03" => 'Israel Discount Bank Ltd.',
        "b04" => 'Bank Yahav for Civil Servants Ltd.',
        "b05" => 'Mizrahi Tefahot Bank Ltd.',
        "b06" => 'Union Bank of Israel Ltd.',
        "b07" => 'Otzar Hachayal Bank Ltd.',
        "b08" => 'Mercantile Discount Bank Ltd.',
        "b09" => 'One Zero Digital Bank Ltd.',
        "b10" => 'Bank of Jerusalem Ltd.',
        "b11" => "HSBC Bank",
        "b12" => 'UBank Ltd.',
        "b13" => 'The First International Bank of Israel Ltd.',
        "b14" => 'Masad Bank Ltd.',
        "b15" => 'Bank Poalei Agudat Israel Ltd.',
        "b16" => 'Bank of Jerusalem Ltd.',
    ];

}

?>