server '167.71.212.214',
user: 'root',
roles: %w{web app},
port:22

# Directory to deploy
# ===================
set :env, 'production'
set :app_debug, 'false'
set :deploy_to, '/home/toja/web'
set :shared_path, '/home/toja/web/shared'
set :overlay_path, '/home/toja/web/overlay'
set :tmp_dir, '/home/toja/web/tmp'
set :site_url, 'http://toja.app'
set :supervisor_task, ''
set :php_fpm, 'php7.2-fpm'