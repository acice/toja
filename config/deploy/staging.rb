server '161.35.235.240',
user: 'toja_stage',
roles: %w{web app},
port:22

# Directory to deploy
# ===================
set :env, 'staging'
set :app_debug, 'false'
set :deploy_to, '/home/toja_stage/web'
set :shared_path, '/home/toja_stage/web/shared'
set :overlay_path, '/home/toja_stage/web/overlay'
set :tmp_dir, '/home/toja_stage/web/tmp'
set :site_url, 'http://stage.toja.app'
set :supervisor_task, ''
set :php_fpm, 'php7.2-fpm'
