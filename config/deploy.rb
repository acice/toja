# Application #
#####################################################################################
set :application,     'toja-app'
set :branch,          ENV["branch"] || "main"
set :user,            ENV["user"] || ENV["USER"] || "toja_stage"

# SCM #
#####################################################################################
set :repo_url,        :"git@github.com:coding-mountain/toja-admin.git"
set :repo_base_url,   :"https://github.com"
set :repo_diff_path,  :"coding-mountain/toja-admin/compare/main..."
set :repo_branch_path,:'coding-mountain/toja-admin/commits'
set :repo_commit_path,:'coding-mountain/toja-admin/commit'


# Multistage Deployment #
#####################################################################################
set :stages,              %w(dev staging production)
set :default_stage,       "staging"


# Other Options #
#####################################################################################
set :ssh_options,         { :forward_agent => false }
set :default_run_options, { :pty => true }


# Permissions #
#####################################################################################
set :use_sudo,            false
set :permission_method,   :acl
set :use_set_permissions, true
set :webserver_user,      "www-data"
set :group,               "www-data"
set :keep_releases,       2

# Set current time #
#######################################################################################
require 'date'
set :current_time, DateTime.now
set :current_timestamp, DateTime.now.to_time.to_i

# Application Tasks #
#######################################################################################
namespace :environment do
    desc "Set environment variables"
    task :set_variables do
        on roles(:app) do
              puts ("--> Copying environment configuration file")
              execute "cp #{release_path}/.env.server #{release_path}/.env"
              puts ("--> Setting environment variables")
              execute "sed --in-place -f #{fetch(:overlay_path)}/parameters.sed #{release_path}/.env"
        end
    end
end

namespace :composer do
    desc "Running Composer Install"
    task :install do
        on roles(:app) do
            within release_path do
                execute :php74 , "/usr/local/bin/composer install --no-dev --quiet"
                execute :php74 , "/usr/local/bin/composer dump-autoload -o"
            end
        end
    end

    desc "Running Composer Install with dev"
    task :install_with_dev do
        on roles(:app) do
            within release_path do
                execute :php74 , "/usr/local/bin/composer install"
                execute :php74 , "/usr/local/bin/composer dump-autoload -o"
            end
        end
    end
end

namespace :app do
    desc "Create shared folders"
    task :create_storage_folder do
        on roles(:all) do
            execute "mkdir -p #{shared_path}/storage"
            execute "mkdir -p #{shared_path}/storage/app"
            execute "mkdir -p #{shared_path}/storage/framework"
            execute "mkdir -p #{shared_path}/storage/framework/cache"
            execute "mkdir -p #{shared_path}/storage/framework/sessions"
            execute "mkdir -p #{shared_path}/storage/framework/views"
            execute "mkdir -p #{shared_path}/storage/logs"
            execute "mkdir -p #{shared_path}/uploads"
        end
    end

    desc "Symbolic link for shared folders"
    task :create_symlink do
        on roles(:app) do
            within release_path do
                execute "rm -rf #{release_path}/storage"
                execute "ln -s #{shared_path}/storage/ #{release_path}"
                execute "rm -rf #{release_path}/public/uploads"
                execute "ln -s #{shared_path}/uploads #{release_path}/public"
            end
        end
    end

    task :create_overlay_folder do
        on roles(:all) do
            execute "mkdir -p #{deploy_to}/overlay"
        end
    end

    task :create_uploads_folder do
        on roles(:all) do
            execute "mkdir #{shared_path}/uploads"
        end
    end

    desc "Run Laravel Artisan migrate task."
    task :migrate do
        on roles(:app) do
            within release_path do
                execute :php74, "artisan migrate --force"
            end
        end
    end

    desc "Run Laravel Artisan command task."
	task :command do
		on roles(:app) do
			within release_path do
				execute :php74, "artisan #{fetch(:command)}"
			end
		end
	end

    desc "Run Laravel Artisan seed task."
    task :seed do
        on roles(:app) do
            within release_path do
            execute :php74, "artisan db:seed --force"
            end
        end
    end

    desc "Run Laravel Artisan fresh seed task."
    task :fresh_seed do
        on roles(:app) do
            within release_path do
            execute :php74, "artisan migrate:fresh --seed"
            end
        end
    end

    desc "Run Laravel Artisan clear view task."
	task :clear_views do
		on roles(:app) do
			within release_path do
			execute :php74, "artisan view:clear"
			end
		end
	end

    desc "Optimize Laravel Class Loader"
    task :optimize do
        on roles(:app) do
            within release_path do
                execute :php74, "artisan clear-compiled"
                execute :php74, "artisan optimize"
            end
        end
    end

    desc "Create ver.txt"
    task :create_ver_txt do
        on roles(:all) do
            puts ("--> Copying ver.txt file")
            execute "cp #{release_path}/config/deploy/ver.txt.example #{release_path}/public/ver.txt"
            execute "sed --in-place 's/%date%/#{fetch(:current_time)}/g
                        s/%branch%/#{fetch(:branch)}/g
                        s/%revision%/#{fetch(:current_revision)}/g
                        s/%deployed_by%/#{fetch(:user)}/g' #{release_path}/public/ver.txt"
            execute "find #{release_path}/public -type f -name 'ver.txt' -exec chmod 664 {} \\;"
        end
    end

    desc "Set up project"
    task :set_up do
        on roles(:all) do
            invoke "app:create_storage_folder"
        end
    end
end


# DevOps Tasks #
#######################################################################################
namespace :devops do
    desc "Setup Application Directories"
    task :set_up do
        on roles(:all) do
            invoke "app:set_up"
        end
    end

    desc "Copy Parameter File(s)"
    task :copy do
        on roles(:all) do |host|
            %w[ parameters.sed ].each do |f|
            upload! "./config/deploy/parameters/#{fetch(:env)}/" + f , "#{fetch(:overlay_path)}/" + f
            end
        end
    end
end

namespace :vendor do
    desc 'Copy vendor directory from last release'
    task :copy do
        on roles(:web) do
            puts ("--> Copy vendor folder from previous release")
            execute "vendorDir=#{current_path}/vendor; if [ -d $vendorDir ] || [ -h $vendorDir ]; then cp -a $vendorDir #{release_path}/vendor; fi;"
        end
    end
end

# Server Tasks #
#######################################################################################
namespace :nginx do
    desc 'Reload nginx server'
        task :reload do
            on roles(:all) do
            execute :sudo, :service, "nginx reload"
        end
    end
end

namespace :php_fpm do
    desc 'Reload php-fpm'
        task :reload do
            on roles(:all) do
            execute :sudo, :service, "#{fetch(:php_fpm)} reload"
        end
    end
end

# Laravel Queue Tasks #
#######################################################################################
namespace :laravel_queue do
    desc 'Restart Supervisor Laravel Queue Task'
        task :restart do
            on roles(:all) do
            execute :sudo, :supervisorctl, "restart #{fetch(:supervisor_task)}"
        end
    end
    desc 'Strat Supervisor Laravel Queue Task'
        task :start do
            on roles(:all) do
            execute :sudo, :supervisorctl, "start #{fetch(:supervisor_task)}"
        end
    end
    desc 'Stop Supervisor Laravel Queue Task'
        task :stop do
            on roles(:all) do
            execute :sudo, :supervisorctl, "stop #{fetch(:supervisor_task)}"
        end
    end
end

#######################################################################################

desc "Setup Initialize"
task :setup do
    invoke "devops:set_up"
    invoke "app:create_overlay_folder"
    invoke "devops:copy"
end

desc "Update Environment File"
task :update_env do
    invoke "devops:copy"
    invoke "environment:set_variables"
end

desc "Database Seeder"
task :db_seed do
    invoke "app:fresh_seed"
end

namespace :deploy do
    after :updated, "vendor:copy"
    after :updated, "composer:install"
    after :updated, "environment:set_variables"
    after :published, "app:create_symlink"
    after :published, "app:clear_views"
    after :finished, "app:create_ver_txt"
end

after "deploy", "php_fpm:reload"
