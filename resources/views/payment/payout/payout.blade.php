<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
</head>
<script src="https://www.paypal.com/sdk/js?client-id=AZVab7_8l9xiyMr9dgRMWgx6ICof5rMqas64CGwSq5zho09LwQGojZ-bnmVvSwSV9rg8Sewsrch_NC7c&intent=capture&currency=ILS"></script> <!-- authorize -->


<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
    integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
    crossorigin="anonymous"></script>

<script>
  paypal.Buttons({
    style: {
        label:  'pay',
        height: 40,
    },
    createOrder: function() {
      return fetch('https://toja.app/api/payment/create/order', {
          method: 'post',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          }
        })
        .then(res=>res.json())
        .then(data => {
          // console.log('data', data)
          return data.id;
      });
    },
    onApprove: function(data, actions) {
      $('.loading').show();


       return fetch('https://toja.app/api/payment/capture/'+data.orderID+'', {
          method: 'post',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          }
        })
        .then(res=>res.json())
        .then(data => {
          $('.loading').hide();
          window.location.href = 'https://toja.app/api/payment/success';
      });
    }
  }).render('#paypal-button-container');
</script>

<style type="text/css">


body {
  background-color: #2e476b;
  width: 100%;
  height: 100%;
}

.center-div {
  text-align: center;
  position: absolute;
  margin: auto;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  width: 400px;
  height: 300px;
}

.loading {
  display: none;
}
.loading img {
    height: 120px;
    position: absolute;
    bottom: 9px;
    right: 0;
    top: 268px;
    left: 0;
    margin-left: auto;
    margin-right: auto;
}

</style>

<body>
<div class="content">
  <div class="center-div">
    <img class="anim2" src="https://toja.app/storage/tojalogo.png" alt="logo" width="300">
    <div id="paypal-button-container"></div>
    <div class="loading"><img src="{{asset('./loading.gif')}}" alt=""></div>
  </div>
</div>
</body>
