<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
</head>

<body>

<span id='cwppButton'></span>

<script src='https://www.paypalobjects.com/js/external/connect/api.js'></script>
<script>
paypal.use( ['login'], function (login) {
  login.render ({
    "appid":"AZVab7_8l9xiyMr9dgRMWgx6ICof5rMqas64CGwSq5zho09LwQGojZ-bnmVvSwSV9rg8Sewsrch_NC7c",
    "authend":"sandbox",
    "scopes":"openid profile email https://uri.paypal.com/services/paypalattributes",
    "containerid":"cwppButton",
    "responseType":"code",
    "locale":"en-us",
    "buttonType":"CWP",
    "buttonShape":"pill",
    "buttonSize":"lg",
    "fullPage":"true",
    "returnurl":"https://toja.app/api/payment/paypal/connected"
  });
});
</script>
</body>