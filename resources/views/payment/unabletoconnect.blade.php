<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
</head>

<style type="text/css">
 

body {
  background-color: #2e476b;
  width: 100%;
  height: 100%;
  font-family: Helvetica, Sans-Serif;
  color: #FFF;
}

.center-div {
  text-align: center;
  position: absolute;
  margin: auto;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  width: 400px;
  height: 300px;
}


</style>

<body>
<div class="content">
  <div class="center-div">
    <img class="anim2" src="https://toja.app/storage/tojalogo.png" alt="logo" width="300">
    <h1>Unable to connect account</h1>
    <p>Please try again or contact TOJA</p>
  </div>
</div>
</body>