<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
</head>

<body>
  <script
    src="https://www.paypal.com/sdk/js?client-id=AZVab7_8l9xiyMr9dgRMWgx6ICof5rMqas64CGwSq5zho09LwQGojZ-bnmVvSwSV9rg8Sewsrch_NC7c&currency=ILS&merchant-id={!! $invoice->vendorPaymentDetails->merchant_id !!}">
  </script>

  <div id="paypal-button-container"></div>

  <script>
   paypal.Buttons({
    createOrder: function(data, actions) {
      return actions.order.create({
        purchase_units: [{
          amount: {
            value: {!! $invoice->price !!}
          }
        }]
      });
    },
    onApprove: function(data, actions) {
      return actions.order.capture().then(function(details) {
        console.log('Transaction completed by ' + details.payer.name.given_name);
        console.log('Data', data);
        console.log('Details', details);
        // Call your server to save the transaction
        return fetch('/api/payment/paypal-transaction-complete', {
          method: 'post',
          headers: {
            'content-type': 'application/json'
          },
          body: JSON.stringify({
            orderID: data.orderID,
            payerID: data.payerID,
            data: data,
            invoice_id: {!! $invoice->id !!}, 
            details: details,
          })
        });
      });
    }
  }).render('#paypal-button-container');
  </script>
</body>
</body>