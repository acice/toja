<html>

<body style="font-family: Open Sans, sans-serif;font-size:100%;font-weight:400;line-height:1.4;color:#000;">
  <table style="max-width:700px;margin:50px auto 10px;background-color:#fff;-webkit-border-radius:3px;-moz-border-radius:3px;border-radius:3px;-webkit-box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24);-moz-box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24);box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24);line-height: 1.6;">
    <thead >
      <tr >
        <th colspan="2" style="padding: 35px 15px;text-align: center;color: #fff;background: #2e476b;">TOJA</th>
      </tr>
    </thead>
    <tbody style="padding:50px;">
      <tr>
        <td colspan="2"  style="height:20px;"></td>
        
      </tr>
      <tr>
        <td  style="padding:10px 20px;">
          <p style="font-size:14px;margin:0 0 0 0;">
            <span style="font-weight:bold;display:inline-block;min-width:146px">
            @lang('mails.invoice') </span>
          <br><br>
          {{$invoice->customer}}
          </p>
        </td>
        <td  style="padding:10px 20px;vertical-align: top;">
          <p style="font-size:14px;margin:0 0 0 0;">
            {{$invoice->invoice_no}} <br><br>
          </p>
          
        </td>
      </tr>
      <tr>
        <td style="height:20px;"></td>
      </tr>
      <tr>
        <td  style="padding:15px;">
          <p style="font-size:14px;margin:0;">
            <span style="font-weight:bold;display:inline-block;">@lang('mails.amount_paid')</span><br><br>
            <span style="font-weight:bold;display:inline-block;">@lang('mails.outstanding_balance')</span>
          </p>
        </td>
        <td style="padding:15px;">
          <p style="font-size:14px;margin:0;">
            @lang('mails.currency') {{ $invoice->comission_price }}<br><br>
            @lang('mails.currency') {{ $invoice->no_comission_price }}
          </p>
        </td>
      </tr>
      <tr>
        <td colspan="2" style="height:20px;border-bottom:solid 1px #ddd"></td>
      </tr>
      <tr>
        <td style="padding:15px;">
          <p style="font-size:14px;margin:0;padding:10px">
            <span style="font-weight:bold;display:inline-block;">@lang('mails.total')</span>
          </p>
        </td>
        <td style="padding:15px;">
          <p style="font-size:14px;margin:0;padding:10px">
            @lang('mails.currency') {{ $invoice->discounted_price }} <br>(@lang('mails.inclusive_vat'))
          </p>
        </td>
      </tr>
    </tbody>
    <tfooter>
 
    </tfooter>
  </table>
</body>

</html>
