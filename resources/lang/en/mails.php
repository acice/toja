<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Mail Language Lines
    |--------------------------------------------------------------------------

    */

	"invoice"     => "Invoice",
	"amount_paid"     => "Amount Paid",
	"outstanding_balance"     => "Outstanding Balance",
	"total"    => "Total",
	"inclusive_vat"  => "Inclusive Of VAT",
	"currency"  => "₪ ",
];
