<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Banks Language List
    |--------------------------------------------------------------------------
    |
    |
    */

    "b01" => 'Bank Leumi LeIsrael Ltd.',
    "b02" => 'Bank Hapoalim Ltd.',
    "b03" => 'Israel Discount Bank Ltd.',
    "b04" => 'Bank Yahav for Civil Servants Ltd.',
    "b05" => 'Mizrahi Tefahot Bank Ltd.',
    "b06" => 'Union Bank of Israel Ltd.',
    "b07" => 'Otzar Hachayal Bank Ltd.',
    "b08" => 'Mercantile Discount Bank Ltd.',
    "b09" => 'One Zero Digital Bank Ltd.',
    "b10" => 'Bank of Jerusalem Ltd.',
    "b11" => "HSBC Bank",
    "b12" => 'UBank Ltd.',
    "b13" => 'The First International Bank of Israel Ltd.',
    "b14" => 'Masad Bank Ltd.',
    "b15" => 'Bank Poalei Agudat Israel Ltd.',
    "b16" => 'Bank of Jerusalem Ltd.',

];
