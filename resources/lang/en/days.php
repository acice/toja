<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Days Language Lines
    |--------------------------------------------------------------------------

    */

 
	"Sunday"     => "Sunday",
	"Monday"     => "Monday",
	"Tuesday"    => "Tuesday",
	"Wednesday"  => "Wednesday",
	"Thursday"   => "Thursday",
	"Friday"     => "Friday",
	"Saturday"   => "Saturday",
	"Open 24 Hrs" =>  "Open 24 Hrs",
	"Closed" 		=>  "Closed",
];
