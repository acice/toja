<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

   
	"accepted"       => "חובה להסכים ל-.",
	"active_url"     => "הערך  חייב להכיל כתובת אינטרנט פעילה.",
	"after"          => "הערך  חייב להכיל תאריך אחרי :date.",
	"alpha"          => "הערך  יכול להכיל רק אותיות.",
	"alpha_dash"     => "הערך  יכול להכיל רק אותיות, מספרים ומקפים.",
	"alpha_num"      => "הערך  יכול להכיל רק אותיות ומספרים.",
	"array"          => "חייב להיות רכיבים נבחרים.",
	"before"         => "הערך  חייב להכיל תאריך לפני :date.",
	"between"        => array(
		"numeric" => "הערך  חייב להיות בין :min ל-:max.",
		"file"    => "הערך  חייב לשקול בין :min ל-:max ק&quot;ב.",
		"string"  => "הערך  חייב להכיל בין :min ל-:max .",
	),
	"confirmed"      => "הערכים של  חייבים להיות זהים.",
	"count"          => "חייב לכלול בדיוק :count רכיבים נבחרים.",
	"countbetween"   => "חייבים להכיל בין :min ל-:max רכיבים שנבחרו.",
	"countmax"       => "חייב לכלול פחות מ-:max רכיבים שנבחרו.",
	"countmin"       => "חייבים לכלול לפחות :min רכיבים נבחרים.",
	"different"      => "הערכים של ו-:other חייבים להיות שונים.",
	"email"          => "הערך  חייב להכיל כתובת אימייל תקינה.",
	"exists"         => "הערך  לא קיים.",
	"image"          => "הערך  חייב להיות תמונה.",
	"in"             => "הערך  חייב להיות ברשימה המאשרת.",
	"integer"        => "הערך  חייב להיות מספר שלם.",
	"ip"             => "הערך  חייב להיות כתובת IP תקינה.",
	"match"          => "התבנית של הערך  אינה תקינה.",
	"max"            => array(
		"numeric" => "הערך  חייב להיות פחות מ-:max.",
		"file"    => "הערך  חייב לשקול פחות מ-:max ק&quotב.",
		"string"  => "הערך  חייב להכיל פחות מ-:max תווים.",
	),
	"mimes"          => "הערך  חייב להיות קובץ מסוג: :values.",
	"min"            => array(
		"numeric" => "הערך  חייב להיות לפחות :min.",
		"file"    => "הערך  חייב לשקול לפחות :min ק&quot;ב.",
		"string"  => "הערך חייב להכיל לפחות :min תווים.",
	),
	"not_in"         => "הערך  נמצא ברשימה השחורה.",
	"numeric"        => "הערך  חייב להיות מספר.",
	"required"       => "חובה למלא את הערך .",
	"same"           => "הערכים  ו-:other חייבים להיות זהים.",
	"size"           => array(
		"numeric" => "הערך  חייב להיות :size.",
		"file"    => "הערך  חייב לשקול :size ק&quot;ב.",
		"string"  => "הערך  חייב להכיל :size תווים.",
	),
	"unique"         => "הערך  כבר קיים.",
	"url"            => "הערך  חייב להכיל כתובת אינטרנט תקינה.",
	'password_doesnt_match'=>'הסיסמה הנוכחית שגויה',
	'something_went_wrong'=> 'מצטערים, משהו השתבש',
	'social_already_exists' => 'החשבון קיים!! נסה להיכנס',
	'user_unavailable' => 'משתמש לא זמין! אנא הירשם קודם',
	'account_created' => 'משתמש שנוצר בהצלחה!',
	'login_success' => 'התחבר בהצלחה',
	'logout_success' => 'התנתק בהצלחה',
	'invalid_credentials' => 'אימייל או סיסמה שגויים ',
	'activate_account' => 'אנא הפעל את חשבונך: בדוק את האימייל שלך!!',
	'invalid_token' => 'אסימון הפעלה זה אינו חוקי',
	'unable_get_category' => 'לא ניתן לקבל קטגוריות',
	'select_one_category' =>'אנא בחר לפחות קטגוריה אחת',
	'select_atleast_one_category' =>'אנא בחר לפחות קטגוריה אחת',
	'added_category' => 'נוספה העדפת קטגוריה',
	'unable_add_category' => 'לא ניתן להוסיף העדפת קטגוריה.',
	'update_category_preferences' => 'העדפת קטגוריה מעודכנת',
	'unable_update_category'=>'לא ניתן לשמור את העדפת המרחק',
	'update_distance_preferences' => 'העדפת מרחק שמורה',
    'unable_update_distance'=>'לא ניתן לשמור את העדפת המרחק',
	'update_operation_time' => 'זמן הפעלה נשמר',
    'unable_update_operation_time'=>' לא ניתן לשמור את העדפת זמן ההפעלה',
	'only_vendor_can_update_operation'=>' רק הספק יכול לעדכן את זמני הפעולה',
	'successful_create_comment'=>'לא ניתן לעדכן את העדפת הקטגוריה.',
	'unable_create_comment'=>'לא ניתן ליצור תגובה.',
	'successful_update_comment'=>'התגובה עודכנה בהצלחה.',
	'unable_update_comment'=>'לא ניתן לעדכן תגובה',
	'successful_remove_comment'=>'ההערה הוסרה בהצלחה',
	'unable_retrive_comment'=>'לא ניתן לאחזר תגובה.',
	'need_id_to_like' => 'אתה צריך את מזהה הפוסט כדי לסמן לייק לפוסט!',
	'successful_like' => 'פוסט אהב בהצלחה' ,
	'unable_like' => 'לא מצליח לעשות לייק לפוסט',
	'no_post' => 'סליחה שהפוסט לא קיים',
	'successful_unlike' => 'פוסט שלא אהב בהצלחה',
	'one_deal_per_account'=> 'מצטערים, אתה יכול להפעיל רק עסקה אחת לכל חשבון',
	'no_of_deals_redeemed'=> 'מצטערים, כל העסקאות מומשו לחלוטין',
	'redeem_deal'=> 'דיל מומש!',
	'minimum_discount_amount'=>'מצטערים, סכום ההנחה חייב להיות לפחות 10%.',
	'successful_create_post'=>'הפוסט נוצר בהצלחה.',
	'unable_create_post'=> 'לא ניתן ליצור פוסט',
	'max_image_upload' =>'אתה יכול להעלות רק מקסימום תמונות ',
	'successful_upload_image' => 'התמונה נוספה בהצלחה',
	'unable_upload_image' => 'לא ניתן להוסיף תמונה',
	'successful_removed_image' => 'התמונה הוסרה בהצלחה',
	'unable_removed_image' => 'לא ניתן להסיר תמונה',
	'successful_update_post' => 'הפוסט עודכן בהצלחה',
    'unable_update_post' => 'לא ניתן לעדכן את הפוסט',
	'successfully_remove_post'=> 'הפוסט הוסר בהצלחה',
    'unable_remove_post'=> 'לא ניתן להסיר את הפוסט',
	'post_has_deal'=> 'לא ניתן להסיר פוסט עם דיל',
	'unable_get_post' => 'לא ניתן לקבל את הפוסטים',
	'unable_get_postid' => 'לא ניתן למצוא פוסט עם המזהה הזה',
	'successful_tagged' => 'משתמש תייג בהצלחה.',
	'successful_removed_tagged' => 'משתמש מתויג הוסר בהצלחה.',
    'unable_removed_tagged' => 'לא ניתן להסיר משתמש מתויג.',
	'unable_tag_with_id'=> 'לא ניתן למצוא תג עם המזהה הזה',
	'successfuly_tag'=> 'משתמש בוטל בהצלחה.',
    'unable_tag'=> 'לא ניתן לבטל את תיוג המשתמש.',
	'no_tags' =>'אין פוסטים מתויגים',
	'invalid_data' => 'הנתונים שניתנו לא היו חוקיים',
	'price_preference_set' => 'הגדרת העדפת מחיר',
	'unable_to_add_Price' => 'לא ניתן להוסיף העדפת מחיר',
    'successfully_updated' => 'הפרטים עודכנו בהצלחה',
	'unable_to_update' => 'לא ניתן לעדכן פרטים',
	'successfuly_updated_location' => 'פרטי המיקום עודכנו בהצלחה',
    'unable_to_update_location' => 'לא ניתן לעדכן את פרטי הספק',
	'vendor_unavailable' => 'הספק לא זמין! נסה חשבון אחר',
	'onboarding_complete' => 'ההטמעה הושלמה',
    'unable_onboarding_complete'=>' לא ניתן להשלים את ההטמעה',
	'password_change' => 'סיסמא שונתה',
	'updated_profile'=> 'תמונת פרופיל מעודכנת',
	'unable_updated_profile'=> ' לא ניתן לעדכן את תמונת הפרופיל',
	'unable_setting'=> 'לא ניתן לקבל הגדרות',
	'followed' => 'אחריו ',
	'unable_followed' => 'לא מצליח לעקוב ',
	'unfollowed' => 'ביטול המעקב ',
    'unable_unfollowed' => 'לא ניתן לבטל את המעקב ',
	'unable_bank'=> 'לא ניתן לקבל הגדרות ',
	'only_for_users'=>'ממשק API זה מקבל רק משתמשים שאינם ספקים.',
	'unable_get_all_user'=>'לא ניתן להשיג את כל המשתמשים.',
	'unable_get_vendor'=>'לא ניתן להשיג ספק',
	'successful_add_favourite'=>'נוסף בהצלחה למועדפים',
    'successful_removed_favourite'=>'הוסר בהצלחה מהמועדפים',
	'unable_removed_favourite'=>'לא ניתן להסיר למועדפים',
	'email_not_found' => 'אנחנו לא יכולים למצוא משתמש עם כתובת האימייל הזו.',
	'magic_link' => 'קישור קסם נשלח למייל שלך',
	'unable_find_deal' => 'מצטער, לא ניתן למצוא את העסקה המופעלת.',
	'select_current_deal'=>'מצטערים, אנא בחר עסקה המופעלת כעת לביטול',
	'only_cancel_deal_own' => 'מצטערים, אתה יכול לבטל רק עסקאות בבעלותך.',
	'deal_cancelled' => 'העסקה בוטלה.',
	'image_required' => 'נדרשת תמונה',
	'consumer_temporary_unavailable' => 'הרשמה לצרכן אינה זמינה באופן זמני',
	'consumer_signin_temporary_unavailable' => 'כניסה לצרכן אינה זמינה באופן זמני',
	'unable_customtime' => 'לא ניתן לקבל את הזמן המותאם אישית',
	'deal_redeem_title' => 'דיל מומש על ידי',
    'deal_redeem_by' => ' העסקה מומשה על ידי',
	'deal_cancel_title' => 'העסקה בוטלה על ידי',
    'deal_cancel_by' => ' העסקה בוטלה על ידי',
	'deal_complete_title' => 'העסקה הושלמה',
    'deal_complete_by' => ' העסקה סומנה כמומשה לחלוטין על ידי הספק.',
	'deal_reject_title' => 'העסקה נדחתה',
    'deal_reject_by' => '  העסקה נדחתה על ידי הספק',
    'deal_confirm_title' => 'העסקה אושרה',
    'deal_confirm_by' => '  העסקה אושרה. נא להמשיך לתשלום',
	'deal_payment_title' => 'העסקה אושרה על ידי',
    'deal_payment_by' => ' אישר את העסקה!!',
	'followed_title' => ' עקב אחריך ',
    'followed_by' => ' אחריך',
	'comment_create' => ' הגיב על הפוסט שלך.',
    'comment_create_by' => 'הפוסט שלך הגיב על ידי ',
    'comment_reply' => ' השיב על תגובתך.',
    'comment_reply_by' => ' נענית על ידי ',
	'liked_title' => ' השיב על תגובתך.',
    'liked_by' => ' נענית על ידי ',
	'notification_read' => 'הודעה מסומנת כנקראה',
	'notification_all_read' => 'הודעה מסומנת כנקראה',
	
    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [],

];
