<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * App\Models\ActivatedDeals
 *
 * @property int $id
 * @property int $user_id
 * @property int $vendor_id
 * @property int $post_id
 * @property string $confirmed
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $status
 * @property string|null $preference
 * @property string|null $invoice_no
 * @property string|null $reject_reason
 * @property-read mixed $created_at_formated
 * @property-read \App\Models\PaymentDetails|null $payment
 * @property-read \App\Models\Post $post
 * @property-read \App\Models\User $user
 * @property-read \App\Models\Vendor $vendor
 * @method static \Illuminate\Database\Eloquent\Builder|ActivatedDeals newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ActivatedDeals newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ActivatedDeals query()
 * @method static \Illuminate\Database\Eloquent\Builder|ActivatedDeals whereConfirmed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ActivatedDeals whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ActivatedDeals whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ActivatedDeals whereInvoiceNo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ActivatedDeals wherePostId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ActivatedDeals wherePreference($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ActivatedDeals whereRejectReason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ActivatedDeals whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ActivatedDeals whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ActivatedDeals whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ActivatedDeals whereVendorId($value)
 */
	class ActivatedDeals extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Bank
 *
 * @property int $id
 * @property int $vendor_id
 * @property string $account_no
 * @property string|null $routing_no
 * @property string|null $name
 * @property string|null $branch
 * @property string|null $address
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Vendor $vendor
 * @method static \Illuminate\Database\Eloquent\Builder|Bank newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Bank newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Bank query()
 * @method static \Illuminate\Database\Eloquent\Builder|Bank whereAccountNo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Bank whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Bank whereBranch($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Bank whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Bank whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Bank whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Bank whereRoutingNo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Bank whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Bank whereVendorId($value)
 */
	class Bank extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\BaseModel
 *
 * @method static \Illuminate\Database\Eloquent\Builder|BaseModel nPerGroup($group, $n = 10)
 * @method static \Illuminate\Database\Eloquent\Builder|BaseModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BaseModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BaseModel query()
 */
	class BaseModel extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Category
 *
 * @property int $id
 * @property string $name
 * @property string|null $iw_name
 * @property string $slug
 * @property int $parent_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CategoryLink[] $latestVendors
 * @property-read int|null $latest_vendors_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Vendor[] $vendors
 * @property-read int|null $vendors_count
 * @method static \Illuminate\Database\Eloquent\Builder|BaseModel nPerGroup($group, $n = 10)
 * @method static \Illuminate\Database\Eloquent\Builder|Category newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Category newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Category query()
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereIwName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereUpdatedAt($value)
 */
	class Category extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\CategoryLink
 *
 * @property int $id
 * @property int $user_id
 * @property int $vendor_id
 * @property int $category_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Vendor[] $vendor
 * @property-read int|null $vendor_count
 * @method static \Illuminate\Database\Eloquent\Builder|BaseModel nPerGroup($group, $n = 10)
 * @method static \Illuminate\Database\Eloquent\Builder|CategoryLink newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CategoryLink newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CategoryLink query()
 * @method static \Illuminate\Database\Eloquent\Builder|CategoryLink whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CategoryLink whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CategoryLink whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CategoryLink whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CategoryLink whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CategoryLink whereVendorId($value)
 */
	class CategoryLink extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Comment
 *
 * @property int $id
 * @property string $body
 * @property string|null $iw_body
 * @property int $user_id
 * @property int $post_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int|null $parent_id
 * @property-read mixed $child_count
 * @property-read \App\Models\Post $post
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Post[] $tags
 * @property-read int|null $tags_count
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|Comment filterLikedFor($model)
 * @method static \Illuminate\Database\Eloquent\Builder|Comment filterUnlikedFor($model)
 * @method static \Illuminate\Database\Eloquent\Builder|Comment likedBy($model)
 * @method static \Illuminate\Database\Eloquent\Builder|Comment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Comment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Comment notLikedBy($model)
 * @method static \Illuminate\Database\Eloquent\Builder|Comment query()
 * @method static \Illuminate\Database\Eloquent\Builder|Comment whereBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Comment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Comment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Comment whereIwBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Comment whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Comment wherePostId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Comment whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Comment whereUserId($value)
 */
	class Comment extends \Eloquent implements \Rennokki\Befriended\Contracts\Liking, \Rennokki\Befriended\Contracts\Liker, \Rennokki\Befriended\Contracts\Likeable {}
}

namespace App\Models{
/**
 * App\Models\CustomTime
 *
 * @property int $id
 * @property string $day
 * @property string|null $status
 * @property string|null $opening
 * @property string|null $closing
 * @property int $vendor_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Vendor $vendor
 * @method static \Illuminate\Database\Eloquent\Builder|CustomTime newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomTime newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomTime query()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomTime whereClosing($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomTime whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomTime whereDay($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomTime whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomTime whereOpening($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomTime whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomTime whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomTime whereVendorId($value)
 */
	class CustomTime extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\DiscountDay
 *
 * @property int $id
 * @property int $post_id
 * @property string $day
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|DiscountDay newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DiscountDay newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DiscountDay query()
 * @method static \Illuminate\Database\Eloquent\Builder|DiscountDay whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscountDay whereDay($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscountDay whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscountDay wherePostId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscountDay whereUpdatedAt($value)
 */
	class DiscountDay extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\EventPurchasedTickets
 *
 * @property int $id
 * @property int $user_id
 * @property int $event_ticket_id
 * @property int $amount
 * @property float $price
 * @property string $merchant
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\EventTicket $eventTicket
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|EventPurchasedTickets newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|EventPurchasedTickets newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|EventPurchasedTickets query()
 * @method static \Illuminate\Database\Eloquent\Builder|EventPurchasedTickets whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EventPurchasedTickets whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EventPurchasedTickets whereEventTicketId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EventPurchasedTickets whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EventPurchasedTickets whereMerchant($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EventPurchasedTickets wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EventPurchasedTickets whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EventPurchasedTickets whereUserId($value)
 */
	class EventPurchasedTickets extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\EventTicket
 *
 * @property int $id
 * @property string $name
 * @property int $amount
 * @property float $price
 * @property int $event_id
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Event $event
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|EventTicket newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|EventTicket newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|EventTicket query()
 * @method static \Illuminate\Database\Eloquent\Builder|EventTicket whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EventTicket whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EventTicket whereEventId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EventTicket whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EventTicket whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EventTicket wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EventTicket whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EventTicket whereUserId($value)
 */
	class EventTicket extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Favourite
 *
 * @property int $id
 * @property int $user_id
 * @property int $post_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Post $post
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|Favourite newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Favourite newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Favourite query()
 * @method static \Illuminate\Database\Eloquent\Builder|Favourite whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Favourite whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Favourite wherePostId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Favourite whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Favourite whereUserId($value)
 */
	class Favourite extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\File
 *
 * @property int $id
 * @property int $user_id
 * @property int $post_id
 * @property int $event_id
 * @property int $vendor_id
 * @property string $file
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read mixed $file_url
 * @property-read \App\Models\Post $post
 * @method static \Illuminate\Database\Eloquent\Builder|File newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|File newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|File query()
 * @method static \Illuminate\Database\Eloquent\Builder|File whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|File whereEventId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|File whereFile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|File whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|File wherePostId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|File whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|File whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|File whereVendorId($value)
 */
	class File extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Followers
 *
 * @property int $id
 * @property int $followable_id
 * @property string $followable_type
 * @property int|null $follower_id
 * @property string|null $follower_type
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\User $following
 * @property-read mixed $name
 * @method static \Illuminate\Database\Eloquent\Builder|Followers newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Followers newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Followers query()
 * @method static \Illuminate\Database\Eloquent\Builder|Followers whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Followers whereFollowableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Followers whereFollowableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Followers whereFollowerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Followers whereFollowerType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Followers whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Followers whereUpdatedAt($value)
 */
	class Followers extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Invoice
 *
 * @property int $id
 * @property int $vendor_id
 * @property string $email
 * @property string $name
 * @property string|null $iw_name
 * @property string $description
 * @property string|null $iw_description
 * @property float $price
 * @property float $admincharge
 * @property float $amountsaved
 * @property float|null $discount
 * @property int|null $dealsactivated
 * @property int $paid
 * @property string|null $email_sent
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\User|null $userPayerDetails
 * @property-read \App\Models\Vendor $vendor
 * @property-read \App\Models\PaymentDetails|null $vendorPaymentDetails
 * @method static \Illuminate\Database\Eloquent\Builder|Invoice invoicePaid($invoice_id)
 * @method static \Illuminate\Database\Eloquent\Builder|Invoice newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Invoice newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Invoice query()
 * @method static \Illuminate\Database\Eloquent\Builder|Invoice whereAdmincharge($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Invoice whereAmountsaved($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Invoice whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Invoice whereDealsactivated($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Invoice whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Invoice whereDiscount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Invoice whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Invoice whereEmailSent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Invoice whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Invoice whereIwDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Invoice whereIwName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Invoice whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Invoice wherePaid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Invoice wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Invoice whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Invoice whereVendorId($value)
 */
	class Invoice extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Notifications
 *
 * @property string $id
 * @property string $type
 * @property string $notifiable_type
 * @property int $notifiable_id
 * @property array $data
 * @property \Illuminate\Support\Carbon|null $read_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $notifiable
 * @property-read \App\Models\User $users
 * @method static \Illuminate\Notifications\DatabaseNotificationCollection|static[] all($columns = ['*'])
 * @method static \Illuminate\Notifications\DatabaseNotificationCollection|static[] get($columns = ['*'])
 * @method static \Illuminate\Database\Eloquent\Builder|Notifications newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Notifications newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Notifications query()
 * @method static \Illuminate\Database\Eloquent\Builder|Notifications whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Notifications whereData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Notifications whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Notifications whereNotifiableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Notifications whereNotifiableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Notifications whereReadAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Notifications whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Notifications whereUpdatedAt($value)
 */
	class Notifications extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\PasswordReset
 *
 * @property int $id
 * @property string $email
 * @property string $token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|PasswordReset newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PasswordReset newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PasswordReset query()
 * @method static \Illuminate\Database\Eloquent\Builder|PasswordReset whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PasswordReset whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PasswordReset whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PasswordReset whereToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PasswordReset whereUpdatedAt($value)
 */
	class PasswordReset extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\PayerDetails
 *
 * @property int $id
 * @property int $user_id
 * @property int $invoice_id
 * @property string $data
 * @property string $details
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|PayerDetails newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PayerDetails newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PayerDetails query()
 * @method static \Illuminate\Database\Eloquent\Builder|PayerDetails whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayerDetails whereData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayerDetails whereDetails($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayerDetails whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayerDetails whereInvoiceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayerDetails whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayerDetails whereUserId($value)
 */
	class PayerDetails extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\PaymentDetails
 *
 * @property int $id
 * @property string $payment_id
 * @property string $name
 * @property string $payment_status
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property float $amount
 * @property int $post_id
 * @property string|null $activated_deals_id
 * @property float $vendor_amount
 * @property-read \App\Models\ActivatedDeals|null $activated_deals
 * @property-read \App\Models\Post $post
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentDetails newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentDetails newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentDetails query()
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentDetails whereActivatedDealsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentDetails whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentDetails whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentDetails whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentDetails whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentDetails wherePaymentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentDetails wherePaymentStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentDetails wherePostId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentDetails whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentDetails whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentDetails whereVendorAmount($value)
 */
	class PaymentDetails extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Post
 *
 * @property int $id
 * @property string|null $body
 * @property string|null $iw_body
 * @property int $user_id
 * @property string|null $discount_start_time
 * @property string|null $discount_end_time
 * @property float|null $discount_cost
 * @property int|null $discount_amount
 * @property string|null $discount_end
 * @property int|null $discount_activations
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property string|null $title
 * @property string|null $address_id
 * @property string|null $iw_title
 * @property string $availibility
 * @property string|null $time_from
 * @property string|null $time_to
 * @property string|null $sit_in_term
 * @property int $activation_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ActivatedDeals[] $activatedDeals
 * @property-read int|null $activated_deals_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ActivatedDeals[] $activatedDealsRejected
 * @property-read int|null $activated_deals_rejected_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Comment[] $comments
 * @property-read int|null $comments_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\DiscountDay[] $discountdays
 * @property-read int|null $discountdays_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Favourite[] $favourites
 * @property-read int|null $favourites_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\File[] $files
 * @property-read int|null $files_count
 * @property-read mixed $user_favourite
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PaymentDetails[] $payments
 * @property-read int|null $payments_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TaggedPosts[] $tags
 * @property-read int|null $tags_count
 * @property-read \App\Models\User $user
 * @property-read \App\Models\Followers $userfollowing
 * @method static \Illuminate\Database\Eloquent\Builder|Post activePosts()
 * @method static \Illuminate\Database\Eloquent\Builder|Post favoriteByUser()
 * @method static \Illuminate\Database\Eloquent\Builder|Post filterBlockingsOf($model)
 * @method static \Illuminate\Database\Eloquent\Builder|Post filterFollowingsOf($model)
 * @method static \Illuminate\Database\Eloquent\Builder|Post filterLikedFor($model)
 * @method static \Illuminate\Database\Eloquent\Builder|Post filterUnfollowingsOf($model)
 * @method static \Illuminate\Database\Eloquent\Builder|Post filterUnlikedFor($model)
 * @method static \Illuminate\Database\Eloquent\Builder|Post followedBy($model)
 * @method static \Illuminate\Database\Eloquent\Builder|Post likedBy($model)
 * @method static \Illuminate\Database\Eloquent\Builder|Post newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Post newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Post notLikedBy($model)
 * @method static \Illuminate\Database\Query\Builder|Post onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Post postsNotByUser()
 * @method static \Illuminate\Database\Eloquent\Builder|Post query()
 * @method static \Illuminate\Database\Eloquent\Builder|Post unfollowedBy($model)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereActivationCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereAddressId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereAvailibility($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereDiscountActivations($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereDiscountAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereDiscountCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereDiscountEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereDiscountEndTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereDiscountStartTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereIwBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereIwTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereSitInTerm($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereTimeFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereTimeTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|Post withTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Post withoutBlockingsOf($model)
 * @method static \Illuminate\Database\Query\Builder|Post withoutTrashed()
 */
	class Post extends \Eloquent implements \Rennokki\Befriended\Contracts\Liking, \Rennokki\Befriended\Contracts\Following, \Rennokki\Befriended\Contracts\Liker, \Rennokki\Befriended\Contracts\Likeable, \Rennokki\Befriended\Contracts\Follower, \Rennokki\Befriended\Contracts\Followable {}
}

namespace App\Models{
/**
 * App\Models\PricePreference
 *
 * @property int $id
 * @property int|null $user_id
 * @property int|null $vendor_id
 * @property int $price_preference
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\User $pricePreference
 * @method static \Illuminate\Database\Eloquent\Builder|PricePreference newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PricePreference newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PricePreference query()
 * @method static \Illuminate\Database\Eloquent\Builder|PricePreference whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PricePreference whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PricePreference wherePricePreference($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PricePreference whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PricePreference whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PricePreference whereVendorId($value)
 */
	class PricePreference extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\TaggedPosts
 *
 * @property int $id
 * @property int $post_id
 * @property int $user_id
 * @property int $user_tagged_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Post[] $posts
 * @property-read int|null $posts_count
 * @property-read \App\Models\User $tagged
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|TaggedPosts newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TaggedPosts newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TaggedPosts query()
 * @method static \Illuminate\Database\Eloquent\Builder|TaggedPosts whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TaggedPosts whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TaggedPosts wherePostId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TaggedPosts whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TaggedPosts whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TaggedPosts whereUserTaggedId($value)
 */
	class TaggedPosts extends \Eloquent {}
}

namespace App\Models{
/**
 * User model
 *
 * @property int $id
 * @property string|null $name
 * @property string $email
 * @property string|null $email_verified_at
 * @property string $password
 * @property string $avatar
 * @property int $active
 * @property string|null $activation_token
 * @property int $identifier
 * @property int $distance
 * @property string|null $flag
 * @property int $posted_notification
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property bool $isadmin
 * @property string|null $phone_no
 * @property string|null $provider
 * @property string|null $provider_id
 * @property string|null $access_token
 * @property string $onboarding_status
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ActivatedDeals[] $activatedDeals
 * @property-read int|null $activated_deals_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Category[] $categories
 * @property-read int|null $categories_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Client[] $clients
 * @property-read int|null $clients_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Favourite[] $favourites
 * @property-read int|null $favourites_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\File[] $files
 * @property-read int|null $files_count
 * @property-read mixed $avatar_url
 * @property-read mixed $no_password
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\UserLogin[] $logins
 * @property-read int|null $logins_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PaymentDetails[] $payments
 * @property-read int|null $payments_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Post[] $posts
 * @property-read int|null $posts_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Token[] $tokens
 * @property-read int|null $tokens_count
 * @property-read \App\Models\Followers $usersFollowing
 * @property-read \App\Models\Vendor|null $vendor
 * @method static \Illuminate\Database\Eloquent\Builder|User filterBlockingsOf($model)
 * @method static \Illuminate\Database\Eloquent\Builder|User filterFollowingsOf($model)
 * @method static \Illuminate\Database\Eloquent\Builder|User filterLikedFor($model)
 * @method static \Illuminate\Database\Eloquent\Builder|User filterUnfollowingsOf($model)
 * @method static \Illuminate\Database\Eloquent\Builder|User filterUnlikedFor($model)
 * @method static \Illuminate\Database\Eloquent\Builder|User followedBy($model)
 * @method static \Illuminate\Database\Eloquent\Builder|User getUser($id)
 * @method static \Illuminate\Database\Eloquent\Builder|User getUsersByNameOrEmail($request)
 * @method static \Illuminate\Database\Eloquent\Builder|User getVendor($id)
 * @method static \Illuminate\Database\Eloquent\Builder|User likedBy($model)
 * @method static \Illuminate\Database\Eloquent\Builder|User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User notLikedBy($model)
 * @method static \Illuminate\Database\Eloquent\Builder|User ofType($type)
 * @method static \Illuminate\Database\Query\Builder|User onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|User query()
 * @method static \Illuminate\Database\Eloquent\Builder|User unfollowedBy($model)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereAccessToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereActivationToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereAvatar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereDistance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereFlag($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereIdentifier($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereIsadmin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereOnboardingStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePhoneNo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePostedNotification($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereProvider($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereProviderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|User withTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|User withoutBlockingsOf($model)
 * @method static \Illuminate\Database\Query\Builder|User withoutTrashed()
 */
	class User extends \Eloquent implements \Rennokki\Befriended\Contracts\Following, \Rennokki\Befriended\Contracts\Blocking, \Rennokki\Befriended\Contracts\Liking, \Rennokki\Befriended\Contracts\Follower, \Rennokki\Befriended\Contracts\Followable, \Rennokki\Befriended\Contracts\Blocker, \Rennokki\Befriended\Contracts\Blockable, \Rennokki\Befriended\Contracts\Liker, \Rennokki\Befriended\Contracts\Likeable {}
}

namespace App\Models{
/**
 * App\Models\UserLogin
 *
 * @property int $id
 * @property int|null $user_id
 * @property string|null $email
 * @property string $ip_address
 * @property int $successful
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $longitude
 * @property string|null $latitude
 * @property-read \App\Models\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|UserLogin newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserLogin newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserLogin query()
 * @method static \Illuminate\Database\Eloquent\Builder|UserLogin whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserLogin whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserLogin whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserLogin whereIpAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserLogin whereLatitude($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserLogin whereLongitude($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserLogin whereSuccessful($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserLogin whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserLogin whereUserId($value)
 */
	class UserLogin extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Vendor
 *
 * @property int $id
 * @property string $name
 * @property string|null $iw_name
 * @property string|null $body
 * @property string|null $iw_body
 * @property string|null $address
 * @property string|null $postcode
 * @property string|null $data
 * @property int $user_id
 * @property string|null $longitude
 * @property string|null $latitude
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $company_number
 * @property string|null $address_id
 * @property string|null $street
 * @property string|null $city
 * @property string|null $opening_days
 * @property string|null $opening_time
 * @property string|null $closing_time
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ActivatedDeals[] $activatedDeals
 * @property-read int|null $activated_deals_count
 * @property-read \App\Models\Bank|null $bank
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Category[] $categories
 * @property-read int|null $categories_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CustomTime[] $customTime
 * @property-read int|null $custom_time_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\File[] $files
 * @property-read int|null $files_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Post[] $posts
 * @property-read int|null $posts_count
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|Vendor distance($latitude, $longitude)
 * @method static \Illuminate\Database\Eloquent\Builder|Vendor filterBlockingsOf($model)
 * @method static \Illuminate\Database\Eloquent\Builder|Vendor filterFollowingsOf($model)
 * @method static \Illuminate\Database\Eloquent\Builder|Vendor filterLikedFor($model)
 * @method static \Illuminate\Database\Eloquent\Builder|Vendor filterUnfollowingsOf($model)
 * @method static \Illuminate\Database\Eloquent\Builder|Vendor filterUnlikedFor($model)
 * @method static \Illuminate\Database\Eloquent\Builder|Vendor followedBy($model)
 * @method static \Illuminate\Database\Eloquent\Builder|Vendor geofence($latitude, $longitude, $inner_radius, $outer_radius)
 * @method static \Illuminate\Database\Eloquent\Builder|Vendor isOpen()
 * @method static \Illuminate\Database\Eloquent\Builder|Vendor likedBy($model)
 * @method static \Illuminate\Database\Eloquent\Builder|Vendor newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Vendor newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Vendor notLikedBy($model)
 * @method static \Illuminate\Database\Eloquent\Builder|Vendor query()
 * @method static \Illuminate\Database\Eloquent\Builder|Vendor unfollowedBy($model)
 * @method static \Illuminate\Database\Eloquent\Builder|Vendor whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vendor whereAddressId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vendor whereBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vendor whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vendor whereClosingTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vendor whereCompanyNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vendor whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vendor whereData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vendor whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vendor whereIwBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vendor whereIwName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vendor whereLatitude($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vendor whereLongitude($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vendor whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vendor whereOpeningDays($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vendor whereOpeningTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vendor wherePostcode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vendor whereStreet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vendor whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vendor whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vendor withoutBlockingsOf($model)
 */
	class Vendor extends \Eloquent {}
}

