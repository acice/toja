<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVendorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendors', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('iw_name')->nullable();
            $table->string('body', 1000);
            $table->string('iw_body', 1000)->nullable();
            $table->string('address')->nullable();
            $table->string('postcode')->nullable();
            $table->text('data')->nullable();
            $table->integer('user_id');
            $table->decimal('longitude', 10, 8)->default(NULL);
            $table->decimal('latitude', 11, 8)->default(NULL);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendors');
    }
}
