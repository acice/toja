<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateActiveDealsFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('activated_deals', function (Blueprint $table) {
            $table->string('status')->default('Pending');
            $table->string('preference')->nullable();
            $table->string('invoice_no')->nullable();
            $table->text('reject_reason')->nullable();
            $table->unsignedInteger('payment_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
