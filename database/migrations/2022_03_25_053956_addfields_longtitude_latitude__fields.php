<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddfieldsLongtitudeLatitudeFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_logins', function (Blueprint $table) {
            $table->decimal('longitude', 15, 8)->nullable()->change();
            $table->decimal('latitude', 15, 8)->nullable()->change();
        });

        Schema::table('vendors', function (Blueprint $table) {
            $table->decimal('longitude', 15, 8)->nullable()->change();
            $table->decimal('latitude', 15, 8)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
