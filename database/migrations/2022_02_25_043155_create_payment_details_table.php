<?php

use App\Helpers\DbConstants;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            DbConstants::TABLE_PAYMENT_DETAILS,
            function (Blueprint $table) {
                $table->increments('id');
                $table->string('merchant_id');
                $table->string('name');
                $table->string('email');
                $table->integer('user_id');
                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(DbConstants::TABLE_PAYMENT_DETAILS);
    }
}
