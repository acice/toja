<?php

use App\Helpers\DbConstants;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayerDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            DbConstants::TABLE_PAYER_DETAILS,
            function (Blueprint $table) {
                $table->increments('id');
                $table->integer('user_id');
                $table->integer('invoice_id');
                $table->text('data');
                $table->text('details');
                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(DbConstants::TABLE_PAYER_DETAILS);
    }
}
