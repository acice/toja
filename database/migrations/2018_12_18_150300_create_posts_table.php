<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('body')->nullable();
            $table->string('iw_body')->nullable();
            $table->integer('user_id');
            $table->time('discount_start_time')->nullable();
            $table->time('discount_end_time')->nullable();
            $table->integer('discount_cost')->nullable();
            $table->integer('discount_amount')->nullable();
            $table->timestamp('discount_end')->nullable();
            $table->integer('discount_activations')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
