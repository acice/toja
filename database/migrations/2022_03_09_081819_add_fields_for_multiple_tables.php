<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsForMultipleTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('phone_no', 20)->nullable();
        });

        Schema::table('vendors', function (Blueprint $table) {
            $table->text('street' )->nullable();
            $table->text('city')->nullable();
        });

        Schema::table('posts', function (Blueprint $table) {
            $table->text('title')->nullable();
            $table->string('address_id', 50)->nullable();
            $table->text('body')->change();
            $table->text('iw_body')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
