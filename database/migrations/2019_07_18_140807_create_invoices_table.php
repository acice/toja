<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vendor_id');
            $table->string('email');
            $table->string('name');
            $table->string('iw_name')->nullable();
            $table->text('description');
            $table->text('iw_description')->nullable();
            $table->double('price', 15, 2)->default(0.00);
            $table->double('admincharge', 15, 2)->default(0.00);
            $table->double('amountsaved', 15, 2)->default(0.00);
            $table->float('discount')->default(0)->nullable();
            $table->integer('dealsactivated')->default(0)->nullable();
            $table->integer('paid')->default(false);
            $table->timestamp('email_sent')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
