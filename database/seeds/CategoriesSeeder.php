<?php

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            [
                'name' => 'Food & Drink',
                'iw_name' => 'מזון ומשקאות',
                'slug' => 'food-drink',
                'parent_id' => 0
            ],
            [
                'name' => 'Chinese',
                'iw_name' => 'סינית',
                'slug' => 'chinese',
                'parent_id' => 3
            ],
            [
                'name' => 'Japanese',
                'iw_name' => 'יפנית',
                'slug' => 'japanese',
                'parent_id' => 3
            ],
            [
                'name' => 'Indian',
                'iw_name' => 'הודי',
                'slug' => 'indian',
                'parent_id' => 3
            ],
            [
                'name' => 'Italian',
                'iw_name' => 'איטלקית',
                'slug' => 'italian',
                'parent_id' => 3
            ],
            [
                'name' => 'Street Food',
                'iw_name' => 'אוכל רחוב',
                'slug' => 'street-food',
                'parent_id' => 3
            ],
            [
                'name' => 'Israeli Tapas',
                'iw_name' => 'טאפאס ישראלי',
                'slug' => 'israeli-tapas',
                'parent_id' => 3
            ],
            [
                'name' => 'Meat',
                'iw_name' => 'בשר',
                'slug' => 'meat',
                'parent_id' => 3
            ],
            [
                'name' => 'Vegetarian',
                'iw_name' => 'צמחוני',
                'slug' => 'vegetarian',
                'parent_id' => 3
            ],
            [
                'name' => 'Kosher',
                'iw_name' => 'כשר',
                'slug' => 'kosher',
                'parent_id' => 3
            ],
            [
                'name' => 'Fast Food',
                'iw_name' => 'אוכל מהיר',
                'slug' => 'fast0food',
                'parent_id' => 3
            ],
            [
                'name' => 'Fine Dinning',
                'iw_name' => 'אוכל משובח',
                'slug' => 'fine-dinning',
                'parent_id' => 3
            ],
            [
                'name' => 'Asian Fusion',
                'iw_name' => 'פיוז\'\'ן אסייתי',
                'slug' => 'asian-fusion',
                'parent_id' => 3
            ],
            [
                'name' => 'Health & Fitness',
                'iw_name' => 'בריאות וכושר',
                'slug' => 'health-and-fitness',
                'parent_id' => 0
            ],
            [
                'name' => 'Travel & Leisure',
                'iw_name' => 'טיולים ופנאי',
                'slug' => 'travel-leisure',
                'parent_id' => 0
            ],
            [
                'name' => 'Sweet Treats',
                'iw_name' => 'קינוחים מתוקים',
                'slug' => 'sweet-treats',
                'parent_id' => 0
            ],
            [
                'name' => 'Hair & Beauty',
                'iw_name' => 'עיצוב שיער ויופי',
                'slug' => 'hair-beauty',
                'parent_id' => 0
            ],
            [
                'name' => 'Nightlife',
                'iw_name' => 'תרבות ופנאי',
                'slug' => 'nightlife',
                'parent_id' => 0
            ],
            [
                'name' => 'Home & Garden',
                'iw_name' => 'בית וגינה',
                'slug' => 'home-garden',
                'parent_id' => 0
            ],
            [
                'name' => 'Freelancers',
                'iw_name' => 'פרילנסרים',
                'slug' => 'freelancers',
                'parent_id' => 0
            ],
            [
                'name' => 'Style & Fashion',
                'iw_name' => 'אופנה ועיצוב',
                'slug' => 'style-fashion',
                'parent_id' => 0
            ],
        ];


        foreach ($categories as $category) {
            Category::create($category);
        }
    }
}
